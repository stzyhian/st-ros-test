# CMake generated Testfile for 
# Source directory: /home/vecan/vecan-ros-test/src
# Build directory: /home/vecan/vecan-ros-test/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(velodyne/velodyne)
SUBDIRS(velodyne/velodyne_msgs)
SUBDIRS(beginner_tutorials)
SUBDIRS(icp_localization)
SUBDIRS(model_publisher)
SUBDIRS(oxford_gps_eth)
SUBDIRS(my_pcl_tutorial)
SUBDIRS(velodyne/velodyne_driver)
SUBDIRS(velodyne/velodyne_pointcloud)
