# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/vecan/vecan-ros-test/src/velodyne/velodyne_driver/src/driver/driver.cc" "/home/vecan/vecan-ros-test/build/velodyne/velodyne_driver/src/driver/CMakeFiles/velodyne_node.dir/driver.cc.o"
  "/home/vecan/vecan-ros-test/src/velodyne/velodyne_driver/src/driver/velodyne_node.cc" "/home/vecan/vecan-ros-test/build/velodyne/velodyne_driver/src/driver/CMakeFiles/velodyne_node.dir/velodyne_node.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"velodyne_driver\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/vecan/vecan-ros-test/build/velodyne/velodyne_driver/src/lib/CMakeFiles/velodyne_input.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/vecan/vecan-ros-test/devel/include"
  "/home/vecan/vecan-ros-test/src/velodyne/velodyne_driver/include"
  "/opt/ros/indigo/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
