(cl:in-package velodyne_compression-msg)
(cl:export '(HEADER-VAL
          HEADER
          DISTANCE-VAL
          DISTANCE
          INTENSITY-VAL
          INTENSITY
          ROTATION-VAL
          ROTATION
          SCAN_ROW-VAL
          SCAN_ROW
          SCAN_COLUMN-VAL
          SCAN_COLUMN
))