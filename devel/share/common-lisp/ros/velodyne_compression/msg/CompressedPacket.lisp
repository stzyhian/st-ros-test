; Auto-generated. Do not edit!


(cl:in-package velodyne_compression-msg)


;//! \htmlinclude CompressedPacket.msg.html

(cl:defclass <CompressedPacket> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (distance
    :reader distance
    :initarg :distance
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 0 :element-type 'cl:fixnum :initial-element 0))
   (intensity
    :reader intensity
    :initarg :intensity
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 0 :element-type 'cl:fixnum :initial-element 0))
   (rotation
    :reader rotation
    :initarg :rotation
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 0 :element-type 'cl:fixnum :initial-element 0))
   (scan_row
    :reader scan_row
    :initarg :scan_row
    :type cl:fixnum
    :initform 0)
   (scan_column
    :reader scan_column
    :initarg :scan_column
    :type cl:fixnum
    :initform 0))
)

(cl:defclass CompressedPacket (<CompressedPacket>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <CompressedPacket>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'CompressedPacket)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name velodyne_compression-msg:<CompressedPacket> is deprecated: use velodyne_compression-msg:CompressedPacket instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <CompressedPacket>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader velodyne_compression-msg:header-val is deprecated.  Use velodyne_compression-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'distance-val :lambda-list '(m))
(cl:defmethod distance-val ((m <CompressedPacket>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader velodyne_compression-msg:distance-val is deprecated.  Use velodyne_compression-msg:distance instead.")
  (distance m))

(cl:ensure-generic-function 'intensity-val :lambda-list '(m))
(cl:defmethod intensity-val ((m <CompressedPacket>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader velodyne_compression-msg:intensity-val is deprecated.  Use velodyne_compression-msg:intensity instead.")
  (intensity m))

(cl:ensure-generic-function 'rotation-val :lambda-list '(m))
(cl:defmethod rotation-val ((m <CompressedPacket>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader velodyne_compression-msg:rotation-val is deprecated.  Use velodyne_compression-msg:rotation instead.")
  (rotation m))

(cl:ensure-generic-function 'scan_row-val :lambda-list '(m))
(cl:defmethod scan_row-val ((m <CompressedPacket>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader velodyne_compression-msg:scan_row-val is deprecated.  Use velodyne_compression-msg:scan_row instead.")
  (scan_row m))

(cl:ensure-generic-function 'scan_column-val :lambda-list '(m))
(cl:defmethod scan_column-val ((m <CompressedPacket>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader velodyne_compression-msg:scan_column-val is deprecated.  Use velodyne_compression-msg:scan_column instead.")
  (scan_column m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <CompressedPacket>) ostream)
  "Serializes a message object of type '<CompressedPacket>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'distance))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) ele) ostream))
   (cl:slot-value msg 'distance))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'intensity))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) ele) ostream))
   (cl:slot-value msg 'intensity))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'rotation))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) ele) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) ele) ostream))
   (cl:slot-value msg 'rotation))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'scan_row)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'scan_row)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'scan_column)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'scan_column)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <CompressedPacket>) istream)
  "Deserializes a message object of type '<CompressedPacket>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'distance) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'distance)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:aref vals i)) (cl:read-byte istream)))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'intensity) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'intensity)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:aref vals i)) (cl:read-byte istream)))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'rotation) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'rotation)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:aref vals i)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:aref vals i)) (cl:read-byte istream)))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'scan_row)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'scan_row)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'scan_column)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'scan_column)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<CompressedPacket>)))
  "Returns string type for a message object of type '<CompressedPacket>"
  "velodyne_compression/CompressedPacket")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'CompressedPacket)))
  "Returns string type for a message object of type 'CompressedPacket"
  "velodyne_compression/CompressedPacket")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<CompressedPacket>)))
  "Returns md5sum for a message object of type '<CompressedPacket>"
  "f98b9e4f7b35345bcee2ad2a9c9faf1f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'CompressedPacket)))
  "Returns md5sum for a message object of type 'CompressedPacket"
  "f98b9e4f7b35345bcee2ad2a9c9faf1f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<CompressedPacket>)))
  "Returns full string definition for message of type '<CompressedPacket>"
  (cl:format cl:nil "Header header~%uint8[] distance~%uint8[] intensity~%uint16[] rotation~%uint16 scan_row~%uint16 scan_column~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'CompressedPacket)))
  "Returns full string definition for message of type 'CompressedPacket"
  (cl:format cl:nil "Header header~%uint8[] distance~%uint8[] intensity~%uint16[] rotation~%uint16 scan_row~%uint16 scan_column~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <CompressedPacket>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'distance) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'intensity) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'rotation) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 2)))
     2
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <CompressedPacket>))
  "Converts a ROS message object to a list"
  (cl:list 'CompressedPacket
    (cl:cons ':header (header msg))
    (cl:cons ':distance (distance msg))
    (cl:cons ':intensity (intensity msg))
    (cl:cons ':rotation (rotation msg))
    (cl:cons ':scan_row (scan_row msg))
    (cl:cons ':scan_column (scan_column msg))
))
