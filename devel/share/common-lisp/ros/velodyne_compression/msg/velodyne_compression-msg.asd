
(cl:in-package :asdf)

(defsystem "velodyne_compression-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "CompressedPacket" :depends-on ("_package_CompressedPacket"))
    (:file "_package_CompressedPacket" :depends-on ("_package"))
  ))