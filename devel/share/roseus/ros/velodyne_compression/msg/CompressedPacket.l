;; Auto-generated. Do not edit!


(when (boundp 'velodyne_compression::CompressedPacket)
  (if (not (find-package "VELODYNE_COMPRESSION"))
    (make-package "VELODYNE_COMPRESSION"))
  (shadow 'CompressedPacket (find-package "VELODYNE_COMPRESSION")))
(unless (find-package "VELODYNE_COMPRESSION::COMPRESSEDPACKET")
  (make-package "VELODYNE_COMPRESSION::COMPRESSEDPACKET"))

(in-package "ROS")
;;//! \htmlinclude CompressedPacket.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass velodyne_compression::CompressedPacket
  :super ros::object
  :slots (_header _distance _intensity _rotation _scan_row _scan_column ))

(defmethod velodyne_compression::CompressedPacket
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:distance __distance) (make-array 0 :initial-element 0 :element-type :char))
    ((:intensity __intensity) (make-array 0 :initial-element 0 :element-type :char))
    ((:rotation __rotation) (make-array 0 :initial-element 0 :element-type :integer))
    ((:scan_row __scan_row) 0)
    ((:scan_column __scan_column) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _distance __distance)
   (setq _intensity __intensity)
   (setq _rotation __rotation)
   (setq _scan_row (round __scan_row))
   (setq _scan_column (round __scan_column))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:distance
   (&optional __distance)
   (if __distance (setq _distance __distance)) _distance)
  (:intensity
   (&optional __intensity)
   (if __intensity (setq _intensity __intensity)) _intensity)
  (:rotation
   (&optional __rotation)
   (if __rotation (setq _rotation __rotation)) _rotation)
  (:scan_row
   (&optional __scan_row)
   (if __scan_row (setq _scan_row __scan_row)) _scan_row)
  (:scan_column
   (&optional __scan_column)
   (if __scan_column (setq _scan_column __scan_column)) _scan_column)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint8[] _distance
    (* 1    (length _distance)) 4
    ;; uint8[] _intensity
    (* 1    (length _intensity)) 4
    ;; uint16[] _rotation
    (* 2    (length _rotation)) 4
    ;; uint16 _scan_row
    2
    ;; uint16 _scan_column
    2
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint8[] _distance
     (write-long (length _distance) s)
     (princ _distance s)
     ;; uint8[] _intensity
     (write-long (length _intensity) s)
     (princ _intensity s)
     ;; uint16[] _rotation
     (write-long (length _rotation) s)
     (dotimes (i (length _rotation))
       (write-word (elt _rotation i) s)
       )
     ;; uint16 _scan_row
       (write-word _scan_row s)
     ;; uint16 _scan_column
       (write-word _scan_column s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint8[] _distance
   (let ((n (sys::peek buf ptr- :integer))) (incf ptr- 4)
     (setq _distance (make-array n :element-type :char))
     (replace _distance buf :start2 ptr-) (incf ptr- n))
   ;; uint8[] _intensity
   (let ((n (sys::peek buf ptr- :integer))) (incf ptr- 4)
     (setq _intensity (make-array n :element-type :char))
     (replace _intensity buf :start2 ptr-) (incf ptr- n))
   ;; uint16[] _rotation
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _rotation (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _rotation i) (sys::peek buf ptr- :short)) (incf ptr- 2)
     ))
   ;; uint16 _scan_row
     (setq _scan_row (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _scan_column
     (setq _scan_column (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;;
   self)
  )

(setf (get velodyne_compression::CompressedPacket :md5sum-) "f98b9e4f7b35345bcee2ad2a9c9faf1f")
(setf (get velodyne_compression::CompressedPacket :datatype-) "velodyne_compression/CompressedPacket")
(setf (get velodyne_compression::CompressedPacket :definition-)
      "Header header
uint8[] distance
uint8[] intensity
uint16[] rotation
uint16 scan_row
uint16 scan_column

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :velodyne_compression/CompressedPacket "f98b9e4f7b35345bcee2ad2a9c9faf1f")


