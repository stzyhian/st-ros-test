//============================================================================================================
//!
//! \file menuConfig.h
//!
//! \brief menuConfig file.
//!
//============================================================================================================

#ifndef __MENUCONFIG_H
#define __MENUCONFIG_H

#include "mainCommon.h"

/** \brief Initialize system
* \param[in] reference of state
* \param[in] reference of pcl display
* \param[in] reference of digital map
* \param[in] reference of inertial
* \param[in] reference of velodyne
*/
void systemInitialize(stateType &myState, digitalMap *myMap, inertial *myInertial, velodyne *myVelodyne);

/** \brief localization and obstacle detection
* \param[in] reference of state
* \param[in] reference of pcl display
* \param[in] pointer   of digital map
* \param[in] pointer   of inertial
* \param[in] pointer   of velodyne
* \param[in] pointer   of localizer
* \param[in] reference of pointcloud
*/
void
localizationAndObstacleDetection(stateType &myState, digitalMap *myMap, inertial *myInertial, velodyne *myVelodyne, localization *myLocalizer);

/** \brief Initialize vechile state
* \param[in] reference of state
*/
void 
stateInitial(stateType &myState);

/** \brief Output interaction information
* \param[in] reference of state
*/
void 
userOperationGuide(stateType &myState);

/** \brief Initialize sensor
* \param[in] pointer of inertial object
* \param[in] pointer of velodyne object
* \param[in] reference of state
*/
void 
sensorInitial(inertial *myInertial, velodyne *myVelodyne, stateType &myState);

/** \brief Set display vechile state
* \param[in] reference of pclDisplayObject
*/
void 
addDisplayText(pclDisp &pclDisplay);

/** \brief tran variable to string and display
* \param[in] reference of pclDisplayObject
* \param[in] reference of inertialData
* \param[in] reference of state
*/
void 
DisplayTextVariable(pclDisp &pclDisplay, InertialType &inetialData, stateType &myState);

/** \brief pcl keyboard callback
* \param[in] keyboard event
* \param[in] state variable pointer
*/
void 
keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event, void* statePtr);

/** \brief time manager
* \param[in] flag = 1 : start, flag = 2 : end and output
*/
double
timeCalculate(int flag);

#endif