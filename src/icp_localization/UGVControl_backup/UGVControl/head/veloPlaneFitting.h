//
// File: veloPlaneFitting.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 07-Sep-2016 10:51:21
//
#ifndef VELOPLANEFITTING_H
#define VELOPLANEFITTING_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

// Function Declarations
extern void veloPlaneFitting(const double veloOriginROI[350000], int
  veloOriginROILen, double useRate, double *p00, double *p10, double *p01);
extern void veloPlaneFitting_initialize();
extern void veloPlaneFitting_terminate();

#endif

//
// File trailer for veloPlaneFitting.h
//
// [EOF]
//
