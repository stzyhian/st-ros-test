//============================================================================================================
//!
//! \file inertial2.h
//!
//! \brief inertial process file.
//!
//============================================================================================================
#ifndef __INERTIAL2_H
#define __INERTIAL2_H


#ifdef _DEBUG
#pragma comment(lib,"myLib\\Inertial2Lib_Debug.lib")
#else 
#pragma comment(lib,"myLib\\Inertial2Lib_Release.lib")
#endif


#include <stdio.h>
#include <iostream>

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

#include "NComRxC.h"
#include "UDPServer.h"
#include "gaussianProjection.h"

#define pi 3.1415926535897932384626433832795

static const char *ComGpsXModeName[31] =
{
	"None",
	"Search",
	"Doppler",
	"SPS",
	"Differential",
	"RTK Float",
	"RTK Integer",
	"WAAS",
	"Omnistar",
	"Omnistar HP",
	"No Data",
	"Blanked",
	"Doppler (PP)",
	"SPS (PP)",
	"Differential (PP)",
	"RTK Float (PP)",
	"RTK Integer (PP)",
	"Omnistar XP",
	"CDGPS",
	"Not Recognised",
	"gxDoppler",
	"gxSPS",
	"gxDifferential",
	"gxFloat",
	"gxInteger",
	"ixDoppler",
	"ixSPS",
	"ixDifferential",
	"ixFloat",
	"ixInteger",
	"Unknown"
};

typedef struct
{
	double mLat; //!< Latitude. [deg]
	double mLon; //!< Longitude. [deg]
	double mAlt; //!< Altitude. [m]
	double mRoll;//!< Roll. [rad]
	double mPitch;//!< Pitch. [rad]
	double mHeading;//!< Heading. [deg]
	double mVn;//!< North velocity. [m s^(-1)]
	double mVe;//!< East velocity. [m s^(-1)]
	double mVf; //!< Forward velocity. [m s^(-1)]
	double mVl;//!< Lateral velocity. [m s^(-1)]
	double mVu;//!< Downward velocity. [m s^(-1)]
	double mAx;//!< Acceleration along the X axis. [m s^(-2)]
	double mAy;//!< Acceleration along the Y axis. [m s^(-2)]
	double mAz;//!< Acceleration along the Z axis. [m s^(-2)]
	double mAf;//!< Acceleration forward. [m s^(-2)]
	double mAl;//!< Acceleration laterally. [m s^(-2)]
	double mAu;//!< Acceleration downward. [m s^(-2)]
	double mWx;//!< Angular rate about the X axis. [deg s^(-1)]
	double mWy;//!< Angular rate about the Y axis. [deg s^(-1)]
	double mWz;//!< Angular rate about the Z axis. [deg s^(-1)]
	double mWf;//!< Angular rate about the forward axis. [deg s^(-1)]
	double mWl;//!< Angular rate about the lateral axis. [deg s^(-1)]
	double mWu;//!< Angular rate about the down axis. [deg s^(-1)]
	double mPos_accuracy;//velocity accuracy (north/east in m)
	double mVel_accuracy;//velocity accuracy (north/east in m/s)
	double mNavstat;//navigation status (see navstat_to_string)
	double mNumsats;//number of satellites tracked by primary GPS receiver
	double mPosmode;//position mode of primary GPS receiver(see gps_mode_to_string)
	double mVelmode;//velocity mode of primary GPS receiver(see gps_mode_to_string)
	double mOrimode;//orientation mode of primary GPS receiver (see gps_mode_to_string)
}InertialType;

typedef struct
{
	double x;
	double y;
}TrajectoryType;

class inertial
{

public:
	// inertial2+  operation mode
	enum modeType { offline = 1, online };

public:
	/** \brief Constructor
	* \param[in] set inertial2 operation mode
	*/
	inertial(enum modeType modeIn);
	~inertial();

	/** \brief Set offline file name
	* \param[in] prefix name
	* \param[in] suffix name
	*/
	void
		setFileName(std::string prefixName, std::string suffixName);

	/** \brief Set online save file name
	* \param[in] prefix name
	* \param[in] suffix name
	*/
	void
		setOnlineDataSaveFileName(std::string prefixName, std::string suffixName);

	/** \brief Read offline interial data
	* \param[in] file index
	*/
	void
		readFromFile(int index);

	/** \brief get newest data
	* \param[in] reference of data
	*/
	void
		returnIntertialData(InertialType &dataIn, int saveFlag = 0);

	/** \brief get GPS state string
	* \param[in] position mode
	*/
	static std::string
		returnGPSStateString(double mPosmode);

	/** \brief calculate vehcile movement
	* \param[in] pre  inertial data
	* \param[in] post inertial data
	*/
	void
		movementCalculate(InertialType &preData, InertialType &postData);

	/** \brief Create trajectory point cloud
	* \param[in] reference of cloud pointer
	*/
	void
		createTrajectoryPointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloudIn);

	/** \brief Inertial data receive thread
	* \param[in] inertial class pointer
	*/
	friend void
		inertialRun(inertial *inertialPtr);

	/** \brief Inertial data save thread
	* \param[in] inertial class pointer
	*/
	friend void
		inertialFileSaveThread(inertial *inertialPtr);

private:
	/** \brief save one frame inertial data
	*/
	void
		callFileSave(void);

public:
	// trajectory points in local coordinate
	std::vector<TrajectoryType> trajectoryPoints;

	// vehicle dynamics
	double dx;
	double dy;
	double dh;

private:
	// inertial2 mode
	enum modeType inertial2Mode;

	// offline file name 
	std::string prefixName;
	std::string suffixName;

	// online save file name 
	std::string onlineFilePrefixName;
	std::string onlineFileSuffixName;

	// thread variable
	boost::thread *inertialRunThread;
	boost::thread *inertialDataSaveThread;
	boost::mutex   inertialMutex;
	int            newDataFlag;
	bool		   threadStopFlag;
	int			   onlineFileIndex;

	UDPServer *inertialUDPServer;
	NComRxC *nrx;

	// newest inertial2 data
	InertialType inertialData;
	InertialType inertialDataSave;

	// multi-frame data
	std::vector<InertialType> multiFrameInertialData;
	const size_t saveFrameCount = 30;


	gaussianProjection inertialProjection;

};

#endif //__INERTIAL2_H