//============================================================================================================
//!
//! \file localization.h
//!
//! \brief localization process file.
//!
//============================================================================================================

#ifndef __LOCALIZATION_H
#define __LOCALIZATION_H

#ifdef _DEBUG
#pragma comment(lib,"myLib\\KalmanfilterDynamic_Debug.lib")
#else 
#pragma comment(lib,"myLib\\KalmanfilterDynamic_Release.lib")
#endif

#include <stdio.h>
#include <iostream>
#include <time.h>
#include <omp.h>

#include "SysKF.h"
#include "mapKFFusion.h"
#include "ICP\icpPointToPoint.h"

#include "digitalMap.h"
#include "gaussianProjection.h"
#include "inertial2.h"
#include "velodyne32E.h"

class localization
{
public:
	/** \brief Constructor
	* \param[in] initial vehicle position
	*/
	localization(InertialType vehiclePosition);

	/** \brief vehicle dynamic estimation
	* \param[in] dx
	* \param[in] dy
	* \param[in] dh
	* \param[in] gps observation
	*/
	void
		dynamicEstimation(double dx, double dy, double dh, InertialType gpsObservation);

	/** \brief feature points matching
	* \param[in] velo curb
	* \param[in] map curb
	*/
	void
		featureMatching(std::vector<VeloPointT> &curbPointsVelo, std::vector<VeloPointT> &curbMap);

	/** \brief grid map matching
	* \param[in] digital gird map
	* \param[in] velo grid parameter
	* \param[in] velo ground gps points
	*/
	void
		gridMatching(gridMapParaT &gridMap, veloGridParaT &veloGrid, std::vector<GpsPointT>  &GroundGpsPoints);

	/** \brief map observation filte
	*/
	void
		mapObservationFilte(void);

public:
	InertialType vehiclePosition;

	int featureMapMatchingFlag;

	int gridMapMatchingFlag;

private:
#define  ICPIteration  8

	double sysEstP0[9];
	double mapEstP0[9];

	double positionEst[3];

	double MapObservation[5];

	double featureMapMatchingError;
	

	gaussianProjection localizationProjection;
};




#endif //__LOCALIZATION_H
