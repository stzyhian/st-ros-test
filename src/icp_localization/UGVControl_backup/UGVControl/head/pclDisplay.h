//============================================================================================================
//!
//! \file pclDisplay.h
//!
//! \brief pclDisplay file.
//!
//============================================================================================================
#ifndef __PCLDISPLAY_H
#define __PCLDISPLAY_H

#include <stdio.h>
#include <string.h>
#include <iostream>

//#include <pcl/io/io.h>
#include <pcl/io/obj_io.h>
#include <pcl/PolygonMesh.h>
#include <pcl/point_cloud.h>
#include <pcl/io/vtk_lib_io.h>//loadPolygonFileOBJ header
#include <pcl/visualization/pcl_visualizer.h>


class pclDisp
{
public:
	/** \brief Constructor
	* \param[in] setWinH	Display Windows Height
	* \param[in] setWinW	Display Windows Width
	*/
	pclDisp(int setWinH = 700, int setWinW = 1000);
	~pclDisp();

	/** \brief Set windows prefix name
	* \param[in] winPreName	Display Windows Prefix Name
	*/
	void
		setWinPreName(std::string winPreName);

	/** \brief Set windows suffix name
	* \param[in] winSufName	Display Windows Suffix Name
	*/
	void
		setWinSufName(std::string winSufName);

	/** \brief Set text display parameter
	* \param[in] font size
	* \param[in] width indentation
	*/
	void
		setTextDisplayPara(int setFontSize = 16, int setWidthIndentation = 250);

	/** \brief Load vehicle 3D model
	* \param[in] filename	vehicle model file path
	*/
	void
		loadVechileModel(char filename[]);

	/** \brief pcl Visualizer Initial
	*/
	void 
		pclVisualizerInit(void);

	/** \brief pcl Visualizer Initial
	* \param[in] keyboard call back function pointer
	* \param[in] variable input pointer
	*/
	void
		pclVisualizerInit(void(*keyboardEventOccurred)(const pcl::visualization::KeyboardEvent &event, void* statePtr), void *state);

	/** \brief pcl Visualizer Initial
	* \param[in] keyboard call back function pointer
	* \param[in] variable input pointer
	* \param[in] the x coordinate of the camera location
	* \param[in] the y coordinate of the camera location
	* \param[in] the z coordinate of the camera location
	* \param[in] the x component of the view up direction of the camera
	* \param[in] the y component of the view up direction of the camera
	* \param[in] the z component of the view up direction of the camera
	*/
	void
		pclVisualizerInit(void(*keyboardEventOccurred)(const pcl::visualization::KeyboardEvent &event, void* statePtr), void *state,
			double cameraPosX, double cameraPosY, double cameraPosZ, double cameraUpX, double cameraUpY, double cameraUpZ);

	/** \brief Add display text
	* \param[in] text string
	*/
	int
		menuTextAdd(std::string addString);

	/** \brief Display the variable
	* \param[in] variable string
	*/
	int
		menuTextDisplay(std::vector<std::string> VariableString);

	/** \brief Add display the pointcloud
	* \param[in]	pointcloud pointer
	* \param[in]	pointcloud handler
	* \param[in]	pointcloud id
	*/
	template<typename PointT>
	void
		pointCloudDisplayAdd(const typename pcl::PointCloud<PointT>::Ptr &cloud,
			const typename pcl::visualization::PointCloudColorHandlerGenericField<PointT> &Handler,
			std::string id);

	/** \brief Add display the pointcloud
	* \param[in]	pointcloud pointer
	* \param[in]	pointcloud handler
	* \param[in]	pointcloud id
	*/
	template<typename PointT>
	void
		pointCloudDisplayAdd(const typename pcl::PointCloud<PointT>::Ptr &cloud,
			const typename pcl::visualization::PointCloudColorHandlerCustom<PointT> &Handler,
			std::string id);

	/** \brief Update display the pointcloud
	* \param[in]	pointcloud pointer
	* \param[in]	pointcloud handler
	* \param[in]	pointcloud id
	*/
	template<typename PointT>
	void
		pointCloudDisplayUpdate(const typename pcl::PointCloud<PointT>::Ptr &cloud,
			const typename pcl::visualization::PointCloudColorHandlerGenericField<PointT> &Handler,
			std::string id);

	/** \brief Update display the pointcloud
	* \param[in]	pointcloud pointer
	* \param[in]	pointcloud handler
	* \param[in]	pointcloud id
	*/
	template<typename PointT>
	void
		pointCloudDisplayUpdate(const typename pcl::PointCloud<PointT>::Ptr &cloud,
			const typename pcl::visualization::PointCloudColorHandlerCustom<PointT> &Handler,
			std::string id);

	/** \brief Set pointcloud property
	* \param[in]	property
	* \param[in]	value
	* \param[in]	pointcloud id
	*/
	void
		setPointCloudProperty(int property, double value, const std::string &id);

	/** \brief Refresh the windows
	*/
	void
		pclVisualizerRefresh(void);

private:

	int windowsHeight;	//700,1000
	int windowsWidth;	//1000,1400
	std::string windowsPrefixName;
	std::string windowsSuffixName;
	// display Text
	int fontSize;
	int widthIndentation;
	std::vector<std::string> displayString;
	std::vector<std::string> displayVariableString;
	int                      displayStringPos;
	// vehicle model
	pcl::PolygonMesh vehicleMesh;
	// pcl visualization
	pcl::visualization::PCLVisualizer *viewer;


};




/** \brief Add display the pointcloud
* \param[in]	pointcloud pointer
* \param[in]	pointcloud handler
* \param[in]	pointcloud id
*/
template<typename PointT>
inline void
pclDisp::pointCloudDisplayAdd(const typename pcl::PointCloud<PointT>::Ptr &cloud,
	const typename pcl::visualization::PointCloudColorHandlerGenericField<PointT> &Handler,
	std::string id)
{
	viewer->addPointCloud<PointT>(cloud, Handler, id);
}

/** \brief Add display the pointcloud
* \param[in]	pointcloud pointer
* \param[in]	pointcloud handler
* \param[in]	pointcloud id
*/
template<typename PointT>
inline void
pclDisp::pointCloudDisplayAdd(const typename pcl::PointCloud<PointT>::Ptr &cloud,
	const typename pcl::visualization::PointCloudColorHandlerCustom<PointT> &Handler,
	std::string id)
{
	viewer->addPointCloud<PointT>(cloud, Handler, id);
}

/** \brief Update display the pointcloud
* \param[in]	pointcloud pointer
* \param[in]	pointcloud handler
* \param[in]	pointcloud id
*/
template<typename PointT>
inline void
pclDisp::pointCloudDisplayUpdate(const typename pcl::PointCloud<PointT>::Ptr & cloud,
	const typename pcl::visualization::PointCloudColorHandlerGenericField<PointT>& Handler,
	std::string id)
{
	viewer->updatePointCloud<PointT>(cloud, Handler, id);
}

/** \brief Update display the pointcloud
* \param[in]	pointcloud pointer
* \param[in]	pointcloud handler
* \param[in]	pointcloud id
*/
template<typename PointT>
inline void
pclDisp::pointCloudDisplayUpdate(const typename pcl::PointCloud<PointT>::Ptr & cloud,
	const typename pcl::visualization::PointCloudColorHandlerCustom<PointT>& Handler,
	std::string id)
{
	viewer->updatePointCloud<PointT>(cloud, Handler, id);
}


#endif //__PCLDISPLAY_H