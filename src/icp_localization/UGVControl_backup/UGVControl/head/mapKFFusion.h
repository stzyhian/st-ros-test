//
// File: mapKFFusion.h
//
// MATLAB Coder version            : 3.2
// C/C++ source code generated on  : 14-Nov-2016 16:30:13
//
#ifndef MAPKFFUSION_H
#define MAPKFFUSION_H

// Include Files
#include <cmath>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

// Function Declarations
extern void mapKFFusion(double P0[9], const double X[3], const double ZZ[5], int
  flag, double R00, double R11, double R22, double R33, double R44, double Xkf[3]);

#endif

//
// File trailer for mapKFFusion.h
//
// [EOF]
//
