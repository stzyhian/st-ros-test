//============================================================================================================
//!
//! \file UDPServer.h
//!
//! \brief UDPServer operation file.
//!
//============================================================================================================
#ifndef __UDPSERVER_H   
#define __UDPSERVER_H

#pragma comment(lib, "ws2_32.lib")

#include <iostream>


#include <WinSock2.h>
#include <windows.h>



class UDPServer
{
public:
	/** \brief Constructor
	* \param[in] udp receive port
	*/
	UDPServer(int port);
	~UDPServer();

	/** \brief UDP server receive
	* \param[in] receive buffer
	* \param[in] buffer length
	*/
	int
		UDPServerReceive(char buf[], int length);

private:
	int fromLength;
	SOCKET recvSocket;
	sockaddr_in from, recvAddr;
	WSADATA recvWsdata;
	BOOL recvOptval;
};

#endif //__UDPSERVER_H