//============================================================================================================
//!
//! \file mainCommon.h
//!
//! \brief main include file.
//!
//============================================================================================================
#ifndef __MAINCOMMON_H
#define __MAINCOMMON_H

#include <iostream>
#include <string>

#define pi 3.1415926535897932384626433832795

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

#include "pclDisplay.h"
#include "velodyne32E.h"
#include "inertial2.h"
#include "digitalMap.h"
#include "localization.h"

typedef pcl::PointCloud<pcl::PointXYZI>::Ptr veloRawPointT;
typedef pcl::PointCloud<pcl::PointXYZ>::Ptr cPointT;

typedef pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZI> veloRawHdl;
typedef pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cHdl;

typedef struct
{
	InertialType inertialVar;		// now frame inertial variable
	InertialType inertialVarPre ;	// pre frame inertial variable
	// program operation mode
	char inputMode;
	// inertial2+ operation mode
	inertial::modeType inertialMode;
	// velodyne hdl-32e operation mode
	velodyne::modeType velodyneMode;
	// program time consume
	double timeConsume;
	// display windows title suffix name string
	char winSufName[100];
	// offline file index
	int offlineFileIndex;
	// offline frame update
	int offlineUpdateFlag;
	// online frame index
	int onlineFrameIndex;
	// program quit flag
	int quitFlag;

	int dispUpdateFlag;
	boost::mutex  Mutex;
	
}stateType;

typedef struct
{
	veloRawPointT *veloRawPointCloud;
	cPointT *cPointCloud[5];

	veloRawHdl *veloColorHandler;
	cHdl *ColorHandler[5];

	std::string pointcloudName[6];

	const int pointArrayLen = 5;

}pclPointCloudArrayType;


#include "menuConfig.h"

#endif