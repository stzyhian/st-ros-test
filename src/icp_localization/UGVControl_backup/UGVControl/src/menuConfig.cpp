#include "menuConfig.h"

/** \brief Initialize system
* \param[in] reference of state
* \param[in] reference of pcl display
* \param[in] reference of digital map
* \param[in] reference of inertial
* \param[in] reference of velodyne
*/
void
systemInitialize(stateType &myState, digitalMap *myMap, inertial *myInertial, velodyne *myVelodyne)
{
	myMap->readMap("Lat.bin", "Lon.bin");

	sensorInitial(myInertial, myVelodyne, myState);

	//-----get initial position---//
	printf("Waiting for initial localization\n");
	int whileFlag = true;
	while (whileFlag)
	{
		myInertial->returnIntertialData(myState.inertialVar);
		if (myMap->gridMapPara.latStart < myState.inertialVar.mLat && myMap->gridMapPara.latEnd > myState.inertialVar.mLat &&
			myMap->gridMapPara.lonStart < myState.inertialVar.mLon && myMap->gridMapPara.lonEnd > myState.inertialVar.mLon &&
			myState.inertialVar.mPos_accuracy <1.0)
		{
			whileFlag = false;
		}
		Sleep(100);
	}
	
}

/** \brief map projection
* \param[in] pointer   of digital map
* \param[in] pointer   of localizer
* \param[in] pointer   of velodyne
*/
void mapProcess(digitalMap *myMap, localization *myLocalizer, velodyne *myVelodyne)
{
	//--create local map data---//
	myMap->createLocalMapData(digitalMap::curb, myLocalizer->vehiclePosition.mLat, myLocalizer->vehiclePosition.mLon, myLocalizer->vehiclePosition.mHeading, 0.4e-3, 0.4e-3);//2 ms
	myMap->createLocalMapData(myLocalizer->vehiclePosition.mLat, myLocalizer->vehiclePosition.mLon, myLocalizer->vehiclePosition.mHeading, 0.75e-3, 0.75e-3, myVelodyne->obsXMin, myVelodyne->obsXMax, myVelodyne->obsYMin, myVelodyne->obsYMax);//3 ms
}

/** \brief velodyne curb detection
* \param[in] pointer   of velodyne
* \param[in] pointer   of inertial
*/
void veloProcess(velodyne *myVelodyne, inertial *myInertial)
{
	myVelodyne->performCurbDetection();//1.0 ms
	myVelodyne->generateMultiFrameCurbs(myInertial->dx, myInertial->dy, myInertial->dh);// 0.2 ms
	myVelodyne->beamModelExtraction(myInertial->trajectoryPoints);//35 ms
}

/** \brief grid map process and matching
* \param[in] pointer   of velodyne
* \param[in] pointer   of localizer
* \param[in] pointer   of digital map
*/
void gridMapProcess(velodyne *myVelodyne, localization *myLocalizer, digitalMap *myMap)
{
	myVelodyne->generateGpsPoints(myLocalizer->vehiclePosition.mLat, myLocalizer->vehiclePosition.mLon, myLocalizer->vehiclePosition.mHeading);// 5 ms

	//-localization(map matching)//
	myLocalizer->gridMatching(myMap->gridMapPara, myVelodyne->veloGridPara, myVelodyne->GroundGpsPoints);//20 ms
}

/** \brief obstacle detect
* \param[in] pointer   of velodyne
* \param[in] pointer   of digital map
* \param[in] reference of pointcloud
*/
void obstacleDetect(velodyne *myVelodyne, digitalMap *myMap)
{
	//--------obstacle detection--------//
	myVelodyne->divideObstaclePoints();//1 ms
	myVelodyne->createDrivableArea(myMap->localCurbMapDrivable);// 4ms
	myVelodyne->computeDrivablePoints();//5 ms
	myVelodyne->obstacleDetect();//8 ms
}

/** \brief localization and obstacle detection
* \param[in] reference of state
* \param[in] reference of pcl display
* \param[in] pointer   of digital map
* \param[in] pointer   of inertial
* \param[in] pointer   of velodyne
* \param[in] pointer   of localizer
* \param[in] reference of pointcloud
*/
void localizationAndObstacleDetection(stateType &myState, digitalMap *myMap, inertial *myInertial, velodyne *myVelodyne, localization *myLocalizer)
{

	//-------------------------sensor processing---------------------//
	//------dynamic compute------//
	myInertial->movementCalculate(myState.inertialVarPre, myState.inertialVar);
	//----position prediction---//
	myLocalizer->dynamicEstimation(myInertial->dx, myInertial->dy, myInertial->dh, myState.inertialVar);
	//---map projection thread-//
	boost::thread mapProcessThread(mapProcess, myMap, myLocalizer, myVelodyne);

	//----velodyne calibration---//
	myVelodyne->fixCalibration(180.9 / 180 * pi, -2.5 / 180 * pi, 0.4 / 180 * pi);
	myVelodyne->posCalibration(myState.inertialVar.mPitch, myState.inertialVar.mRoll);
	myVelodyne->dividePoints();
	//----curb detection ------//
	boost::thread curbDetectThread(veloProcess, myVelodyne, myInertial);
	//-----grid matching-------//
	boost::thread gridMatchingThread(gridMapProcess, myVelodyne, myLocalizer, myMap);
	//--waiting for map projection and start obstacle detect---//
	mapProcessThread.join();
	boost::thread obstacleDetectionThread(obstacleDetect, myVelodyne, myMap);
	//--waiting for curb and start feature matching---//
	curbDetectThread.join();
	myLocalizer->featureMatching(myVelodyne->curbPointsBeamModel, myMap->localCurbMap);// 10ms

	//myVelodyne->createVeloPointCloud(velodyne::curbMulti, *myPointCloudArray.cPointCloud[4]);//2 ms
	//--waiting for all thread complete--//
	gridMatchingThread.join();
	obstacleDetectionThread.join();

	myLocalizer->mapObservationFilte();

	
}

/** \brief Initialize vechile state
* \param[in] reference of state
*/
void
stateInitial(stateType &myState)
{
	memset(&myState, 0, sizeof(myState));
	myState.inputMode = '0';
	myState.inertialMode = inertial::offline;
	myState.velodyneMode = velodyne::offline;
	myState.timeConsume = 0;

	myState.offlineFileIndex = 1100;
	myState.offlineUpdateFlag = 1;

	myState.onlineFrameIndex = 0;
	myState.quitFlag = 0;
	
}

/** \brief Output interaction information
* \param[in] reference of state
*/
void 
userOperationGuide(stateType &myState)
{
	std::cout << "'1'离线模式，'2'在线模式" << std::endl;
	std::cin >> myState.inputMode;
	// 离线模式
	if ('1' == myState.inputMode)
	{
		myState.velodyneMode = velodyne::offline;
		myState.inertialMode = inertial::offline;
	}
	// 在线模式
	else if ('2' == myState.inputMode)
	{
		myState.velodyneMode = velodyne::online;
		myState.inertialMode = inertial::online;
	}
	else
	{
		std::cout << "输入错误，程序退出." << std::endl;
		Sleep(1000);
		exit(1);
	}
}

/** \brief Initialize sensor
* \param[in] pointer of inertial object
* \param[in] pointer of velodyne object
* \param[in] reference of state
*/
void
sensorInitial(inertial *myInertial, velodyne *myVelodyne, stateType &myState)
{
	//-------Inertial2+ sensor-------//
	myInertial->setOnlineDataSaveFileName("D:\\UGV\\Data\\Record9\\GPS\\", ".txt");

	myInertial->setFileName("G:\\SelfRecordData\\Data\\Record7\\GPS\\", ".txt");
	myInertial->readFromFile(myState.offlineFileIndex);
	
	//-------Velodyne sensor-------//
	myVelodyne->setOnlineDataSaveFileName("D:\\UGV\\Data\\Record9\\Velo\\", ".bin");

	myVelodyne->setFileName("G:\\SelfRecordData\\Data\\Record7\\Velo\\", ".bin");
	myVelodyne->readFromFile(myState.offlineFileIndex);

	Sleep(100);
}

/** \brief Set display vechile state
* \param[in] reference of pclDisplayObject
*/
void 
addDisplayText(pclDisp &pclDisplay)
{
	pclDisplay.menuTextAdd("Compute Time:");
	pclDisplay.menuTextAdd("GPS State:");
	pclDisplay.menuTextAdd("SatelliteNum:");
	pclDisplay.menuTextAdd("Latitude:");
	pclDisplay.menuTextAdd("Longitude:");
	pclDisplay.menuTextAdd("Velocity:");
	pclDisplay.menuTextAdd("Pos Accuracy:");
}

/** \brief tran variable to string and display
* \param[in] reference of pclDisplayObject
* \param[in] reference of inertialData
* \param[in] reference of state
*/
void 
DisplayTextVariable(pclDisp &pclDisplay, InertialType &inetialData, stateType &myState)
{
	std::vector<std::string> dispVarString;
	dispVarString.resize(7);

	char transStr[100] = "";

	sprintf_s(transStr, 100, " %.3lf ms", myState.timeConsume);
	dispVarString[0] = transStr;
	dispVarString[1] = inertial::returnGPSStateString(inetialData.mPosmode);

	sprintf_s(transStr, 100, " %.3lf", inetialData.mNumsats);
	dispVarString[2] = transStr;

	sprintf_s(transStr, 100, " %.6lf deg", inetialData.mLat);
	dispVarString[3] = transStr;

	sprintf_s(transStr, 100, " %.6lf deg", inetialData.mLon);
	dispVarString[4] = transStr;

	sprintf_s(transStr, 100, " %.3lf km/h", inetialData.mVf*3.6);
	dispVarString[5] = transStr;

	sprintf_s(transStr, 100, " %.3lf m", inetialData.mPos_accuracy);
	dispVarString[6] = transStr;

	pclDisplay.menuTextDisplay(dispVarString);
}

/** \brief pcl keyboard callback
* \param[in] keyboard event
* \param[in] state variable pointer
*/
void
keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event, void* statePtr)
{
	stateType *myStatePtr = (stateType*)statePtr;
	// "w" key down
	if (event.getKeySym() == "w" && event.keyDown())
	{
		myStatePtr->offlineFileIndex++;
		myStatePtr->offlineUpdateFlag = 1;
	}
	// "s" key down
	else if (event.getKeySym() == "s" && event.keyDown())
	{
		myStatePtr->offlineFileIndex--;
		myStatePtr->offlineUpdateFlag = 1;
	}
	// "q" quit
	else if (event.getKeySym() == "q")
	{
		myStatePtr->quitFlag = 1;
	}
}

/** \brief time manager
* \param[in] flag = 1 : start, flag = 2 : end and output
*/
double 
timeCalculate(int flag)
{
	LARGE_INTEGER nFreq;
	static LARGE_INTEGER nBeginTime;
	static LARGE_INTEGER nEndTime;
	double timeMs = 0;

	QueryPerformanceFrequency(&nFreq);
	// get start time
	if (1 == flag)
	{
		QueryPerformanceCounter(&nBeginTime);
	}
	// get end time and output
	else if (2 == flag)
	{
		QueryPerformanceCounter(&nEndTime);
		timeMs = (double)(nEndTime.QuadPart - nBeginTime.QuadPart) * 1000 / (double)nFreq.QuadPart;
	}

	return timeMs;
}