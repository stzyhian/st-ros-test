#include "pclDisplay.h"

/** \brief Constructor
* \param[in] setWinH	Display Windows Height
* \param[in] setWinW	Display Windows Width
*/
pclDisp::pclDisp(int setWinH, int setWinW)
{
	windowsHeight = setWinH;
	windowsWidth =  setWinW;

	fontSize = 16;
	widthIndentation = 250;

	displayString.clear();
	displayVariableString.clear();
	displayStringPos = 1;

	viewer = NULL;
}

pclDisp::~pclDisp()
{
	if (NULL != viewer)	delete viewer;
}

/** \brief Set windows prefix name
* \param[in] winPreName	Display Windows Prefix Name
*/
void
pclDisp::setWinPreName(std::string winPreName)
{
	windowsPrefixName = winPreName;
}

/** \brief Set windows suffix name
* \param[in] winSufName	Display Windows Suffix Name
*/
void
pclDisp::setWinSufName(std::string winSufName)
{
	windowsSuffixName = winSufName;
}

/** \brief Set text display parameter
* \param[in] font size
* \param[in] width indentation
*/
void
pclDisp::setTextDisplayPara(int setFontSize, int setWidthIndentation)
{
	fontSize = setFontSize;
	widthIndentation = setWidthIndentation;
}

/** \brief Load vehicle 3D model
* \param[in] filename	vehicle model file path
*/
void 
pclDisp::loadVechileModel(char filename[])
{
	pcl::io::loadPolygonFile(filename, vehicleMesh);
}

/** \brief pcl Visualizer Initial
*/
void
pclDisp::pclVisualizerInit(void)
{
	viewer = new pcl::visualization::PCLVisualizer(windowsPrefixName.c_str());

	viewer->initCameraParameters();

	viewer->setCameraPosition(0.0, 35.0, 0.0, -1.0, 0.0, 0.0, 0);

	viewer->setSize(windowsWidth, windowsHeight);

	viewer->addCoordinateSystem(1.0, "coordinate", 0);

	viewer->addPolygonMesh(vehicleMesh);
}

/** \brief pcl Visualizer Initial
* \param[in] keyboard call back function pointer
* \param[in] variable input pointer
*/
void
pclDisp::pclVisualizerInit(void (*keyboardEventOccurred)(const pcl::visualization::KeyboardEvent &event, void* statePtr), void *state)
{
	viewer = new pcl::visualization::PCLVisualizer(windowsPrefixName.c_str());

	viewer->initCameraParameters();

	viewer->setCameraPosition(0.0, 35.0, 0.0, -1.0, 0.0, 0.0, 0);

	viewer->setSize(windowsWidth, windowsHeight);

	viewer->addCoordinateSystem(1.0, "coordinate", 0);

	viewer->registerKeyboardCallback(keyboardEventOccurred, state);

	viewer->addPolygonMesh(vehicleMesh);
}

/** \brief pcl Visualizer Initial
* \param[in] keyboard call back function pointer
* \param[in] variable input pointer
* \param[in] the x coordinate of the camera location
* \param[in] the y coordinate of the camera location
* \param[in] the z coordinate of the camera location
* \param[in] the x component of the view up direction of the camera
* \param[in] the y component of the view up direction of the camera
* \param[in] the z component of the view up direction of the camera
*/
void 
pclDisp::pclVisualizerInit(void(*keyboardEventOccurred)(const pcl::visualization::KeyboardEvent &event, void* statePtr), void *state,
						   double cameraPosX, double cameraPosY, double cameraPosZ, double cameraUpX, double cameraUpY, double cameraUpZ)
{
	viewer = new pcl::visualization::PCLVisualizer(windowsPrefixName.c_str());

	viewer->initCameraParameters();

	viewer->setCameraPosition(cameraPosX, cameraPosY, cameraPosZ, cameraUpX, cameraUpY, cameraUpZ, 0);

	viewer->setSize(windowsWidth, windowsHeight);

	viewer->addCoordinateSystem(1.0, "coordinate", 0);

	viewer->registerKeyboardCallback(keyboardEventOccurred, state);

	viewer->addPolygonMesh(vehicleMesh);
}

/** \brief Add display text
* \param[in] text string
*/
int
pclDisp::menuTextAdd(std::string addString)
{
	
	if (NULL == viewer)
	{
		std::cout << "No viewer to visualise!" << std::endl;
		return -1;
	}

	displayString.insert(displayString.end(), addString);
	viewer->addText(addString, windowsWidth - widthIndentation, windowsHeight - displayStringPos*fontSize - 10, fontSize, 1, 1, 1, addString, 0);
	displayStringPos++;

	return 0;
}

/** \brief Display the variable
* \param[in] variable string
*/
int 
pclDisp::menuTextDisplay(std::vector<std::string> VariableString)
{
	displayVariableString = VariableString;

	if (NULL == viewer)
	{
		std::cout << "No viewer to visualise!" << std::endl;
		return -1;
	}

	if (VariableString.size() != displayString.size())
	{
		std::cout << "Variable string not equal to display string!" << std::endl;
		return -1;
	}

	displayStringPos = 1;
	for (int i = 0; i < VariableString.size(); i++)
	{
		viewer->updateText(displayString[i] + VariableString[i], windowsWidth - widthIndentation, windowsHeight - displayStringPos*fontSize - 10, fontSize, 1, 1, 1, displayString[i]);
		displayStringPos++;
	}

	return 0;
}

/** \brief Set pointcloud property
* \param[in]	property
* \param[in]	value
* \param[in]	pointcloud id
*/
void 
pclDisp::setPointCloudProperty(int property, double value, const std::string &id)
{
	if (NULL == viewer)
	{
		std::cout << "No viewer to visualise!" << std::endl;
		return ;
	}

	viewer->setPointCloudRenderingProperties(property, value, id);
}

/** \brief Refresh the windows
*/
void
pclDisp::pclVisualizerRefresh(void)
{
	if (NULL == viewer)
	{
		std::cout << "No viewer to visualise!" << std::endl;
		return;
	}
	viewer->setWindowName(windowsPrefixName + windowsSuffixName);
	viewer->spinOnce();
}