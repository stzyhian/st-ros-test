#include "inertial2.h"

/** \brief Constructor
* \param[in] set inertial2 mode
*/
inertial::inertial(modeType modeIn)
{
	inertial2Mode = modeIn;

	//-------------public variable------------//
	trajectoryPoints.clear();
	this->dx = 0;
	this->dy = 0;
	this->dh = 0;

	//------------private variable------------//
	inertialRunThread = NULL;
	inertialDataSaveThread = NULL;
	newDataFlag = 0;

	threadStopFlag = false;
	onlineFileIndex = 0;
	
	inertialUDPServer = NULL;
	nrx = NULL;

	//------------inertial data---------------//
	memset(&inertialData, 0, sizeof(inertialData));
	memset(&inertialDataSave, 0, sizeof(inertialDataSave));
	
	multiFrameInertialData.clear();
	
	if (offline == modeIn)
	{

	}
	else if (online == modeIn)
	{
		inertialRunThread = new boost::thread(inertialRun, this);
		if (inertialRunThread == NULL)
		{
			std::cout << "Open inertial thread failed" << std::endl;
			Sleep(500);
			exit(1);
		}
	}
}

inertial::~inertial()
{
	if (online == inertial2Mode)
	{
		threadStopFlag = true;
		Sleep(100);
		if (NULL != inertialRunThread)		delete inertialRunThread;
		if (NULL != inertialDataSaveThread) delete inertialDataSaveThread;	
	}
}

/** \brief Set offline file name
* \param[in] prefix name
* \param[in] suffix name
*/
void 
inertial::setFileName(std::string prefixName, std::string suffixName)
{
	this->prefixName = prefixName;
	this->suffixName = suffixName;
}

/** \brief Set online save file name
* \param[in] prefix name
* \param[in] suffix name
*/
void
inertial::setOnlineDataSaveFileName(std::string prefixName, std::string suffixName)
{
	this->onlineFilePrefixName = prefixName;
	this->onlineFileSuffixName = suffixName;
}

/** \brief Read offline interial data
* \param[in] file index
*/
void 
inertial::readFromFile(int index)
{
	if (online == inertial2Mode)
	{
		return ;
	}

	char indexChar[10] = "";
	sprintf_s(indexChar, "%06d", index);
	std::string indexStr = indexChar;
	std::string fileName = prefixName + indexStr + suffixName;


	FILE* inertialFp;
	fopen_s(&inertialFp, fileName.c_str(), "rt");
	if (NULL == inertialFp)
	{
		std::cout << "Can not open :" << fileName.c_str() << std::endl;
		return ;
	}

	double tempGPSData[30];
	for (int i = 0; i < 30; ++i)
	{
		char gpsStr[20];
		for (int j = 0; j < 20; ++j)
		{
			gpsStr[j] = fgetc(inertialFp);
			if (gpsStr[j] == ' ')
			{
				gpsStr[j] = '\0';
				break;
			}
		}
		tempGPSData[i] = atof(gpsStr);
	}
	fclose(inertialFp);

	inertialData.mLat = tempGPSData[0];
	inertialData.mLon = tempGPSData[1];
	inertialData.mAlt = tempGPSData[2];
	inertialData.mRoll = tempGPSData[3];
	inertialData.mPitch = tempGPSData[4];
	inertialData.mHeading = tempGPSData[5];
	inertialData.mVn = tempGPSData[6];
	inertialData.mVe = tempGPSData[7];
	inertialData.mVf = tempGPSData[8];
	inertialData.mVl = tempGPSData[9];
	inertialData.mVu = tempGPSData[10];
	inertialData.mAx = tempGPSData[11];
	inertialData.mAy = tempGPSData[12];
	inertialData.mAz = tempGPSData[13];
	inertialData.mAf = tempGPSData[14];
	inertialData.mAl = tempGPSData[15];
	inertialData.mAu = tempGPSData[16];
	inertialData.mWx = tempGPSData[17];
	inertialData.mWy = tempGPSData[18];
	inertialData.mWz = tempGPSData[19];
	inertialData.mWf = tempGPSData[20];
	inertialData.mWl = tempGPSData[21];
	inertialData.mWu = tempGPSData[22];
	inertialData.mPos_accuracy = tempGPSData[23];
	inertialData.mVel_accuracy = tempGPSData[24];
	inertialData.mNavstat = tempGPSData[25];
	inertialData.mNumsats = tempGPSData[26];
	inertialData.mPosmode = tempGPSData[27];
	inertialData.mVelmode = tempGPSData[28];
	inertialData.mOrimode = tempGPSData[29];

}

/** \brief get newest data
* \param[in] reference of data
*/
void
inertial::returnIntertialData(InertialType & dataIn ,int saveFlag)
{
	// remove old frame data
	if (multiFrameInertialData.size() >= saveFrameCount)
	{
		multiFrameInertialData.erase(multiFrameInertialData.begin());
	}

	if (offline == inertial2Mode)
	{
		dataIn = inertialData;
		multiFrameInertialData.insert(multiFrameInertialData.end(), inertialData);
		return;
	}
	else if(online == inertial2Mode)
	{
		bool whileFlag = true;
		while (whileFlag)
		{
			// lock mutex
			inertialMutex.lock();
			if (0 == newDataFlag)
			{
				// unlock mutex
				inertialMutex.unlock();
				Sleep(1);
			}
			else
			{
				newDataFlag = 0;
				dataIn = inertialData;
				multiFrameInertialData.insert(multiFrameInertialData.end(), inertialData);
				if (saveFlag)
				{
					inertialDataSave = inertialData;
					callFileSave();
				}

				whileFlag = false;
				// unlock mutex
				inertialMutex.unlock();
			}
		}	
	}
}

/** \brief get GPS state string
* \param[in] position mode
*/
std::string 
inertial::returnGPSStateString(double mPosmode)
{
	std::string returnString = ComGpsXModeName[(int)(mPosmode)];
	return returnString;
}

/** \brief calculate vehcile movement and trajectory in local coordinate
* \param[in] pre  inertial data
* \param[in] post inertial data
*/
void
inertial::movementCalculate(InertialType & preData, InertialType & postData)
{
	inertialProjection.forwardProjectStep1(preData.mLat, preData.mLon);
	inertialProjection.forwardProjectStep2(postData.mLat, postData.mLon, preData.mHeading, this->dx, this->dy);

	this->dh = - postData.mHeading + preData.mHeading;
	if (this->dh > pi)
	{
		this->dh = this->dh - 2 * pi;
	}
	else if (this->dh < -pi)
	{
		this->dh = this->dh + 2 * pi;
	}

	//local trajecotory point calculate
	trajectoryPoints.clear();
	if (multiFrameInertialData.size() <= 0) return;

	inertialProjection.forwardProjectStep1(multiFrameInertialData[multiFrameInertialData.size()-1].mLat, multiFrameInertialData[multiFrameInertialData.size() - 1].mLon);

	for (size_t i = 0; i < multiFrameInertialData.size(); i++)
	{
		TrajectoryType tempTrajectory;
		inertialProjection.forwardProjectStep2(multiFrameInertialData[i].mLat, multiFrameInertialData[i].mLon, multiFrameInertialData[multiFrameInertialData.size() - 1].mHeading, tempTrajectory.x, tempTrajectory.y);
		trajectoryPoints.insert(trajectoryPoints.end(), tempTrajectory);
	}
}

/** \brief Create trajectory point cloud
* \param[in] reference of cloud pointer
*/
void 
inertial::createTrajectoryPointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr & cloudIn)
{
	if (trajectoryPoints.size() <= 0)	return;

	cloudIn->clear();
	cloudIn->width = trajectoryPoints.size();
	cloudIn->height = 1;
	cloudIn->is_dense = false;
	cloudIn->points.resize(cloudIn->width * cloudIn->height);

	for (int i = 0; i < trajectoryPoints.size(); ++i)
	{
		cloudIn->points[i].x = -trajectoryPoints[i].y;
		cloudIn->points[i].y = 0;
		cloudIn->points[i].z = -trajectoryPoints[i].x;
	}
}

/** \brief save one frame inertial data
*/
void
inertial::callFileSave(void)
{
	if (NULL != inertialDataSaveThread)
	{
		delete inertialDataSaveThread;
		inertialDataSaveThread = NULL;
	}
	inertialDataSaveThread = new boost::thread(inertialFileSaveThread, this);
}

/** \brief Inertial data receive thread
* \param[in] inertial class pointer
*/
void 
inertialRun(inertial *inertialPtr)
{
	char buf[512];
	inertialPtr->inertialUDPServer = new UDPServer(3000);
	inertialPtr->nrx = NComCreateNComRxC();

	while (1)
	{
		if (inertialPtr->threadStopFlag)	break;

		inertialPtr->inertialUDPServer->UDPServerReceive(buf, 256);
		NComNewChars(inertialPtr->nrx, (unsigned char *)buf, 512);

		inertialPtr->inertialMutex.lock();//����������

		inertialPtr->inertialData.mLat = inertialPtr->nrx->mLat;
		inertialPtr->inertialData.mLon = inertialPtr->nrx->mLon;
		inertialPtr->inertialData.mAlt = inertialPtr->nrx->mAlt;
		inertialPtr->inertialData.mRoll = (inertialPtr->nrx->mRoll) / 180.0*pi;
		inertialPtr->inertialData.mPitch = (inertialPtr->nrx->mPitch) / 180.0*pi;
		inertialPtr->inertialData.mHeading = (inertialPtr->nrx->mHeading) / 180.0*pi;
		inertialPtr->inertialData.mVn = inertialPtr->nrx->mVn;
		inertialPtr->inertialData.mVe = inertialPtr->nrx->mVe;
		inertialPtr->inertialData.mVf = inertialPtr->nrx->mVf;
		inertialPtr->inertialData.mVl = inertialPtr->nrx->mVl;
		inertialPtr->inertialData.mVu = inertialPtr->nrx->mVd;
		inertialPtr->inertialData.mAx = inertialPtr->nrx->mAx;
		inertialPtr->inertialData.mAy = inertialPtr->nrx->mAy;
		inertialPtr->inertialData.mAz = inertialPtr->nrx->mAz;
		inertialPtr->inertialData.mAf = inertialPtr->nrx->mAf;
		inertialPtr->inertialData.mAl = inertialPtr->nrx->mAl;
		inertialPtr->inertialData.mAu = inertialPtr->nrx->mAd;
		inertialPtr->inertialData.mWx = (inertialPtr->nrx->mWx) / 180.0*pi;
		inertialPtr->inertialData.mWy = (inertialPtr->nrx->mWy) / 180.0*pi;
		inertialPtr->inertialData.mWz = (inertialPtr->nrx->mWz) / 180.0*pi;
		inertialPtr->inertialData.mWf = (inertialPtr->nrx->mWf) / 180.0*pi;
		inertialPtr->inertialData.mWl = (inertialPtr->nrx->mWl) / 180.0*pi;
		inertialPtr->inertialData.mWu = (inertialPtr->nrx->mWd) / 180.0*pi;
		inertialPtr->inertialData.mPos_accuracy = sqrt(inertialPtr->nrx->mNorthAcc*inertialPtr->nrx->mNorthAcc + inertialPtr->nrx->mEastAcc*inertialPtr->nrx->mEastAcc);
		inertialPtr->inertialData.mVel_accuracy = sqrt(inertialPtr->nrx->mVnAcc*inertialPtr->nrx->mVnAcc + inertialPtr->nrx->mVeAcc*inertialPtr->nrx->mVeAcc);
		inertialPtr->inertialData.mNavstat = inertialPtr->nrx->mInsNavMode;
		inertialPtr->inertialData.mNumsats = inertialPtr->nrx->mGpsNumObs;
		inertialPtr->inertialData.mPosmode = inertialPtr->nrx->mGpsPosMode;
		inertialPtr->inertialData.mVelmode = inertialPtr->nrx->mGpsVelMode;
		inertialPtr->inertialData.mOrimode = inertialPtr->nrx->mGpsAttMode;

		inertialPtr->newDataFlag = 1;
		inertialPtr->inertialMutex.unlock();
	}

	NComDestroyNComRxC(inertialPtr->nrx);
	delete inertialPtr->inertialUDPServer;

}

/** \brief Inertial data save thread
* \param[in] inertial class pointer
*/
void
inertialFileSaveThread(inertial *inertialPtr)
{
	double tempGpsData[30];

	tempGpsData[0] = inertialPtr->inertialDataSave.mLat;
	tempGpsData[1] = inertialPtr->inertialDataSave.mLon;
	tempGpsData[2] = inertialPtr->inertialDataSave.mAlt;
	tempGpsData[3] = inertialPtr->inertialDataSave.mRoll;
	tempGpsData[4] = inertialPtr->inertialDataSave.mPitch;
	tempGpsData[5] = inertialPtr->inertialDataSave.mHeading;
	tempGpsData[6] = inertialPtr->inertialDataSave.mVn;
	tempGpsData[7] = inertialPtr->inertialDataSave.mVe;
	tempGpsData[8] = inertialPtr->inertialDataSave.mVf;
	tempGpsData[9] = inertialPtr->inertialDataSave.mVl;
	tempGpsData[10] = inertialPtr->inertialDataSave.mVu;
	tempGpsData[11] = inertialPtr->inertialDataSave.mAx;
	tempGpsData[12] = inertialPtr->inertialDataSave.mAy;
	tempGpsData[13] = inertialPtr->inertialDataSave.mAz;
	tempGpsData[14] = inertialPtr->inertialDataSave.mAf;
	tempGpsData[15] = inertialPtr->inertialDataSave.mAl;
	tempGpsData[16] = inertialPtr->inertialDataSave.mAu;
	tempGpsData[17] = inertialPtr->inertialDataSave.mWx;
	tempGpsData[18] = inertialPtr->inertialDataSave.mWy;
	tempGpsData[19] = inertialPtr->inertialDataSave.mWz;
	tempGpsData[20] = inertialPtr->inertialDataSave.mWf;
	tempGpsData[21] = inertialPtr->inertialDataSave.mWl;
	tempGpsData[22] = inertialPtr->inertialDataSave.mWu;
	tempGpsData[23] = inertialPtr->inertialDataSave.mPos_accuracy;
	tempGpsData[24] = inertialPtr->inertialDataSave.mVel_accuracy;
	tempGpsData[25] = inertialPtr->inertialDataSave.mNavstat;
	tempGpsData[26] = inertialPtr->inertialDataSave.mNumsats;
	tempGpsData[27] = inertialPtr->inertialDataSave.mPosmode;
	tempGpsData[28] = inertialPtr->inertialDataSave.mVelmode;
	tempGpsData[29] = inertialPtr->inertialDataSave.mOrimode;

	FILE *gpsFp = NULL;
	char fileIndex[10] = "";
	sprintf_s(fileIndex, "%06d", inertialPtr->onlineFileIndex);
	std::string fileName = inertialPtr->onlineFilePrefixName + fileIndex + inertialPtr->onlineFileSuffixName;

	fopen_s(&gpsFp, fileName.c_str(), "wt");

	if (NULL == gpsFp)
	{
		std::cout << "Can not open file :" << fileName.c_str() << std::endl;
		return;
	}

	char strGps[200];
	for (int i = 0; i < 30; i++)
	{
		sprintf_s(strGps, "%.13lf", tempGpsData[i]);
		fputs(strGps, gpsFp);
		fputc(' ', gpsFp);
	}
	fclose(gpsFp);


	inertialPtr->onlineFileIndex++;
}