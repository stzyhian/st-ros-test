#include "UDPServer.h"

/** \brief Constructor
* \param[in] udp receive port
*/
UDPServer::UDPServer(int port)
{
	fromLength = 0;
	//start socket lib version 2.0
	int WSRet = -1;
	WSRet = WSAStartup(0x0202, &recvWsdata);
	if (0 != WSRet)
	{
		std::cout << "WS Startup failed" << std::endl;
	}
	
	recvAddr.sin_family = AF_INET;
	recvAddr.sin_addr.s_addr = 0;//INADDR_BROADCAST
	recvAddr.sin_port = htons(port);
	
	recvSocket = socket(AF_INET, SOCK_DGRAM, 0);
	//set socket type
	int setSocketRet = -1;
	setSocketRet = setsockopt(recvSocket, SOL_SOCKET, SO_BROADCAST, (char FAR *)&recvOptval, sizeof(recvOptval));
	if (0 != setSocketRet)
	{
		std::cout << "Set socket failed" << std::endl;
	}
	int bindRet = -1;
	bindRet = bind(recvSocket, (sockaddr *)&recvAddr, sizeof(sockaddr_in));
	if (0 != bindRet)
	{
		std::cout << "Socket bind failed" << std::endl;
	}

	fromLength = sizeof(SOCKADDR);
}
UDPServer::~UDPServer()
{
	closesocket(recvSocket);
	WSACleanup();
}

/** \brief UDP server receive
* \param[in] receive buffer
* \param[in] buffer length
*/
int 
UDPServer::UDPServerReceive(char buf[], int length)
{
	int retReceiveLen = recvfrom(recvSocket, buf, length, 0, (struct sockaddr FAR *)&from, (int FAR *)&fromLength);
	return retReceiveLen;
}