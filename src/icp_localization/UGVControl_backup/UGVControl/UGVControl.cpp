// UGVControl.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "mainCommon.h"

void pclDisplayThread(stateType *myState, pclPointCloudArrayType &myPointCloudArray, localization *myLocalizer);

int main()
{
	pclPointCloudArrayType myPointCloudArray;
	//--------------------------myState initial--------------------------//
	stateType myState;
	stateInitial(myState);
	//--------------------------interaction  menu------------------------//
	userOperationGuide(myState);

	//--------------------------sensor & map initial----------------------//
	inertial *myInertial = new inertial(myState.inertialMode);
	velodyne *myVelodyne = new velodyne(myState.velodyneMode);
	digitalMap *myMap = new digitalMap("DigitalMap\\bin\\CurbMap",
									   "DigitalMap\\bin\\LaneMarkerMap",
									   "DigitalMap\\bin\\ZebraCrossingMap",
									   "DigitalMap\\bin\\GridMap");

	systemInitialize(myState, myMap, myInertial, myVelodyne);
	//-------initial localizer----//
	localization *myLocalizer = new localization(myState.inertialVar);

	//--------------------------set pcl Display -------------------------//
	myPointCloudArray.pointcloudName[0] = "veloRawCloud";
	myPointCloudArray.pointcloudName[1] = "curbMapCloud";
	myPointCloudArray.pointcloudName[2] = "laneMarkerMapCloud";
	myPointCloudArray.pointcloudName[3] = "zebraCrossingMapCloud";
	myPointCloudArray.pointcloudName[4] = "obstacleCloud";
	myPointCloudArray.pointcloudName[5] = "curbCloud";
	//---------pointcloud initial-------//
	myPointCloudArray.veloRawPointCloud = NULL;
	myPointCloudArray.veloRawPointCloud = new veloRawPointT(new pcl::PointCloud<pcl::PointXYZI>);
	myPointCloudArray.veloColorHandler = NULL;
	myPointCloudArray.veloColorHandler = new veloRawHdl(*myPointCloudArray.veloRawPointCloud, "intensity");

	for (int i = 0; i < myPointCloudArray.pointArrayLen; i++)
	{
		myPointCloudArray.cPointCloud[i] = NULL;
		myPointCloudArray.cPointCloud[i] = new cPointT(new pcl::PointCloud<pcl::PointXYZ>);
		myPointCloudArray.ColorHandler[i] = NULL;
	}
	//-------pointcloud handler--------//
	myPointCloudArray.ColorHandler[0] = new cHdl(*myPointCloudArray.cPointCloud[0], 255, 0, 0);
	myPointCloudArray.ColorHandler[1] = new cHdl(*myPointCloudArray.cPointCloud[1], 180, 180, 180);
	myPointCloudArray.ColorHandler[2] = new cHdl(*myPointCloudArray.cPointCloud[2], 180, 180, 180);
	myPointCloudArray.ColorHandler[3] = new cHdl(*myPointCloudArray.cPointCloud[3], 255, 255, 0);
	myPointCloudArray.ColorHandler[4] = new cHdl(*myPointCloudArray.cPointCloud[4], 0, 255, 125);

	//-------create pcl display---//
	boost::thread pclThread(pclDisplayThread, &myState, myPointCloudArray, myLocalizer);
	Sleep(200);

	while (1)
	{
		if (1 == myState.offlineUpdateFlag || '2' == myState.inputMode)
		{

			//myState.offlineUpdateFlag = 0;
			
			//------------------------save pre-frame data--------------------//
			myState.inertialVarPre = myState.inertialVar;

			//-------------------------update sensor data--------------------//
			//----update velodyne data---//
			myVelodyne->readFromFile(myState.offlineFileIndex);
			myVelodyne->copyVeloData();
			//----update inertial data---//
			myInertial->readFromFile(myState.offlineFileIndex);
			myInertial->returnIntertialData(myState.inertialVar);

			//-------------------------data process--------------------------//
			timeCalculate(1);//start time

			localizationAndObstacleDetection(myState, myMap, myInertial, myVelodyne, myLocalizer);
			
			//--------------------pcl point cloud creation-------------------//
			myState.Mutex.lock();// lock mutex

			myVelodyne->createVeloPointCloud(velodyne::raw, *myPointCloudArray.veloRawPointCloud);
			myPointCloudArray.veloColorHandler->setInputCloud(*myPointCloudArray.veloRawPointCloud);

			myVelodyne->createVeloPointCloud(velodyne::obstacle, *myPointCloudArray.cPointCloud[3]);
			//myVelodyne->createVeloPointCloud(velodyne::curbBeam, *myPointCloudArray.cPointCloud[4]);
			myMap->createMapPointCloud(*myPointCloudArray.cPointCloud[0], digitalMap::curb, 0.75e-3, 0.75e-3, myLocalizer->vehiclePosition.mLat, myLocalizer->vehiclePosition.mLon, myLocalizer->vehiclePosition.mHeading);
			myMap->createMapPointCloud(*myPointCloudArray.cPointCloud[1], digitalMap::laneMarker, 0.75e-3, 0.75e-3, myLocalizer->vehiclePosition.mLat, myLocalizer->vehiclePosition.mLon, myLocalizer->vehiclePosition.mHeading);
			myMap->createMapPointCloud(*myPointCloudArray.cPointCloud[2], digitalMap::zebraCrossing, 0.75e-3, 0.75e-3, myLocalizer->vehiclePosition.mLat, myLocalizer->vehiclePosition.mLon, myLocalizer->vehiclePosition.mHeading);

			if ('1' == myState.inputMode)
			{
				if (myLocalizer->featureMapMatchingFlag == -1 && myLocalizer->gridMapMatchingFlag == 1)
				{
					sprintf_s(myState.winSufName, "%d  F", myState.offlineFileIndex);
				}
				else if (myLocalizer->featureMapMatchingFlag == 1 && myLocalizer->gridMapMatchingFlag == -1)
				{
					sprintf_s(myState.winSufName, "%d  G", myState.offlineFileIndex);
				}
				else if (myLocalizer->featureMapMatchingFlag == -1 && myLocalizer->gridMapMatchingFlag == -1)
				{
					sprintf_s(myState.winSufName, "%d  FG", myState.offlineFileIndex);
				}
				else
				{
					sprintf_s(myState.winSufName, "%d", myState.offlineFileIndex);
				}
				
				myState.offlineFileIndex++;
			}
			else if ('2' == myState.inputMode)
			{
				sprintf_s(myState.winSufName, "%d", myState.onlineFrameIndex);
				myState.onlineFrameIndex++;
			}
			
			//end time
			myState.timeConsume = timeCalculate(2);
			
			myState.dispUpdateFlag = 1;

			myState.Mutex.unlock();//unlock mutex
			
			
		}

		if (1 == myState.quitFlag)	break;
	}

	if (NULL != myInertial)	 delete myInertial;
	if (NULL != myVelodyne)  delete myVelodyne;
	if (NULL != myMap)	 delete myMap;
	if (NULL != myLocalizer) delete myLocalizer;

	//-----pointcloud delete----//
	if (NULL != myPointCloudArray.veloRawPointCloud) delete myPointCloudArray.veloRawPointCloud;
	if (NULL != myPointCloudArray.veloColorHandler) delete myPointCloudArray.veloColorHandler;
	for (int i = 0; i < myPointCloudArray.pointArrayLen; i++)
	{
		if (NULL != myPointCloudArray.cPointCloud[i]) delete myPointCloudArray.cPointCloud[i];
		if (NULL != myPointCloudArray.ColorHandler[i]) delete myPointCloudArray.ColorHandler[i];
	}
    return 0;
}


void pclDisplayThread(stateType *myState, pclPointCloudArrayType &myPointCloudArray, localization *myLocalizer)
{
	//--------------------------pcl Display initial----------------------//
	pclDisp myPclDisplay(1000, 1920);
	//--------load 3D vehicle model-----//
	myPclDisplay.loadVechileModel("VehicleModel\\car.obj");

	if ('1' == myState->inputMode)	myPclDisplay.setWinPreName("离线模式:Frame ");
	if ('2' == myState->inputMode)	myPclDisplay.setWinPreName("在线模式:Frame ");

	//-------windows create----------//
	myPclDisplay.pclVisualizerInit(&keyboardEventOccurred, &myState);
	//----add text information----//
	addDisplayText(myPclDisplay);

	//------add pointcloud--------//
	myPclDisplay.pointCloudDisplayAdd<pcl::PointXYZI>(*myPointCloudArray.veloRawPointCloud, *myPointCloudArray.veloColorHandler, myPointCloudArray.pointcloudName[0]);
	for (int i = 0; i < myPointCloudArray.pointArrayLen; i++)
	{
		myPclDisplay.pointCloudDisplayAdd<pcl::PointXYZ>(*myPointCloudArray.cPointCloud[i], *myPointCloudArray.ColorHandler[i], myPointCloudArray.pointcloudName[i + 1]);
	}
	//------set point size-------//
	myPclDisplay.setPointCloudProperty(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, myPointCloudArray.pointcloudName[1]);
	myPclDisplay.setPointCloudProperty(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, myPointCloudArray.pointcloudName[2]);
	myPclDisplay.setPointCloudProperty(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, myPointCloudArray.pointcloudName[3]);
	myPclDisplay.setPointCloudProperty(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 8, myPointCloudArray.pointcloudName[4]);
	myPclDisplay.setPointCloudProperty(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 8, myPointCloudArray.pointcloudName[5]);

	while (1)
	{
		bool whileFlag = true;
		while (whileFlag)
		{
			myState->Mutex.lock();
			if (1 == myState->dispUpdateFlag)
			{
				myState->dispUpdateFlag = 0;
				whileFlag = false;
			}
			else
			{
				myState->Mutex.unlock();
				Sleep(5);
			}
		}

		//-----------------------pcl display func------------------------//
		//-----display pointcloud----//

		myPclDisplay.pointCloudDisplayUpdate<pcl::PointXYZI>(*myPointCloudArray.veloRawPointCloud, *myPointCloudArray.veloColorHandler, myPointCloudArray.pointcloudName[0]);

		for (int i = 0; i < myPointCloudArray.pointArrayLen; i++)
		{
			myPclDisplay.pointCloudDisplayUpdate<pcl::PointXYZ>(*myPointCloudArray.cPointCloud[i], *myPointCloudArray.ColorHandler[i], myPointCloudArray.pointcloudName[i + 1]);
		}

		myPclDisplay.setWinSufName(myState->winSufName);
		//-----display text variable-//
		DisplayTextVariable(myPclDisplay, myLocalizer->vehiclePosition, *myState);

		myState->Mutex.unlock();
		//----refresh screen--------//
		myPclDisplay.pclVisualizerRefresh();
	}

	return;
}

