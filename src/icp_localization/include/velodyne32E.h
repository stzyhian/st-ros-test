//============================================================================================================
//!
//! \file velodyne32E.h
//!
//! \brief velodyne process file.
//!
//============================================================================================================
#ifndef __VELODYNE32E_H
#define __VELODYNE32E_H

#include <stdio.h>
#include <iostream>

#include <cv.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <cxcore.h>
#include <highgui.h>

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "veloPlaneFitting.h"

#include "inertial2.h"
#include "UDPServer.h"
#include "gaussianProjection.h"

#define  pii 3.1415926535897932384626433832795
#define  Ang2Rad 0.01745329251994

#define  VeloDataSizeMax 70000
#define  LineNum 32

typedef struct
{
	float  x[VeloDataSizeMax];
	float  y[VeloDataSizeMax];
	float  z[VeloDataSizeMax];
	float  r[VeloDataSizeMax];
	float id[VeloDataSizeMax];
	int   size;
} VeloType;

typedef struct
{
	float x;
	float y;
	float z;
	float r;
	float h;
	int age;
} VeloPointT;

typedef struct
{
	double lat;
	double lon;
	double r;
}GpsPointT;

typedef struct
{
	double gridLatStart;
	double gridLatEnd;
	double gridLonStart;
	double gridLonEnd;
	int gridWidth;
	int gridHeight;

	float** gridMapAve;
	float** gridMapVar;
	float** gridMapNum;
}veloGridParaT;

class velodyne
{

public:
	// velodyne operation mode
	enum modeType { offline = 1, online };
	// velodyne pointcloud type
	enum cloudType { raw = 1, curbSingle, curbMulti, curbBeam, obstacle };

public:
	/** \brief Constructor
	* \param[in] set velodyne operation mode
	*/
	velodyne(enum modeType modeIn);
	~velodyne();

	/** \brief Set offline file name
	* \param[in] prefix name
	* \param[in] suffix name
	*/
	void
		setFileName(std::string prefixName, std::string suffixName);

	/** \brief Set online save file name
	* \param[in] prefix name
	* \param[in] suffix name
	*/
	void
		setOnlineDataSaveFileName(std::string prefixName, std::string suffixName);

	/** \brief Set bound parameters
	* \param[in] x min
	* \param[in] x max
	* \param[in] y min
	* \param[in] y max
	*/
	void
		setBoundPara(double setXMin = -25.0, double setXMax = 25.0, double setYMin = -25.0, double setYMax = 25.0);

	/** \brief Read offline interial data
	* \param[in] file index
	*/
	void
		readFromFile(int index);

	/** \brief copy velo thread data
	*/
	void
		copyVeloData(int saveFlag = 0);

	/** \brief fix coordinate calibration
	* \param[in] yaw angle
	* \param[in] pitch angle
	* \param[in] roll angle
	*/
	void
		fixCalibration(double yawFix, double pitchFix, double rollFix);

	/** \brief vehicle posture coordinate calibration
	* \param[in] pitch angle
	* \param[in] roll angle
	*/
	void
		posCalibration(double pitch, double roll);

	/** \brief separate and bounding the pointcloud
	* \param[in] bounding zLim1
	* \param[in] bounding zLim2
	*/
	void
		dividePoints(double zLim1 = -1.4, double zLim2 = -1.8);

	/** \brief separate the obstacle pointcloud
	* \param[in] bounding zLim
	*/
	void divideObstaclePoints(double zLim = 0);

	/** \brief curb detection
	*/
	void
		performCurbDetection(void);

	/** \brief generate multi frame curbs
	* \param[in] bounding dx
	* \param[in] bounding dy
	* \param[in] bounding dh
	*/
	void
		generateMultiFrameCurbs(double dx, double dy, double dh);

	/** \brief generate multi frame curbs
	* \param[in] lat original
	* \param[in] lon original
	* \param[in] heading
	*/
	void
		generateGpsPoints(double latCenter, double lonCenter, double heading);


	/** \brief contour beam model extract
	* \param[in] vehicle local trajectory points
	*/
	void
		beamModelExtraction(std::vector<TrajectoryType> trajectoryPoints);

	/** \brief drivable area calculate
	* \param[in] local curb map
	*/
	void
		createDrivableArea(std::vector<VeloPointT> localCurbMapDrivable);

	/** \brief compute drivable velo points 
	*/
	void 
		computeDrivablePoints(void);

	/** \brief detect the obstacle within drivable area
	*/
	void 
		obstacleDetect(void);

	/** \brief Velodyne data receive thread
	* \param[in] velo class pointer
	*/
	friend void
		veloRun(velodyne *veloPtr);

	/** \brief Velodyne data save thread
	* \param[in] velo class pointer
	*/
	friend void
		veloFileSaveThread(velodyne *veloPtr);
private:
	/** \brief save one frame inertial data
	*/
	void
		callFileSave(void);

	/** \brief udp package praser
	* \param[in] udp package buffer
	*/
	void
		packagePrase(char buf[1206]);
	/** \brief set velo angle
	*/
	void
		xmlCorrection(void);

public:

	// velodyne newest raw point data 
	VeloType veloNewestData;

	// velodyne ground point data
	VeloType veloGroundData;
	std::vector<GpsPointT>  GroundGpsPoints;

	// velodyne line point data
	std::vector<VeloPointT> lineVeloPoints[LineNum];

	// detected one frame curb point data
	std::vector<VeloPointT> curbPoints;

	// multi-frame curb point data
	std::vector<VeloPointT> curbPointsMulti;

	// beam model curb point data
	std::vector<VeloPointT> curbPointsBeamModel;

	// velo grid
	veloGridParaT veloGridPara;

	//---obstacle detector
	std::vector<VeloPointT> detectedObstaclePoints;

	double veloObstaclePoints[5][VeloDataSizeMax];
	double veloObstaclePointsCopy[5][VeloDataSizeMax];
	int    veloObstaclePointsLen;
	int    veloObstaclePointsLenCopy;


	double totalObstaclePoints[5][VeloDataSizeMax];
	int    totalObstaclePointsLen;

	const double obsXMin = -40;
	const double obsXMax = 40;
	const double obsYMin = -30;
	const double obsYMax = 50;

private:
	// velodyne operation mode
	enum modeType veloMode;

	std::string prefixName;
	std::string suffixName;

	// online save file name 
	std::string onlineFilePrefixName;
	std::string onlineFileSuffixName;

	boost::thread *veloDataSaveThread;
	boost::thread *veloRunThread;
	boost::mutex  veloMutex;
	bool          threadStopFlag;
	int			  onlineFileIndex;

	UDPServer     *veloUDPServer;

	//---obstacle detector
	const double resolution = 0.2;

	double p00;
	double p01;
	double p10;

	int drivableGridHeight;
	int drivableGridWidth;

	cv::Mat* drivableAreaImgErodeMat;
	IplImage *drivableAreaImg;
	IplImage *drivableAreaImgErode;

	//---data & parameter
	VeloType veloData[2];
	int valid_index;
	int invalid_index;
	int newDataFlag;

	double boundXMin;
	double boundXMax;
	double boundYMin;
	double boundYMax;

	float xmlData[64];
	const int saveMultiFrameCount = 30;


	gaussianProjection veloProjection;
};

#endif //__VELODYNE32E_H
