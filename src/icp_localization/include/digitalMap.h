//============================================================================================================
//!
//! \file digitalMap.h
//!
//! \brief digitalMap process file.
//!
//============================================================================================================
#ifndef __DIGITALMAP_H
#define __DIGITALMAP_H

#include <stdio.h>
#include <iostream>
#include <vector>

#include "inertial2.h"
#include "gaussianProjection.h"
#include "velodyne32E.h"

typedef struct
{
	int gridMapRow;
	int gridMapCol;
	float **gridMapAve;
	float **gridMapNum;
	float **gridMapVar;

	double latStart;
	double latEnd;
	int    latLength;
	double latResolution;

	double lonStart;
	double lonEnd;
	int    lonLength;
	double lonResolution;

	double resolution;

}gridMapParaT;

class digitalMap
{
public:
	// map type
	enum mapType { curb = 1, laneMarker, zebraCrossing, grid };

public:
	/** \brief Constructor
	* \param[in] curb map prefix path
	* \param[in] lanemarker map prefix path
	* \param[in] zebraCross map prefix path
	* \param[in] grid       map prefix path
	*/
	digitalMap(std::string curbMapPath = "DigitalMap\\bin\\CurbMap",
		std::string laneMapPath = "DigitalMap\\bin\\LaneMarkerMap",
		std::string zebraMapPath = "DigitalMap\\bin\\ZebraCrossingMap",
		std::string gridMapPath = "DigitalMap\\bin\\GridMap");
	~digitalMap();

	/** \brief read digital map
	* \param[in] lat coordinate file suffix path
	* \param[in] lon coordinate file suffix path
	*/
	void
		readMap(std::string suffixLat, std::string suffixLon);

	/** \brief transform global map data into local coordinate
	* \param[in] map type
	* \param[in] lat original
	* \param[in] lon original
	* \param[in] head original
	* \param[in] lat range limit
	* \param[in] lon range limit
	*/
	void
		createLocalMapData(enum mapType mapTypeIn, double latCenter, double lonCenter, double heading, double latRange, double lonRange);

	/** \brief transform global map data into local coordinate
	* \param[in] lat original
	* \param[in] lon original
	* \param[in] head original
	* \param[in] lat range limit
	* \param[in] lon range limit
	* \param[in] xMin
	* \param[in] xMax
	* \param[in] yMin
	* \param[in] yMax
	*/
	void 
		createLocalMapData(double latCenter, double lonCenter, double heading, double latRange, double lonRange, double xMin, double xMax, double yMin, double yMax);

private:
	/** \brief read digital map
	* \param[in] reference of map data
	* \param[in] prefix name
	* \param[in] suffix name
	*/
	void
		readMapFile(std::vector<double> &map, std::string prefix, std::string suffix);

	/** \brief read grid map
	* \param[in] pointer of grid map data
	* \param[in] prefix name
	* \param[in] suffix name
	*/
	void
		readGridMapFile(float **gridMap, std::string prefix, std::string suffix);

	/** \brief read grid map
	* \param[in] prefix name
	* \param[in] suffix name
	*/
	void
		readGridMapConfigFile(std::string prefix, std::string suffix);

public:
	// map vector (local coordinate)
	std::vector<VeloPointT> localCurbMapDrivable;
	std::vector<VeloPointT> localCurbMap;
	std::vector<VeloPointT> localLaneMarkerMap;
	std::vector<VeloPointT> localZebraCrossingMap;

	gridMapParaT gridMapPara;

private:
	// map file prefix path
	std::string curbMapPathPrefix;
	std::string laneMarkerMapPathPrefix;
	std::string zebraCrossingMapPathPrefix;
	std::string gridMapPathPrefix;

	// map vector (global coordinate)
	std::vector<double> curbMap[2];
	std::vector<double> laneMarkerMap[2];
	std::vector<double> zebraCrossingMap[2];

	gaussianProjection mapProjection;

};

#endif //__DIGITALMAP_H
