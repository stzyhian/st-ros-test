//
// File: kalmanDynamic.h
//
// MATLAB Coder version            : 3.2
// C/C++ source code generated on  : 14-Nov-2016 16:18:01
//
#ifndef __KALMANFILTERDYNAMIC_DEBUG_H
#define __KALMANFILTERDYNAMIC_DEBUG_H

// Include Files
#include <cmath>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>


// Function Declarations
extern void SysKF(double P0[9], const double X[3], const double Z[3], double R00,
                  double R11, double R22, double Xkf[3]);

extern void mapKFFusion(double P0[9], const double X[3], const double ZZ[5], int
	flag, double R00, double R11, double R22, double R33, double R44, double Xkf[3]);

#endif

//
// File trailer for SysKF.h
//
// [EOF]
//
