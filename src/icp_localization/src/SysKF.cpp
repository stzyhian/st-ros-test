//
// File: SysKF.cpp
//
// MATLAB Coder version            : 3.2
// C/C++ source code generated on  : 14-Nov-2016 16:18:01
//

// Include Files
#include "SysKF.h"
#include "rtwtypes.h"
// Function Definitions

//
// Arguments    : double P0[9]
//                const double X[3]
//                const double Z[3]
//                double R00
//                double R11
//                double R22
//                double Xkf[3]
// Return Type  : void
//
void SysKF(double P0[9], const double X[3], const double Z[3], double R00,
           double R11, double R22, double Xkf[3])
{
  double a[9];
  double P1[9];
  int p1;
  double Xn[3];
  double b_a[9];
  int p2;
  double b_R00[9];
  int p3;
  double absx11;
  static const signed char c_a[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };

  static const double Q[9] = { 0.05, 0.0, 0.0, 0.0, 0.05, 0.0, 0.0, 0.0, 0.05 };

  double x[9];
  double K[9];
  double absx21;
  double absx31;
  int itmp;
  double b_Z[3];

  // 10
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for (p1 = 0; p1 < 3; p1++) {
    Xn[p1] = 0.0;
    for (p2 = 0; p2 < 3; p2++) {
      a[p1 + 3 * p2] = 0.0;
      for (p3 = 0; p3 < 3; p3++) {
        a[p1 + 3 * p2] += (double)c_a[p1 + 3 * p3] * P0[p3 + 3 * p2];
      }

      Xn[p1] += (double)c_a[p1 + 3 * p2] * X[p2];
    }

    for (p2 = 0; p2 < 3; p2++) {
      absx11 = 0.0;
      for (p3 = 0; p3 < 3; p3++) {
        absx11 += a[p1 + 3 * p3] * (double)c_a[p3 + 3 * p2];
      }

      P1[p1 + 3 * p2] = absx11 + Q[p1 + 3 * p2];
    }
  }

  for (p1 = 0; p1 < 3; p1++) {
    for (p2 = 0; p2 < 3; p2++) {
      a[p1 + 3 * p2] = 0.0;
      for (p3 = 0; p3 < 3; p3++) {
        a[p1 + 3 * p2] += (double)c_a[p1 + 3 * p3] * P1[p3 + 3 * p2];
      }
    }

    for (p2 = 0; p2 < 3; p2++) {
      b_a[p1 + 3 * p2] = 0.0;
      for (p3 = 0; p3 < 3; p3++) {
        b_a[p1 + 3 * p2] += a[p1 + 3 * p3] * (double)c_a[p3 + 3 * p2];
      }
    }
  }

  b_R00[0] = R00;
  b_R00[3] = 0.0;
  b_R00[6] = 0.0;
  b_R00[1] = 0.0;
  b_R00[4] = R11;
  b_R00[7] = 0.0;
  b_R00[2] = 0.0;
  b_R00[5] = 0.0;
  b_R00[8] = R22;
  for (p1 = 0; p1 < 3; p1++) {
    for (p2 = 0; p2 < 3; p2++) {
      x[p2 + 3 * p1] = b_a[p2 + 3 * p1] + b_R00[p2 + 3 * p1];
    }
  }

  memcpy(&K[0], &x[0], 9U * sizeof(double));
  p1 = 0;
  p2 = 3;
  p3 = 6;
  absx11 = std::abs(x[0]);
  absx21 = std::abs(x[1]);
  absx31 = std::abs(x[2]);
  if ((absx21 > absx11) && (absx21 > absx31)) {
    p1 = 3;
    p2 = 0;
    K[0] = x[1];
    K[1] = x[0];
    K[3] = x[4];
    K[4] = x[3];
    K[6] = x[7];
    K[7] = x[6];
  } else {
    if (absx31 > absx11) {
      p1 = 6;
      p3 = 0;
      K[0] = x[2];
      K[2] = x[0];
      K[3] = x[5];
      K[5] = x[3];
      K[6] = x[8];
      K[8] = x[6];
    }
  }

  absx11 = K[1] / K[0];
  K[1] /= K[0];
  absx21 = K[2] / K[0];
  K[2] /= K[0];
  K[4] -= absx11 * K[3];
  K[5] -= absx21 * K[3];
  K[7] -= absx11 * K[6];
  K[8] -= absx21 * K[6];
  if (std::abs(K[5]) > std::abs(K[4])) {
    itmp = p2;
    p2 = p3;
    p3 = itmp;
    K[1] = absx21;
    K[2] = absx11;
    absx11 = K[4];
    K[4] = K[5];
    K[5] = absx11;
    absx11 = K[7];
    K[7] = K[8];
    K[8] = absx11;
  }

  absx11 = K[5] / K[4];
  K[5] /= K[4];
  K[8] -= absx11 * K[7];
  absx11 = (K[5] * K[1] - K[2]) / K[8];
  absx21 = -(K[1] + K[7] * absx11) / K[4];
  a[p1] = ((1.0 - K[3] * absx21) - K[6] * absx11) / K[0];
  a[p1 + 1] = absx21;
  a[p1 + 2] = absx11;
  absx11 = -K[5] / K[8];
  absx21 = (1.0 - K[7] * absx11) / K[4];
  a[p2] = -(K[3] * absx21 + K[6] * absx11) / K[0];
  a[p2 + 1] = absx21;
  a[p2 + 2] = absx11;
  absx11 = 1.0 / K[8];
  absx21 = -K[7] * absx11 / K[4];
  a[p3] = -(K[3] * absx21 + K[6] * absx11) / K[0];
  a[p3 + 1] = absx21;
  a[p3 + 2] = absx11;
  for (p1 = 0; p1 < 3; p1++) {
    for (p2 = 0; p2 < 3; p2++) {
      b_a[p1 + 3 * p2] = 0.0;
      for (p3 = 0; p3 < 3; p3++) {
        b_a[p1 + 3 * p2] += P1[p1 + 3 * p3] * (double)c_a[p3 + 3 * p2];
      }
    }

    absx11 = 0.0;
    for (p2 = 0; p2 < 3; p2++) {
      K[p1 + 3 * p2] = 0.0;
      for (p3 = 0; p3 < 3; p3++) {
        K[p1 + 3 * p2] += b_a[p1 + 3 * p3] * a[p3 + 3 * p2];
      }

      absx11 += (double)c_a[p1 + 3 * p2] * Xn[p2];
    }

    b_Z[p1] = Z[p1] - absx11;
  }

  for (p1 = 0; p1 < 3; p1++) {
    absx11 = 0.0;
    for (p2 = 0; p2 < 3; p2++) {
      absx11 += K[p1 + 3 * p2] * b_Z[p2];
    }

    Xkf[p1] = Xn[p1] + absx11;
  }

  memset(&x[0], 0, 9U * sizeof(double));
  for (p1 = 0; p1 < 3; p1++) {
    x[p1 + 3 * p1] = 1.0;
  }

  for (p1 = 0; p1 < 3; p1++) {
    for (p2 = 0; p2 < 3; p2++) {
      absx11 = 0.0;
      for (p3 = 0; p3 < 3; p3++) {
        absx11 += K[p1 + 3 * p3] * (double)c_a[p3 + 3 * p2];
      }

      a[p1 + 3 * p2] = x[p1 + 3 * p2] - absx11;
    }

    for (p2 = 0; p2 < 3; p2++) {
      P0[p1 + 3 * p2] = 0.0;
      for (p3 = 0; p3 < 3; p3++) {
        P0[p1 + 3 * p2] += a[p1 + 3 * p3] * P1[p3 + 3 * p2];
      }
    }
  }

  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
}
