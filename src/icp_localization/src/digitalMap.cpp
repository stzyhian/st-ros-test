#include "digitalMap.h"

/** \brief Constructor
* \param[in] curb map prefix path
* \param[in] lanemarker map prefix path
* \param[in] zebraCross map prefix path
* \param[in] grid       map prefix path
*/
digitalMap::digitalMap(std::string curbMapPath, std::string laneMapPath, std::string zebraMapPath, std::string gridMapPath)
{
	//---------------------variable initialize-----------------//
	this->curbMapPathPrefix			 = curbMapPath;
	this->laneMarkerMapPathPrefix	 = laneMapPath;
	this->zebraCrossingMapPathPrefix = zebraMapPath;
	this->gridMapPathPrefix			 = gridMapPath;

	gridMapPara.gridMapAve = NULL;
	gridMapPara.gridMapNum = NULL;
	gridMapPara.gridMapVar = NULL;

	localCurbMapDrivable.clear();
	localCurbMap.clear();
	localLaneMarkerMap.clear();
	localZebraCrossingMap.clear();

	//----------read grid Map Configure-----//
	readGridMapConfigFile(gridMapPathPrefix + "Config", ".bin");

	//----------grid map alloc memory-------//
	gridMapPara.gridMapAve = new float*[gridMapPara.gridMapRow];
	gridMapPara.gridMapNum = new float*[gridMapPara.gridMapRow];
	gridMapPara.gridMapVar = new float*[gridMapPara.gridMapRow];
	for (int i = 0; i < gridMapPara.gridMapRow; i++)
	{
		gridMapPara.gridMapAve[i] = new float[gridMapPara.gridMapCol];
		gridMapPara.gridMapNum[i] = new float[gridMapPara.gridMapCol];
		gridMapPara.gridMapVar[i] = new float[gridMapPara.gridMapCol];
	}
}

digitalMap::~digitalMap()
{
	//---------delete grid map memory-------//
	if (NULL != gridMapPara.gridMapAve)
	{
		for (int i = 0; i < gridMapPara.gridMapRow; i++)
		{
			delete[] gridMapPara.gridMapAve[i];
		}
		delete[] gridMapPara.gridMapAve;
	}
	if (NULL != gridMapPara.gridMapNum)
	{
		for (int i = 0; i < gridMapPara.gridMapRow; i++)
		{
			delete[] gridMapPara.gridMapNum[i];
		}
		delete[] gridMapPara.gridMapNum;
	}
	if (NULL != gridMapPara.gridMapVar)
	{
		for (int i = 0; i < gridMapPara.gridMapRow; i++)
		{
			delete[] gridMapPara.gridMapVar[i];
		}
		delete[] gridMapPara.gridMapVar;
	}
}

/** \brief read digital map
* \param[in] lat coordinate file suffix path
* \param[in] lon coordinate file suffix path
*/
void 
digitalMap::readMap(std::string suffixLat, std::string suffixLon)
{
	
	//------------------Curb Map-------------------//
	readMapFile(curbMap[0], curbMapPathPrefix, suffixLat);
	readMapFile(curbMap[1], curbMapPathPrefix, suffixLon);

	//-----------------Lane Marker Map-----------//
	readMapFile(laneMarkerMap[0], laneMarkerMapPathPrefix, suffixLat);
	readMapFile(laneMarkerMap[1], laneMarkerMapPathPrefix, suffixLon);

	//-----------------Zebra Crossing Map-----------//
	readMapFile(zebraCrossingMap[0], zebraCrossingMapPathPrefix, suffixLat);
	readMapFile(zebraCrossingMap[1], zebraCrossingMapPathPrefix, suffixLon);

	//------------------Grid Map--------------------//
	readGridMapFile(gridMapPara.gridMapAve, gridMapPathPrefix + "Ave", ".bin");
	readGridMapFile(gridMapPara.gridMapNum, gridMapPathPrefix + "Num", ".bin");
	readGridMapFile(gridMapPara.gridMapVar, gridMapPathPrefix + "Var", ".bin");
}

/** \brief transform global map data into local coordinate
* \param[in] map type
* \param[in] lat original
* \param[in] lon original
* \param[in] head original
* \param[in] lat range limit
* \param[in] lon range limit
*/
void 
digitalMap::createLocalMapData(mapType mapTypeIn, double latCenter, double lonCenter, double heading, double latRange, double lonRange)
{
	std::vector<double> *mapOutput = NULL;
	std::vector<VeloPointT> *localMapData = NULL;

	if (curb == mapTypeIn)
	{
		mapOutput = curbMap;
		localMapData = &localCurbMap;
	}
	else if (laneMarker == mapTypeIn)
	{
		mapOutput = laneMarkerMap;
		localMapData = &localLaneMarkerMap;
	}
	else if (zebraCrossing == mapTypeIn)
	{
		mapOutput = zebraCrossingMap;
		localMapData = &localZebraCrossingMap;
	}
	else 
	{
		printf("map type error!\n");
		return;
	}

	localMapData->clear();

	mapProjection.forwardProjectStep1(latCenter, lonCenter);

	for (size_t i = 0; i < mapOutput[0].size(); i++)
	{

		double deltaLat = fabs(mapOutput[0][i] - latCenter);
		double deltaLon = fabs(mapOutput[1][i] - lonCenter);
		double tranedX, tranedY;

		if (deltaLat > latRange || deltaLon > lonRange) continue;

		VeloPointT tempPoint;
		memset(&tempPoint, 0, sizeof(tempPoint));

		mapProjection.forwardProjectStep2(mapOutput[0][i], mapOutput[1][i], heading, tranedX, tranedY);
		tempPoint.x = (float)(tranedX);
		tempPoint.y = (float)(tranedY);

		localMapData->insert(localMapData->end(), tempPoint);
	}
}

/** \brief transform global map data into local coordinate
* \param[in] lat original
* \param[in] lon original
* \param[in] head original
* \param[in] lat range limit
* \param[in] lon range limit
* \param[in] xMin
* \param[in] xMax
* \param[in] yMin
* \param[in] yMax
*/
void
digitalMap::createLocalMapData(double latCenter, double lonCenter, double heading, double latRange, double lonRange, double xMin, double xMax, double yMin, double yMax)
{
	std::vector<double> *mapOutput = NULL;
	std::vector<VeloPointT> *localMapData = NULL;

	mapOutput = curbMap;
	localMapData = &localCurbMapDrivable;

	localMapData->clear();

	mapProjection.forwardProjectStep1(latCenter, lonCenter);

	for (size_t i = 0; i < mapOutput[0].size(); i++)
	{

		double deltaLat = fabs(mapOutput[0][i] - latCenter);
		double deltaLon = fabs(mapOutput[1][i] - lonCenter);
		double tranedX, tranedY;

		if (deltaLat > latRange || deltaLon > lonRange) continue;

		mapProjection.forwardProjectStep2(mapOutput[0][i], mapOutput[1][i], heading, tranedX, tranedY);

		if (tranedX > xMax || tranedX<xMin || tranedY>yMax || tranedY < yMin)
		{
			continue;
		}

		VeloPointT tempPoint;
		memset(&tempPoint, 0, sizeof(tempPoint));
		
		tempPoint.x = (float)(tranedX);
		tempPoint.y = (float)(tranedY);

		localMapData->insert(localMapData->end(), tempPoint);
	}

}

/** \brief read digital map
* \param[in] reference of map data
* \param[in] prefix name
* \param[in] suffix name
*/
void 
digitalMap::readMapFile(std::vector<double>& map, std::string prefix, std::string suffix)
{
	FILE *MapPtr = NULL;
	std::string Path = prefix + suffix;

	MapPtr = fopen( Path.c_str(), "rb");
	if (NULL == MapPtr)
	{
		std::cout << "Open Map File failed:" << Path.c_str() << std::endl;
		return;
	}

	double *buf = new double[100000];

	size_t fileSize = fread(buf, sizeof(double), 100000, MapPtr);
	fclose(MapPtr);

	map.resize(fileSize);
	for (size_t i = 0; i < fileSize; ++i)
	{
		map[i] = buf[i];
	}
	delete[] buf;
}

/** \brief read grid map
* \param[in] pointer of grid map data
* \param[in] prefix name
* \param[in] suffix name
*/
void 
digitalMap::readGridMapFile(float **gridMap, std::string prefix, std::string suffix)
{
	FILE *mapPtr = NULL;
	std::string Path = prefix + suffix;

	mapPtr = fopen( Path.c_str(), "rb");
	if (NULL == mapPtr)
	{
		std::cout << "Open Map File failed:" << Path.c_str() << std::endl;
		return;
	}

	for (int i = 0; i < gridMapPara.gridMapRow; i++)
	{
		fread(gridMap[i], sizeof(float), gridMapPara.gridMapCol, mapPtr);
	}

	fclose(mapPtr);
}

/** \brief read grid map
* \param[in] prefix name
* \param[in] suffix name
*/
void
digitalMap::readGridMapConfigFile(std::string prefix, std::string suffix)
{
	FILE *mapPtr = NULL;
	std::string Path = prefix + suffix;

	mapPtr=fopen(Path.c_str(), "rb");
	if (NULL == mapPtr)
	{
		std::cout << "Open Map File failed:" << Path.c_str() << std::endl;
		return;
	}

	fread(&gridMapPara.latStart, sizeof(double), 1, mapPtr);
	fread(&gridMapPara.latEnd, sizeof(double), 1, mapPtr);
	fread(&gridMapPara.latResolution, sizeof(double), 1, mapPtr);
	fread(&gridMapPara.latLength, sizeof(int), 1, mapPtr);

	fread(&gridMapPara.lonStart, sizeof(double), 1, mapPtr);
	fread(&gridMapPara.lonEnd, sizeof(double), 1, mapPtr);
	fread(&gridMapPara.lonResolution, sizeof(double), 1, mapPtr);
	fread(&gridMapPara.lonLength, sizeof(int), 1, mapPtr);

	fread(&gridMapPara.resolution, sizeof(double), 1, mapPtr);

	fread(&gridMapPara.gridMapRow, sizeof(int), 1, mapPtr);
	fread(&gridMapPara.gridMapCol, sizeof(int), 1, mapPtr);

	fclose(mapPtr);
}



