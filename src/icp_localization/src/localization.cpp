#include "localization.h"

/** \brief randomly pick points
* \param[in] reference of src points
* \param[in] reference of dst points
*/
void randPickPoints(std::vector<VeloPointT> &Points, std::vector<VeloPointT> &outputPoints, int pickNumMax)
{
	outputPoints.clear();
	srand((unsigned)time(NULL));

	int pointsSize = Points.size();
	int needNum = int(pointsSize*0.8);
	if (needNum >= pickNumMax)
	{
		needNum = pickNumMax;
	}

	std::vector<int> touch;
	touch.resize(pointsSize, 0);
	int pickedCount = 0;
	int pointIndex = 0;
	while (pickedCount < needNum)
	{
		pointIndex = (rand() % pointsSize);
		if (touch[pointIndex] == 1)
		{
			continue;
		}
		else
		{
			outputPoints.insert(outputPoints.end(), Points[pointIndex]);
			touch[pointIndex] = 1;
			pickedCount++;
		}
	}

}

/** \brief Constructor
* \param[in] initial vehicle position
*/
localization::localization(InertialType vehiclePosition)
{
	//-------vehicle position initial------//
	this->vehiclePosition = vehiclePosition;

	//-------initialize Kalman filter-------//
	sysEstP0[0] = 10000; sysEstP0[1] = 0; sysEstP0[2] = 0; sysEstP0[3] = 0; sysEstP0[4] = 10000; sysEstP0[5] = 0; sysEstP0[6] = 0; sysEstP0[7] = 0; sysEstP0[8] = 10000;
	mapEstP0[0] = 10000; mapEstP0[1] = 0; mapEstP0[2] = 0; mapEstP0[3] = 0; mapEstP0[4] = 10000; mapEstP0[5] = 0; mapEstP0[6] = 0; mapEstP0[7] = 0; mapEstP0[8] = 10000;

	positionEst[0] = vehiclePosition.mLat;
	positionEst[1] = vehiclePosition.mLon;
	positionEst[2] = vehiclePosition.mHeading;

	MapObservation[0] = 0; MapObservation[1] = 0; MapObservation[2] = 0; MapObservation[3] = 0; MapObservation[4] = 0;

	featureMapMatchingError = 0;
	featureMapMatchingFlag = -1;

	gridMapMatchingFlag = -1;

}

/** \brief vehicle dynamic estimation
* \param[in] dx
* \param[in] dy
* \param[in] dh
* \param[in] gps observation
*/
void
localization::dynamicEstimation(double dx, double dy, double dh, InertialType gpsObservation)
{
	vehiclePosition = gpsObservation;

	double localSysEst[3] = { 0 };
	localSysEst[0] = dx;
	localSysEst[1] = dy;
	localSysEst[2] = positionEst[2] - dh;

	localizationProjection.forwardProjectStep1(positionEst[0], positionEst[1]);

	double observationX, observationY;
	localizationProjection.forwardProjectStep2(gpsObservation.mLat, gpsObservation.mLon, positionEst[2], observationX, observationY);

	double vehiclePositionGpsObservation[3] = { 0 };
	vehiclePositionGpsObservation[0] = observationX;
	vehiclePositionGpsObservation[1] = observationY;
	vehiclePositionGpsObservation[2] = gpsObservation.mHeading;

	if (fabs(vehiclePositionGpsObservation[2] - localSysEst[2]) > pii)
	{
		if (vehiclePositionGpsObservation[2] > pii)
		{
			vehiclePositionGpsObservation[2] = vehiclePositionGpsObservation[2] - 2 * pii;
		}
		else
		{
			vehiclePositionGpsObservation[2] = vehiclePositionGpsObservation[2] + 2 * pii;
		}
	}

	double KFSysEst[3] = { 0 };
	SysKF(sysEstP0, localSysEst, vehiclePositionGpsObservation, 600.0, 100.0, 30.0, KFSysEst);

	if (KFSysEst[2] > 2 * pii)
	{
		KFSysEst[2] = KFSysEst[2] - 2 * pii;
	}
	else if (KFSysEst[2] < 0)
	{
		KFSysEst[2] = KFSysEst[2] + 2 * pii;
	}

	localizationProjection.backwardProjectStep1(positionEst[0], positionEst[1]);

	double tranedLat, tranedLon;
	localizationProjection.backwardProjectStep2(KFSysEst[0], KFSysEst[1], positionEst[2], tranedLat, tranedLon);

	positionEst[0] = tranedLat;
	positionEst[1] = tranedLon;
	positionEst[2] = KFSysEst[2];

	vehiclePosition.mLat = positionEst[0];
	vehiclePosition.mLon = positionEst[1];
	vehiclePosition.mHeading = positionEst[2];
}

/** \brief feature points matching
* \param[in] velo curb
* \param[in] map curb
*/
void
localization::featureMatching(std::vector<VeloPointT> &curbPointsVelo, std::vector<VeloPointT> &curbMap)
{
	featureMapMatchingFlag = -1;

	if (curbPointsVelo.size() < 60)
	{
		return;
	}

	// feature map data
	double* M = (double*)calloc(2 * curbMap.size(), sizeof(double));
	for (int i = 0; i < curbMap.size(); i++)
	{
		M[i * 2 + 0] = curbMap[i].x;
		M[i * 2 + 1] = curbMap[i].y;
	}

	// velo features 
	double* TT = (double*)calloc(2 * curbPointsVelo.size(), sizeof(double));
	for (int i = 0; i < curbPointsVelo.size(); i++)
	{
		TT[i * 2 + 0] = curbPointsVelo[i].x;
		TT[i * 2 + 1] = curbPointsVelo[i].y;
	}

	double outlierRatio[ICPIteration] = { 0 };
	double ICPHeading  [ICPIteration] = { 0 };
	double ICPDeltaX   [ICPIteration] = { 0 };
	double ICPDeltaY   [ICPIteration] = { 0 };
	double ICPFitError [ICPIteration] = { 0 };

        //std::cout << "curb size:" << curbPointsVelo.size() <<std::endl;


#pragma omp parallel for
	for (int i = 0; i < ICPIteration; i++)
	{
		std::vector<VeloPointT> pickVeloPoint;
		randPickPoints(curbPointsVelo, pickVeloPoint, 180);

		double* T = (double*)calloc(2 * pickVeloPoint.size(), sizeof(double));
		for (int j = 0; j < pickVeloPoint.size(); j++)
		{
			T[j * 2 + 0] = pickVeloPoint[j].x;
			T[j * 2 + 1] = pickVeloPoint[j].y;
		}

		vector<int32_t> outliers;
		double fitErrorError = 0;
		double fitError = 0;

		Matrix R = Matrix::eye(2);
		Matrix t(2, 1);

		IcpPointToPoint icp(M, curbMap.size(), 2);
		icp.setMaxIterations(10);
		icp.setMinDeltaParam(1e-5);

		icp.fit(T, pickVeloPoint.size(), R, t, 15.0, &fitErrorError, outliers);
		icp.fit(TT, curbPointsVelo.size(), R, t, 2.0, &fitErrorError, outliers);

		icp.setMaxIterations(0);
		icp.fit(TT, curbPointsVelo.size(), R, t, 2.0, &fitErrorError, outliers);
		fitError = fitErrorError;
		int outliersLen1 = outliers.size();

		icp.fit(TT, curbPointsVelo.size(), R, t, 15.0, &fitErrorError, outliers);
		int outliersLen2 = outliers.size();


		outlierRatio[i] = double(outliersLen1 - outliersLen2) / curbPointsVelo.size();

		ICPHeading[i]   = asin(R.val[0][1]);
		ICPDeltaX[i]    = t.val[0][0];
		ICPDeltaY[i]    = t.val[1][0];
		ICPFitError[i]  = fitError / (curbPointsVelo.size() - outliersLen1);

		free(T);
	}
	free(M);
	free(TT);

	double deltaVarSum[3] = { 0 };
	double weightSum = 0;
	featureMapMatchingError = 0;

	for (int i = 0; i < ICPIteration; i++)
	{
		featureMapMatchingError += ICPFitError[i];

		if (outlierRatio[i] <= 0.20)
		{
			double weightTemp = 0;
			weightTemp = pow(exp(-ICPFitError[i]), 6.0);
			weightSum = weightSum + weightTemp;

			deltaVarSum[0] = deltaVarSum[0] + weightTemp*ICPDeltaX[i];
			deltaVarSum[1] = deltaVarSum[1] + weightTemp*ICPDeltaY[i];
			deltaVarSum[2] = deltaVarSum[2] + weightTemp*ICPHeading[i];
		}
	}
	featureMapMatchingError = featureMapMatchingError / ICPIteration;

	if (weightSum > 0.000001)
	{
		MapObservation[0] = deltaVarSum[0] / weightSum;
		MapObservation[1] = deltaVarSum[1] / weightSum;
		MapObservation[2] = positionEst[2] + deltaVarSum[2] / weightSum;

		featureMapMatchingFlag = 1;
	}
	else
	{
		MapObservation[0] = 0;
		MapObservation[1] = 0;
		MapObservation[2] = positionEst[2];
	}

	

}

/** \brief grid map matching
* \param[in] digital gird map
* \param[in] velo grid parameter
* \param[in] velo ground gps points
*/
void
localization::gridMatching(gridMapParaT &gridMap, veloGridParaT &veloGrid, std::vector<GpsPointT>  &GroundGpsPoints)
{
	gridMapMatchingFlag = -1;

	double boundLatMin = vehiclePosition.mLat - 100.0*gridMap.latResolution;
	double boundLatMax = vehiclePosition.mLat + 100.0*gridMap.latResolution;
	double boundLonMin = vehiclePosition.mLon - 100.0*gridMap.lonResolution;
	double boundLonMax = vehiclePosition.mLon + 100.0*gridMap.lonResolution;

	double pointCloudLonMin = 10000;
	double pointCloudLonMax = -10000;
	double pointCloudLatMin = 10000;
	double pointCloudLatMax = -10000;

	for (std::vector<GpsPointT>::iterator it = GroundGpsPoints.begin(); it != GroundGpsPoints.end();)
	{
		if (it->lat<boundLatMin || it->lat>boundLatMax || it->lon<boundLonMin || it->lon>boundLonMax)
		{
			it = GroundGpsPoints.erase(it);
		}
		else
		{
			if (pointCloudLonMin > it->lon)
			{
				pointCloudLonMin = it->lon;
			}
			if (pointCloudLonMax < it->lon)
			{
				pointCloudLonMax = it->lon;
			}

			if (pointCloudLatMin > it->lat)
			{
				pointCloudLatMin = it->lat;
			}
			if (pointCloudLatMax < it->lat)
			{
				pointCloudLatMax = it->lat;
			}
			++it;
		}
	}

	veloGrid.gridLatStart = floor(pointCloudLatMin / gridMap.latResolution)*gridMap.latResolution;
	veloGrid.gridLatEnd   = ceil(pointCloudLatMax / gridMap.latResolution)*gridMap.latResolution;

	veloGrid.gridLonStart = floor(pointCloudLonMin / gridMap.lonResolution)*gridMap.lonResolution;
	veloGrid.gridLonEnd   = ceil(pointCloudLonMax / gridMap.lonResolution)*gridMap.lonResolution;

	veloGrid.gridWidth   = floor((veloGrid.gridLonEnd - veloGrid.gridLonStart) / gridMap.lonResolution) + 2;
	veloGrid.gridHeight  = floor((veloGrid.gridLatEnd - veloGrid.gridLatStart) / gridMap.latResolution) + 2;

	veloGrid.gridMapAve = new float*[veloGrid.gridHeight];
	veloGrid.gridMapNum = new float*[veloGrid.gridHeight];
	veloGrid.gridMapVar = new float*[veloGrid.gridHeight];
	for (int i = 0; i < veloGrid.gridHeight; i++)
	{
		veloGrid.gridMapAve[i] = new float[veloGrid.gridWidth];
		memset(veloGrid.gridMapAve[i], 0, sizeof(float)*veloGrid.gridWidth);

		veloGrid.gridMapNum[i] = new float[veloGrid.gridWidth];
		memset(veloGrid.gridMapNum[i], 0, sizeof(float)*veloGrid.gridWidth);

		veloGrid.gridMapVar[i] = new float[veloGrid.gridWidth];
		memset(veloGrid.gridMapVar[i], 0, sizeof(float)*veloGrid.gridWidth);
	}


	for (int i = 0; i < GroundGpsPoints.size(); i++)
	{
		int indW = (int)(round((GroundGpsPoints[i].lon - veloGrid.gridLonStart) / gridMap.lonResolution));
		int indH = (int)(round((GroundGpsPoints[i].lat - veloGrid.gridLatStart) / gridMap.latResolution));

		veloGrid.gridMapNum[indH][indW]++;
		if (fabs(veloGrid.gridMapNum[indH][indW] - 1) < 0.00001)
		{
			veloGrid.gridMapVar[indH][indW] = 0;
			veloGrid.gridMapAve[indH][indW] = GroundGpsPoints[i].r;
		}
		else
		{
			veloGrid.gridMapVar[indH][indW] = (veloGrid.gridMapNum[indH][indW] - 2) / (veloGrid.gridMapNum[indH][indW] - 1)*veloGrid.gridMapVar[indH][indW] + (GroundGpsPoints[i].r - veloGrid.gridMapAve[indH][indW])*(GroundGpsPoints[i].r - veloGrid.gridMapAve[indH][indW]) / veloGrid.gridMapNum[indH][indW];
			veloGrid.gridMapAve[indH][indW] =  veloGrid.gridMapAve[indH][indW] + (GroundGpsPoints[i].r - veloGrid.gridMapAve[indH][indW]) / veloGrid.gridMapNum[indH][indW];
		}
	}

	int numThreshold =3;
	double mapAlpha = 0.02;
	std::vector<int> tempMapIndH;
	std::vector<int> tempMapIndW;
	tempMapIndH.reserve(2000);
	tempMapIndW.reserve(2000);
	for (int i = 0; i < veloGrid.gridHeight; i++)
	{
		for (int j = 0; j < veloGrid.gridWidth; j++)
		{
			if (veloGrid.gridMapNum[i][j] >= numThreshold)
			{
				tempMapIndH.insert(tempMapIndH.end(), i);
				tempMapIndW.insert(tempMapIndW.end(), j);
			}
		}
	}
	//printf("size=%d\n", tempMapIndW.size());

	if (tempMapIndH.size() >= 1000)
	{
		localizationProjection.backwardProjectStep1(vehiclePosition.mLat, vehiclePosition.mLon);

		std::vector<int> searchLatIntArray;
		std::vector<int> searchLonIntArray;
		for (double lateralX = -0.75; lateralX <= 0.75; lateralX += 0.25)
		{
			for (double longitY = -3.0; longitY <= 3.0; longitY += 0.25)
			{
				double searchLat, searchLon;
				int    searchIntLat, searchIntLon;
				localizationProjection.backwardProjectStep2(lateralX, longitY, vehiclePosition.mHeading, searchLat, searchLon);

				searchLat = searchLat - vehiclePosition.mLat;
				searchLon = searchLon - vehiclePosition.mLon;

				searchIntLat = (int)(round(searchLat / gridMap.latResolution));
				searchIntLon = (int)(round(searchLon / gridMap.lonResolution));

				int newFlag = 1;
				for (size_t i = 0; i < searchLatIntArray.size(); i++)
				{
					if (searchLatIntArray[i] == searchIntLat && searchLonIntArray[i] == searchIntLon)
					{
						newFlag = 0;
						break;
					}
				}
				if (1 == newFlag)
				{
					searchLatIntArray.insert(searchLatIntArray.end(), searchIntLat);
					searchLonIntArray.insert(searchLonIntArray.end(), searchIntLon);
				}
				
			}
		}
		std::vector<double> searchLatArray;
		std::vector<double> searchLonArray;

		searchLatArray.resize(searchLatIntArray.size(), 0);
		searchLonArray.resize(searchLonIntArray.size(), 0);
		for (int i = 0; i < searchLatIntArray.size(); i++)
		{
			searchLatArray[i] = searchLatIntArray[i] * gridMap.latResolution;
			searchLonArray[i] = searchLonIntArray[i] * gridMap.lonResolution;
		}

		std::vector<double> matchProbably;
		matchProbably.resize(searchLatArray.size(), 1);

#pragma omp parallel for
		for (int i = 0; i < searchLatArray.size(); i++)
		{
			double insProb = exp((searchLonArray[i] * searchLonArray[i] + searchLatArray[i] * searchLatArray[i]) / (-2 * (gridMap.lonResolution*20.0)*(gridMap.lonResolution*20.0)));

			int startWidth = round((veloGrid.gridLonStart - gridMap.lonStart + searchLonArray[i]) / gridMap.lonResolution);
			int startHeight = round((veloGrid.gridLatStart - gridMap.latStart + searchLatArray[i]) / gridMap.latResolution);

			int j = startHeight;
			int k = startWidth;

			if (startHeight < 0 || startWidth < 0 || (startHeight + veloGrid.gridHeight) >= gridMap.gridMapRow || (startWidth + veloGrid.gridWidth) >= gridMap.gridMapCol)
			{
				printf("Warnning! Out of Map Data!\n");
				matchProbably[i] = 0;
				continue;
			}

			for (int m = 0; m < tempMapIndH.size(); m++)
			{
				double pointAve = gridMap.gridMapAve[j + tempMapIndH[m]][k + tempMapIndW[m]];
				double pointStd = sqrt(gridMap.gridMapVar[j + tempMapIndH[m]][k + tempMapIndW[m]]);

				double veloPointAve = veloGrid.gridMapAve[tempMapIndH[m]][tempMapIndW[m]];
				double veloPointStd = sqrt(veloGrid.gridMapVar[tempMapIndH[m]][tempMapIndW[m]]);

				double mol = (pointAve - veloPointAve)*(pointAve - veloPointAve);
				double den = (pointStd + veloPointStd + 2.5)*(pointStd + veloPointStd + 2.5);
			
				double mapProb = pow(exp(-0.5*mol / den), mapAlpha);

				matchProbably[i] = matchProbably[i] * mapProb;
			}
			matchProbably[i] = matchProbably[i] * insProb;
		}

		double probSum = 0;
		double weightedDeltaLon = 0;
		double weightedDeltaLat = 0;;
		for (int i = 0; i < matchProbably.size(); i++)
		{
			probSum += matchProbably[i];
			weightedDeltaLat = weightedDeltaLat + searchLatArray[i] * matchProbably[i];
			weightedDeltaLon = weightedDeltaLon + searchLonArray[i] * matchProbably[i];
		}

		weightedDeltaLat = weightedDeltaLat / probSum;
		weightedDeltaLon = weightedDeltaLon / probSum;
		

		double mapEstGlobal[2] = { 0 };
		mapEstGlobal[0] = weightedDeltaLat + vehiclePosition.mLat;
		mapEstGlobal[1] = weightedDeltaLon + vehiclePosition.mLon;

		localizationProjection.forwardProjectStep1(vehiclePosition.mLat, vehiclePosition.mLon);
		localizationProjection.forwardProjectStep2(mapEstGlobal[0], mapEstGlobal[1], vehiclePosition.mHeading, MapObservation[3], MapObservation[4]);

		gridMapMatchingFlag = 1;
	}



	//----------------delete memory------------------//
	for (int i = 0; i < veloGrid.gridHeight; i++)
	{
		delete[] veloGrid.gridMapAve[i];
		delete[] veloGrid.gridMapNum[i];
		delete[] veloGrid.gridMapVar[i];
	}
	delete[] veloGrid.gridMapAve;
	delete[] veloGrid.gridMapNum;
	delete[] veloGrid.gridMapVar;
}

/** \brief map observation filte
*/
void
localization::mapObservationFilte(void)
{
	double mapEst[3] = { 0 };
	mapEst[0] = 0;
	mapEst[1] = 0;
	mapEst[2] = positionEst[2];

	double mapEstPosition[3] = { 0 };

	double R00 = featureMapMatchingError*200.0;
	double R11 = featureMapMatchingError*100.0;
	double R22 = featureMapMatchingError*10000.0;

	double R33 = 600.0;
	double R44 = 50.0;
	//gridMapMatchingFlag = -1;
	//featureMapMatchingFlag = -1;
	if (featureMapMatchingFlag == 1 && gridMapMatchingFlag == -1)
	{
		mapKFFusion(mapEstP0, mapEst, MapObservation, 1, R00, R11, R22, R33, R44, mapEstPosition);
	}
	else if (featureMapMatchingFlag == -1 && gridMapMatchingFlag == 1)
	{
		mapKFFusion(mapEstP0, mapEst, MapObservation, 2, R00, R11, R22, R33, R44, mapEstPosition);
	}
	else if (featureMapMatchingFlag == 1 && gridMapMatchingFlag == 1)
	{
		mapKFFusion(mapEstP0, mapEst, MapObservation, 0, R00, R11, R22, R33, R44, mapEstPosition);
	}
	else
	{
		return;
	}

	if (mapEstPosition[2] > 2 * pii)
	{
		mapEstPosition[2] = mapEstPosition[2] - 2 * pii;
	}
	else if (mapEstPosition[2] < 0)
	{
		mapEstPosition[2] = mapEstPosition[2] + 2 * pii;
	}

	localizationProjection.backwardProjectStep1(positionEst[0], positionEst[1]);

	double tranedLat, tranedLon;
	localizationProjection.backwardProjectStep2(mapEstPosition[0], mapEstPosition[1], positionEst[2], tranedLat, tranedLon);

	positionEst[0] = tranedLat;
	positionEst[1] = tranedLon;
	positionEst[2] = mapEstPosition[2];

	vehiclePosition.mLat = positionEst[0];
	vehiclePosition.mLon = positionEst[1];
	vehiclePosition.mHeading = positionEst[2];
}
