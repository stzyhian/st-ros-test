//
// File: mapKFFusion.cpp
//
// MATLAB Coder version            : 3.2
// C/C++ source code generated on  : 14-Nov-2016 16:30:13
//

// Include Files
#include "mapKFFusion.h"
#include "rtwtypes.h"
// Function Declarations
static void invNxN(const double x_data[], const int x_size[2], double y_data[],
                   int y_size[2]);

// Function Definitions

//
// Arguments    : const double x_data[]
//                const int x_size[2]
//                double y_data[]
//                int y_size[2]
// Return Type  : void
//
static void invNxN(const double x_data[], const int x_size[2], double y_data[],
                   int y_size[2])
{
  int n;
  int yk;
  int i0;
  int minval;
  double A_data[25];
  int ipiv_data[5];
  int k;
  int j;
  int p_data[5];
  int mmj;
  int c;
  int ix;
  double smax;
  int kAcol;
  int i1;
  int jy;
  double s;
  int ijA;
  n = x_size[0];
  y_size[0] = x_size[0];
  y_size[1] = x_size[1];
  yk = x_size[0] * x_size[1];
  for (i0 = 0; i0 < yk; i0++) {
    y_data[i0] = 0.0;
  }

  yk = x_size[0] * x_size[1];
  for (i0 = 0; i0 < yk; i0++) {
    A_data[i0] = x_data[i0];
  }

  minval = x_size[0];
  ipiv_data[0] = 1;
  yk = 1;
  for (k = 2; k <= minval; k++) {
    yk++;
    ipiv_data[k - 1] = yk;
  }

  if (x_size[0] - 1 <= x_size[0]) {
    i0 = x_size[0] - 1;
  } else {
    i0 = x_size[0];
  }

  for (j = 0; j + 1 <= i0; j++) {
    mmj = n - j;
    c = j * (n + 1);
    if (mmj < 1) {
      yk = -1;
    } else {
      yk = 0;
      if (mmj > 1) {
        ix = c;
        smax = std::abs(A_data[c]);
        for (k = 2; k <= mmj; k++) {
          ix++;
          s = std::abs(A_data[ix]);
          if (s > smax) {
            yk = k - 1;
            smax = s;
          }
        }
      }
    }

    if (A_data[c + yk] != 0.0) {
      if (yk != 0) {
        ipiv_data[j] = (j + yk) + 1;
        ix = j;
        yk += j;
        for (k = 1; k <= n; k++) {
          smax = A_data[ix];
          A_data[ix] = A_data[yk];
          A_data[yk] = smax;
          ix += n;
          yk += n;
        }
      }

      i1 = c + mmj;
      for (jy = c + 1; jy + 1 <= i1; jy++) {
        A_data[jy] /= A_data[c];
      }
    }

    yk = n - j;
    kAcol = (c + n) + 1;
    jy = c + n;
    for (k = 1; k < yk; k++) {
      smax = A_data[jy];
      if (A_data[jy] != 0.0) {
        ix = c + 1;
        i1 = mmj + kAcol;
        for (ijA = kAcol; ijA + 1 < i1; ijA++) {
          A_data[ijA] += A_data[ix] * -smax;
          ix++;
        }
      }

      jy += n;
      kAcol += n;
    }
  }

  p_data[0] = 1;
  yk = 1;
  for (k = 2; k <= x_size[0]; k++) {
    yk++;
    p_data[k - 1] = yk;
  }

  for (k = 0; k < minval; k++) {
    if (ipiv_data[k] > 1 + k) {
      yk = p_data[ipiv_data[k] - 1];
      p_data[ipiv_data[k] - 1] = p_data[k];
      p_data[k] = yk;
    }
  }

  for (k = 0; k + 1 <= n; k++) {
    c = p_data[k] - 1;
    y_data[k + y_size[0] * (p_data[k] - 1)] = 1.0;
    for (j = k; j + 1 <= n; j++) {
      if (y_data[j + y_size[0] * c] != 0.0) {
        for (jy = j + 1; jy + 1 <= n; jy++) {
          y_data[jy + y_size[0] * c] -= y_data[j + y_size[0] * c] * A_data[jy +
            x_size[0] * j];
        }
      }
    }
  }

  for (j = 1; j <= n; j++) {
    yk = n * (j - 1);
    for (k = n - 1; k + 1 > 0; k--) {
      kAcol = n * k;
      if (y_data[k + yk] != 0.0) {
        y_data[k + yk] /= A_data[k + kAcol];
        for (jy = 0; jy + 1 <= k; jy++) {
          y_data[jy + yk] -= y_data[k + yk] * A_data[jy + kAcol];
        }
      }
    }
  }
}

//
// Arguments    : double P0[9]
//                const double X[3]
//                const double ZZ[5]
//                int flag
//                double R00
//                double R11
//                double R22
//                double R33
//                double R44
//                double Xkf[3]
// Return Type  : void
//
void mapKFFusion(double P0[9], const double X[3], const double ZZ[5], int flag,
                 double R00, double R11, double R22, double R33, double R44,
                 double Xkf[3])
{
  int Z_size_idx_0;
  double b_R00[25];
  double b_R33[4];
  double c_R00[9];
  int i2;
  int H_size_idx_0;
  double Z_data[5];
  int cr;
  signed char H_data[15];
  static const signed char iv0[6] = { 1, 0, 0, 1, 0, 0 };

  double R_data[25];
  double P1[9];
  static const signed char a[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };

  int c;
  int br;
  int ic;
  double d0;
  double y_data[15];
  int ar;
  static const double Q[9] = { 0.5, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.5 };

  int ib;
  int C_size[2];
  signed char b_data[15];
  int ia;
  static const signed char iv1[15] = { 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0,
    0 };

  double C_data[25];
  double b_y_data[15];
  int R_size[2];
  double K_data[15];
  double b_C_data[5];
  double y[3];
  signed char I[9];
  double b_y[9];
  if (flag == 0) {
    Z_size_idx_0 = 5;
    b_R00[0] = R00;
    b_R00[5] = 0.0;
    b_R00[10] = 0.0;
    b_R00[15] = 0.0;
    b_R00[20] = 0.0;
    b_R00[1] = 0.0;
    b_R00[6] = R11;
    b_R00[11] = 0.0;
    b_R00[16] = 0.0;
    b_R00[21] = 0.0;
    b_R00[2] = 0.0;
    b_R00[7] = 0.0;
    b_R00[12] = R22;
    b_R00[17] = 0.0;
    b_R00[22] = 0.0;
    b_R00[3] = 0.0;
    b_R00[8] = 0.0;
    b_R00[13] = 0.0;
    b_R00[18] = R33;
    b_R00[23] = 0.0;
    b_R00[4] = 0.0;
    b_R00[9] = 0.0;
    b_R00[14] = 0.0;
    b_R00[19] = 0.0;
    b_R00[24] = R44;
    for (i2 = 0; i2 < 5; i2++) {
      Z_data[i2] = ZZ[i2];
      for (cr = 0; cr < 5; cr++) {
        R_data[cr + 5 * i2] = b_R00[cr + 5 * i2];
      }
    }

    H_size_idx_0 = 5;
    for (i2 = 0; i2 < 15; i2++) {
      H_data[i2] = iv1[i2];
    }
  } else if (flag == 1) {
    Z_size_idx_0 = 3;
    c_R00[0] = R00;
    c_R00[3] = 0.0;
    c_R00[6] = 0.0;
    c_R00[1] = 0.0;
    c_R00[4] = R11;
    c_R00[7] = 0.0;
    c_R00[2] = 0.0;
    c_R00[5] = 0.0;
    c_R00[8] = R22;
    for (i2 = 0; i2 < 3; i2++) {
      Z_data[i2] = ZZ[i2];
      for (cr = 0; cr < 3; cr++) {
        R_data[cr + 3 * i2] = c_R00[cr + 3 * i2];
      }
    }

    H_size_idx_0 = 3;
    for (i2 = 0; i2 < 9; i2++) {
      H_data[i2] = a[i2];
    }
  } else {
    Z_size_idx_0 = 2;
    b_R33[0] = R33;
    b_R33[2] = 0.0;
    b_R33[1] = 0.0;
    b_R33[3] = R44;
    for (i2 = 0; i2 < 2; i2++) {
      Z_data[i2] = ZZ[3 + i2];
      for (cr = 0; cr < 2; cr++) {
        R_data[cr + (i2 << 1)] = b_R33[cr + (i2 << 1)];
      }
    }

    H_size_idx_0 = 2;
    for (i2 = 0; i2 < 6; i2++) {
      H_data[i2] = iv0[i2];
    }
  }

  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for (i2 = 0; i2 < 3; i2++) {
    Xkf[i2] = 0.0;
    for (cr = 0; cr < 3; cr++) {
      c_R00[i2 + 3 * cr] = 0.0;
      for (c = 0; c < 3; c++) {
        c_R00[i2 + 3 * cr] += (double)a[i2 + 3 * c] * P0[c + 3 * cr];
      }

      Xkf[i2] += (double)a[i2 + 3 * cr] * X[cr];
    }

    for (cr = 0; cr < 3; cr++) {
      d0 = 0.0;
      for (c = 0; c < 3; c++) {
        d0 += c_R00[i2 + 3 * c] * (double)a[c + 3 * cr];
      }

      P1[i2 + 3 * cr] = d0 + Q[i2 + 3 * cr];
    }

    for (cr = 0; cr < H_size_idx_0; cr++) {
      y_data[cr + H_size_idx_0 * i2] = 0.0;
    }
  }

  c = H_size_idx_0 << 1;
  for (cr = 0; cr <= c; cr += H_size_idx_0) {
    i2 = cr + H_size_idx_0;
    for (ic = cr; ic + 1 <= i2; ic++) {
      y_data[ic] = 0.0;
    }
  }

  br = 0;
  for (cr = 0; cr <= c; cr += H_size_idx_0) {
    ar = -1;
    for (ib = br; ib + 1 <= br + 3; ib++) {
      if (P1[ib] != 0.0) {
        ia = ar;
        i2 = cr + H_size_idx_0;
        for (ic = cr; ic + 1 <= i2; ic++) {
          ia++;
          y_data[ic] += P1[ib] * (double)H_data[ia];
        }
      }

      ar += H_size_idx_0;
    }

    br += 3;
  }

  for (i2 = 0; i2 < H_size_idx_0; i2++) {
    for (cr = 0; cr < 3; cr++) {
      b_data[cr + 3 * i2] = H_data[i2 + H_size_idx_0 * cr];
    }
  }

  C_size[0] = H_size_idx_0;
  C_size[1] = H_size_idx_0;
  for (i2 = 0; i2 < H_size_idx_0; i2++) {
    for (cr = 0; cr < H_size_idx_0; cr++) {
      C_data[cr + H_size_idx_0 * i2] = 0.0;
    }
  }

  c = H_size_idx_0 * (H_size_idx_0 - 1);
  for (cr = 0; cr <= c; cr += H_size_idx_0) {
    i2 = cr + H_size_idx_0;
    for (ic = cr; ic + 1 <= i2; ic++) {
      C_data[ic] = 0.0;
    }
  }

  br = 0;
  for (cr = 0; cr <= c; cr += H_size_idx_0) {
    ar = -1;
    for (ib = br; ib + 1 <= br + 3; ib++) {
      if (b_data[ib] != 0) {
        ia = ar;
        i2 = cr + H_size_idx_0;
        for (ic = cr; ic + 1 <= i2; ic++) {
          ia++;
          C_data[ic] += (double)b_data[ib] * y_data[ia];
        }
      }

      ar += H_size_idx_0;
    }

    br += 3;
  }

  for (i2 = 0; i2 < H_size_idx_0; i2++) {
    for (cr = 0; cr < 3; cr++) {
      b_data[cr + 3 * i2] = H_data[i2 + H_size_idx_0 * cr];
    }
  }

  for (i2 = 0; i2 < H_size_idx_0; i2++) {
    for (cr = 0; cr < 3; cr++) {
      b_y_data[cr + 3 * i2] = 0.0;
    }
  }

  c = 3 * (H_size_idx_0 - 1);
  for (cr = 0; cr <= c; cr += 3) {
    for (ic = cr; ic + 1 <= cr + 3; ic++) {
      b_y_data[ic] = 0.0;
    }
  }

  br = 0;
  for (cr = 0; cr <= c; cr += 3) {
    ar = -1;
    for (ib = br; ib + 1 <= br + 3; ib++) {
      if (b_data[ib] != 0) {
        ia = ar;
        for (ic = cr; ic + 1 <= cr + 3; ic++) {
          ia++;
          b_y_data[ic] += (double)b_data[ib] * P1[ia];
        }
      }

      ar += 3;
    }

    br += 3;
  }

  c = H_size_idx_0 * H_size_idx_0;
  for (i2 = 0; i2 < c; i2++) {
    C_data[i2] += R_data[i2];
  }

  invNxN(C_data, C_size, R_data, R_size);
  c = (signed char)R_size[1];
  for (i2 = 0; i2 < c; i2++) {
    for (cr = 0; cr < 3; cr++) {
      K_data[cr + 3 * i2] = 0.0;
    }
  }

  c = 3 * (R_size[1] - 1);
  for (cr = 0; cr <= c; cr += 3) {
    for (ic = cr; ic + 1 <= cr + 3; ic++) {
      K_data[ic] = 0.0;
    }
  }

  br = 0;
  for (cr = 0; cr <= c; cr += 3) {
    ar = -1;
    i2 = br + H_size_idx_0;
    for (ib = br; ib + 1 <= i2; ib++) {
      if (R_data[ib] != 0.0) {
        ia = ar;
        for (ic = cr; ic + 1 <= cr + 3; ic++) {
          ia++;
          K_data[ic] += R_data[ib] * b_y_data[ia];
        }
      }

      ar += 3;
    }

    br += H_size_idx_0;
  }

  for (i2 = 0; i2 < H_size_idx_0; i2++) {
    b_C_data[i2] = 0.0;
  }

  cr = 0;
  while (cr <= 0) {
    for (ic = 1; ic <= H_size_idx_0; ic++) {
      b_C_data[ic - 1] = 0.0;
    }

    cr = H_size_idx_0;
  }

  br = 0;
  cr = 0;
  while (cr <= 0) {
    ar = -1;
    for (ib = br; ib + 1 <= br + 3; ib++) {
      if (Xkf[ib] != 0.0) {
        ia = ar;
        for (ic = 0; ic + 1 <= H_size_idx_0; ic++) {
          ia++;
          b_C_data[ic] += Xkf[ib] * (double)H_data[ia];
        }
      }

      ar += H_size_idx_0;
    }

    br += 3;
    cr = H_size_idx_0;
  }

  for (i2 = 0; i2 < Z_size_idx_0; i2++) {
    Z_data[i2] -= b_C_data[i2];
  }

  for (ic = 0; ic < 3; ic++) {
    y[ic] = 0.0;
  }

  ar = -1;
  for (ib = 0; ib + 1 <= (signed char)R_size[1]; ib++) {
    if (Z_data[ib] != 0.0) {
      ia = ar;
      for (ic = 0; ic < 3; ic++) {
        ia++;
        y[ic] += Z_data[ib] * K_data[ia];
      }
    }

    ar += 3;
  }

  for (i2 = 0; i2 < 3; i2++) {
    Xkf[i2] += y[i2];
  }

  for (i2 = 0; i2 < 9; i2++) {
    I[i2] = 0;
  }

  for (c = 0; c < 3; c++) {
    I[c + 3 * c] = 1;
  }

  c = (signed char)R_size[1];
  memset(&b_y[0], 0, 9U * sizeof(double));
  for (cr = 0; cr <= 7; cr += 3) {
    for (ic = cr; ic + 1 <= cr + 3; ic++) {
      b_y[ic] = 0.0;
    }
  }

  br = 0;
  for (cr = 0; cr <= 7; cr += 3) {
    ar = -1;
    i2 = br + c;
    for (ib = br; ib + 1 <= i2; ib++) {
      if (H_data[ib] != 0) {
        ia = ar;
        for (ic = cr; ic + 1 <= cr + 3; ic++) {
          ia++;
          b_y[ic] += (double)H_data[ib] * K_data[ia];
        }
      }

      ar += 3;
    }

    br += c;
  }

  for (i2 = 0; i2 < 3; i2++) {
    for (cr = 0; cr < 3; cr++) {
      c_R00[cr + 3 * i2] = (double)I[cr + 3 * i2] - b_y[cr + 3 * i2];
    }
  }

  for (i2 = 0; i2 < 3; i2++) {
    for (cr = 0; cr < 3; cr++) {
      P0[i2 + 3 * cr] = 0.0;
      for (c = 0; c < 3; c++) {
        P0[i2 + 3 * cr] += c_R00[i2 + 3 * c] * P1[c + 3 * cr];
      }
    }
  }

  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
}
