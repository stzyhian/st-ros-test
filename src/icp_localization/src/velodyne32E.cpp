#include "velodyne32E.h"

/** \brief Velodyne data receive thread
* \param[in] velo class pointer
*/
void
veloRun(velodyne *veloPtr)
{
	char buf[1206];
	veloPtr->veloUDPServer = new UDPServer(2368);

	while (1)
	{
		if (veloPtr->threadStopFlag)	break;

		veloPtr->veloUDPServer->UDPServerReceive(buf, 1206);
		veloPtr->packagePrase(buf);
	}

	delete veloPtr->veloUDPServer;
}

/** \brief Velodyne data save thread
* \param[in] velo class pointer
*/
void
veloFileSaveThread(velodyne *veloPtr)
{
	float *tempArray = new float[5 * VeloDataSizeMax];
	int   tempArrayLen = 0;

	for (int i = 0; i < veloPtr->veloNewestData.size; i++)
	{
		tempArray[tempArrayLen] = veloPtr->veloNewestData.x[i];
		tempArray[tempArrayLen + 1] = veloPtr->veloNewestData.y[i];
		tempArray[tempArrayLen + 2] = veloPtr->veloNewestData.z[i];
		tempArray[tempArrayLen + 3] = veloPtr->veloNewestData.r[i];
		tempArray[tempArrayLen + 4] = veloPtr->veloNewestData.id[i];
		tempArrayLen = tempArrayLen + 5;
	}
	
	FILE *veloFp = NULL;
	char fileIndex[10] = "";
	sprintf(fileIndex, "%06d", veloPtr->onlineFileIndex);
	std::string fileName = veloPtr->onlineFilePrefixName + fileIndex + veloPtr->onlineFileSuffixName;

	veloFp=fopen(fileName.c_str(), "wb");
	if (NULL == veloFp)
	{
		std::cout << "Can not open file :" << fileName.c_str() << std::endl;
		return;
	}

	fwrite(tempArray, sizeof(float), 5 * veloPtr->veloNewestData.size, veloFp);
	fclose(veloFp);


	veloPtr->onlineFileIndex++;

	delete[] tempArray;
}


/** \brief Constructor
* \param[in] set velodyne operation mode
*/
velodyne::velodyne(modeType modeIn)
{
	veloMode = modeIn;

	//------------------public variable--------------//
	veloNewestData.size = 0;
	veloGroundData.size = 0;

	GroundGpsPoints.clear();

	for (int i = 0; i < LineNum; i++)
	{
		lineVeloPoints[i].clear();
	}

	curbPoints.clear();
	curbPointsMulti.clear();
	curbPointsBeamModel.clear();

	veloGridPara.gridMapAve = NULL;
	veloGridPara.gridMapNum = NULL;
	veloGridPara.gridMapVar = NULL;

	//-----------------private variable---------------//
	veloDataSaveThread = NULL;
	veloRunThread = NULL;

	threadStopFlag = false;
	onlineFileIndex = 0;

	veloUDPServer = NULL;

	//--------obstacle detector---//
	drivableGridHeight = int(ceil((obsYMax - obsYMin) / resolution) + 3);
	drivableGridWidth = int(ceil((obsXMax - obsXMin) / resolution) + 3);

	detectedObstaclePoints.clear();
	veloObstaclePointsLen = 0;
	veloObstaclePointsLenCopy = 0;

	totalObstaclePointsLen = 0;

	p00 = 0;
	p01 = 0;
	p10 = 0;

	CvSize imgSize;
	imgSize.height = drivableGridHeight;
	imgSize.width = drivableGridWidth;

	drivableAreaImg = cvCreateImage(imgSize, 8, 1);
	drivableAreaImgErode = cvCreateImage(imgSize, 8, 1);

	//--------data & parameter----//
	veloData[0].size = 0;
	veloData[1].size = 0;

	valid_index = 0;
	invalid_index = 1;
	newDataFlag = 0;

	boundXMin = -25.0;
	boundXMax = 25.0;
	boundYMin = -25.0;
	boundYMax = 25.0;

	xmlCorrection();

	if (offline == modeIn)
	{

	}
	else
	{
		veloRunThread = new boost::thread(veloRun, this);
		if (veloRunThread == NULL)
		{
			std::cout << "Open velo thread falied" << std::endl;
			boost::this_thread::sleep(boost::posix_time::millisec(500));
			exit(1);
		}
	}
}

velodyne::~velodyne()
{
	cvReleaseImage(&drivableAreaImg);
	cvReleaseImage(&drivableAreaImgErode);

	if (online == veloMode)
	{
		threadStopFlag = true;
		boost::this_thread::sleep(boost::posix_time::millisec(100));
		if (NULL != veloRunThread)		delete veloRunThread;
		if (NULL != veloDataSaveThread) delete veloDataSaveThread;
	}
}
/** \brief Set offline file name
* \param[in] prefix name
* \param[in] suffix name
*/
void
velodyne::setFileName(std::string prefixName, std::string suffixName)
{
	this->prefixName = prefixName;
	this->suffixName = suffixName;
}

/** \brief Set online save file name
* \param[in] prefix name
* \param[in] suffix name
*/
void
velodyne::setOnlineDataSaveFileName(std::string prefixName, std::string suffixName)
{
	this->onlineFilePrefixName = prefixName;
	this->onlineFileSuffixName = suffixName;
}

/** \brief Set bound parameters
* \param[in] x min
* \param[in] x max
* \param[in] y min
* \param[in] y max
*/
void
velodyne::setBoundPara(double setXMin, double setXMax, double setYMin, double setYMax)
{
	boundXMin = setXMin;
	boundXMax = setXMax;
	boundYMin = setYMin;
	boundYMax = setYMax;
}

/** \brief Read offline interial data
* \param[in] file index
*/
void
velodyne::readFromFile(int index)
{
	if (online == veloMode)
	{
		return;
	}
	veloNewestData.size = 0;

	char indexChar[10] = "";
	sprintf(indexChar, "%06d", index);
	std::string indexStr = indexChar;
	std::string fileName = prefixName + indexStr + suffixName;

	FILE* veloFp;
	veloFp=fopen(fileName.c_str(), "rb");
	if (NULL == veloFp)
	{
		std::cout << "Can not open :" << fileName.c_str() << std::endl;
		return;
	}

	float *tempArray = new float[5 * VeloDataSizeMax];
	int ArraySize = fread(tempArray, sizeof(float), 5 * VeloDataSizeMax, veloFp);

	fclose(veloFp);

	if (ArraySize == 0)
	{
		std::cout << "File read error :" << fileName.c_str() << std::endl;
	}

#pragma omp parallel for
	for (int i = 0; i < ArraySize; i = i + 5)
	{
		veloNewestData.x[i / 5] = tempArray[i];
		veloNewestData.y[i / 5] = tempArray[i + 1];
		veloNewestData.z[i / 5] = tempArray[i + 2];
		veloNewestData.r[i / 5] = tempArray[i + 3];
		veloNewestData.id[i / 5] = tempArray[i + 4];
	}
	veloNewestData.size = ArraySize / 5;

	delete[] tempArray;

}

/** \brief copy velo thread data
*/
void
velodyne::copyVeloData(int saveFlag)
{
	if (offline == veloMode)
	{
		return;
	}
	else if (online == veloMode)
	{
		bool whileFlag = true;
		while (whileFlag)
		{
			veloMutex.lock();
			if (0 == newDataFlag)
			{
				veloMutex.unlock();
				boost::this_thread::sleep(boost::posix_time::millisec(1));
			}
			else
			{
				newDataFlag = 0;

				memcpy(&veloNewestData, &veloData[valid_index], sizeof(veloNewestData));
				if (saveFlag)	callFileSave();
				
				whileFlag = false;
				veloMutex.unlock();
			}
			
			
		}
	}
}

/** \brief fix coordinate calibration
* \param[in] yaw angle
* \param[in] pitch angle
* \param[in] roll angle
*/
void
velodyne::fixCalibration(double yawFix, double pitchFix, double rollFix)
{
	double R[9] = { 0 };
	R[0] = cos(yawFix)*cos(rollFix) - sin(yawFix)*sin(pitchFix)*sin(rollFix);
	R[1] = sin(yawFix)*cos(pitchFix);
	R[2] = -cos(yawFix)*sin(rollFix) - sin(yawFix)*sin(pitchFix)*cos(rollFix);

	R[3] = -sin(yawFix)*cos(rollFix) - cos(yawFix)*sin(pitchFix)*sin(rollFix);
	R[4] = cos(yawFix)*cos(pitchFix);
	R[5] = sin(yawFix)*sin(rollFix) - cos(yawFix)*sin(pitchFix)*cos(rollFix);

	R[6] = cos(pitchFix)*sin(rollFix);
	R[7] = sin(pitchFix);
	R[8] = cos(pitchFix)*cos(rollFix);

#pragma omp parallel for
	for (int i = 0; i < veloNewestData.size; ++i)
	{
		double nx, ny, nz;
		nx = veloNewestData.x[i] * R[0] + veloNewestData.y[i] * R[3] + veloNewestData.z[i] * R[6];
		ny = veloNewestData.x[i] * R[1] + veloNewestData.y[i] * R[4] + veloNewestData.z[i] * R[7];
		nz = veloNewestData.x[i] * R[2] + veloNewestData.y[i] * R[5] + veloNewestData.z[i] * R[8];

		veloNewestData.x[i] = nx;
		veloNewestData.y[i] = ny;
		veloNewestData.z[i] = nz;
	}
}

/** \brief vehicle posture coordinate calibration
* \param[in] pitch angle
* \param[in] roll angle
*/
void
velodyne::posCalibration(double pitch, double roll)
{
	double R[9] = { 0 };
	pitch = -pitch;
	R[0] = cos(roll);
	R[1] = -sin(pitch)*sin(roll);
	R[2] = -cos(pitch)*sin(roll);

	R[3] = 0;
	R[4] = cos(pitch);
	R[5] = -sin(pitch);

	R[6] = sin(roll);
	R[7] = sin(pitch)*cos(roll);
	R[8] = cos(pitch)*cos(roll);

#pragma omp parallel for
	for (int i = 0; i < veloNewestData.size; ++i)
	{
		double nx, ny, nz;
		nx = veloNewestData.x[i] * R[0] + veloNewestData.y[i] * R[3] + veloNewestData.z[i] * R[6];
		ny = veloNewestData.x[i] * R[1] + veloNewestData.y[i] * R[4] + veloNewestData.z[i] * R[7];
		nz = veloNewestData.x[i] * R[2] + veloNewestData.y[i] * R[5] + veloNewestData.z[i] * R[8];

		veloNewestData.x[i] = nx;
		veloNewestData.y[i] = ny;
		veloNewestData.z[i] = nz;
	}
}

/** \brief separate and bounding the pointcloud
* \param[in] bounding zLim1
* \param[in] bounding zLim2
*/
void
velodyne::dividePoints(double zLim1, double zLim2)
{
	veloGroundData.size = 0;
	for (int i = 0; i < LineNum; i++)
	{
		lineVeloPoints[i].clear();
	}

	for (int i = 0; i < veloNewestData.size; i++)
	{
		if (veloNewestData.x[i]<boundXMin || veloNewestData.x[i]>boundXMax || veloNewestData.y[i]<boundYMin || veloNewestData.y[i]>boundYMax || veloNewestData.z[i] > zLim1)
		{
			continue;
		}

		VeloPointT tempPoint;
		tempPoint.x = veloNewestData.x[i];
		tempPoint.y = veloNewestData.y[i];
		tempPoint.z = veloNewestData.z[i];
		tempPoint.r = veloNewestData.r[i];
		tempPoint.h = 0;	//atan2f(tempPoint.y, tempPoint.x)/pii*180;
		tempPoint.age = 0;

		lineVeloPoints[(int)(veloNewestData.id[i])].insert(lineVeloPoints[(int)(veloNewestData.id[i])].end(), tempPoint);

		if (veloNewestData.z[i] > zLim2)
		{
			continue;
		}
		if (veloNewestData.x[i] > -6.0 && veloNewestData.x[i] < 6.0 && veloNewestData.y[i] > -6.0 && veloNewestData.y[i] < 6.0)
		{
			continue;
		}
		veloGroundData.x[veloGroundData.size] = veloNewestData.x[i];
		veloGroundData.y[veloGroundData.size] = veloNewestData.y[i];
		veloGroundData.z[veloGroundData.size] = veloNewestData.z[i];
		veloGroundData.r[veloGroundData.size] = veloNewestData.r[i];
		veloGroundData.id[veloGroundData.size] = veloNewestData.id[i];
		++veloGroundData.size;
	}
}

/** \brief separate the obstacle pointcloud
* \param[in] bounding zLim
*/
void 
velodyne::divideObstaclePoints(double zLim)
{
	veloObstaclePointsLen = 0;

	for (int i = 0; i < veloNewestData.size; i++)
	{
		if (veloNewestData.z[i] > zLim || veloNewestData.x[i] > obsXMax || veloNewestData.x[i]<obsXMin || veloNewestData.y[i] > obsYMax || veloNewestData.y[i] < obsYMin)
		{
			continue;
		}
		if (veloNewestData.x[i] > -1.5 && veloNewestData.x[i]<1.5 && veloNewestData.y[i]>-3.0 && veloNewestData.y[i] < 3.0)
		{
			continue;
		}

		veloObstaclePoints[0][veloObstaclePointsLen] = veloNewestData.x[i];
		veloObstaclePoints[1][veloObstaclePointsLen] = veloNewestData.y[i];
		veloObstaclePoints[2][veloObstaclePointsLen] = veloNewestData.z[i];
		veloObstaclePoints[3][veloObstaclePointsLen] = veloNewestData.r[i];
		veloObstaclePoints[4][veloObstaclePointsLen] = veloNewestData.id[i];
		veloObstaclePointsLen++;
	}


	totalObstaclePointsLen = 0;

	for (int i = 0; i < veloNewestData.size; i++)
	{
		if (veloNewestData.z[i] > -0.5 || veloNewestData.x[i] > obsXMax || veloNewestData.x[i]<obsXMin || veloNewestData.y[i] > obsYMax || veloNewestData.y[i] < obsYMin)
		{
			continue;
		}
		if (veloNewestData.x[i] > -1.5 && veloNewestData.x[i]<1.5 && veloNewestData.y[i]>-3.0 && veloNewestData.y[i] < 3.0)
		{
			continue;
		}

		totalObstaclePoints[0][totalObstaclePointsLen] = veloNewestData.x[i];
		totalObstaclePoints[1][totalObstaclePointsLen] = veloNewestData.y[i];
		totalObstaclePoints[2][totalObstaclePointsLen] = veloNewestData.z[i];
		totalObstaclePoints[3][totalObstaclePointsLen] = veloNewestData.r[i];
		totalObstaclePoints[4][totalObstaclePointsLen] = veloNewestData.id[i];
		totalObstaclePointsLen++;
	}
}

/** \brief curb detection
*/
/* \brief curb detection aux func
*/
double
findZMax(int s, int e, std::vector<VeloPointT> &points)
{
	double retZMax = points[s].z;
	for (int i = s + 1; i <= e; i++)
	{
		if (retZMax < points[i].z)
		{
			retZMax = points[i].z;
		}
	}
	return retZMax;
}
double
findZMin(int s, int e, std::vector<VeloPointT> &points)
{
	double retZMin = points[s].z;
	for (int i = s + 1; i <= e; i++)
	{
		if (retZMin > points[i].z)
		{
			retZMin = points[i].z;
		}
	}
	return retZMin;
}
int
findGradientMaxIndex(int s, int e, std::vector<VeloPointT> &points)
{
	int retIndex = s;
	double maxGradient = 0;
	for (int i = s + 1; i <= e; i++)
	{
		double gradient = fabs(points[i].z - points[i - 1].z);
		if (maxGradient < gradient)
		{
			maxGradient = gradient;
			retIndex = i;
		}
	}
	return retIndex;
}
double
computVar(int s, int e, std::vector<VeloPointT> &points)
{
	double mean = 0;
	double var = 0;
	for (int i = s; i <= e; i++)
	{
		mean += points[i].z;
	}
	mean = mean / (e - s + 1);

	for (int i = s; i <= e; i++)
	{
		var = var + (points[i].z - mean)*(points[i].z - mean);
	}
	var = var / (e - s + 1);
	return var;
}
/** \brief curb detection
*/
void
velodyne::performCurbDetection(void)
{
	curbPoints.clear();

	const int step = 8;
	const int winWidth = 30;

	double curbSroundHeightVarMinOld = 0;
	int newCurbFlag = 0;
	int curbRecordIndOld = 0;
	int curbRecordInd = 0;


	int newCurbWin = 0, sroundWin = 0;

	

	for (int i = 0; i < LineNum; i++)
	{
		if (0 == (i % 2))
		{
			newCurbWin = 160; sroundWin = 100;
		}
		else
		{
			newCurbWin = 80; sroundWin = 60;
		}

		newCurbFlag = 0;

		if (int(sroundWin + winWidth / 3) >= int(lineVeloPoints[i].size() - sroundWin - winWidth / 3))
		{
			continue;
		}

		for (int j = (sroundWin + winWidth / 3); j < (lineVeloPoints[i].size() - sroundWin - winWidth / 3); j = j + step)
		{
			double zMax = findZMax(j, j + winWidth, lineVeloPoints[i]);
			double zMin = findZMin(j, j + winWidth, lineVeloPoints[i]);
			if ((zMax - zMin) > 0.1 && (zMax - zMin) < 0.3)
			{
				curbRecordInd = findGradientMaxIndex(j, j + winWidth, lineVeloPoints[i]);

				double dist1, dist2;
				dist1 = sqrt((lineVeloPoints[i][curbRecordInd].x - lineVeloPoints[i][curbRecordInd - 1].x)*(lineVeloPoints[i][curbRecordInd].x - lineVeloPoints[i][curbRecordInd - 1].x) +
					(lineVeloPoints[i][curbRecordInd].y - lineVeloPoints[i][curbRecordInd - 1].y)*(lineVeloPoints[i][curbRecordInd].y - lineVeloPoints[i][curbRecordInd - 1].y));
				dist2 = sqrt((lineVeloPoints[i][curbRecordInd].x - lineVeloPoints[i][curbRecordInd + 1].x)*(lineVeloPoints[i][curbRecordInd].x - lineVeloPoints[i][curbRecordInd + 1].x) +
					(lineVeloPoints[i][curbRecordInd].y - lineVeloPoints[i][curbRecordInd + 1].y)*(lineVeloPoints[i][curbRecordInd].y - lineVeloPoints[i][curbRecordInd + 1].y));
				if (dist1 > 2.0 || dist2 > 2.0)
				{
					continue;
				}

				if ((curbRecordInd < (sroundWin + winWidth / 3)) || (curbRecordInd >= (lineVeloPoints[i].size() - sroundWin - winWidth / 3)))
				{
					continue;
				}

				double curbSroundHeightPostVar, curbSroundHeightPreVar, curbSroundHeightVarMin;
				curbSroundHeightPostVar = computVar(curbRecordInd + winWidth / 3, curbRecordInd + winWidth / 3 + sroundWin, lineVeloPoints[i]);
				curbSroundHeightPreVar = computVar(curbRecordInd - winWidth / 3 - sroundWin, curbRecordInd - winWidth / 3, lineVeloPoints[i]);

				if (curbSroundHeightPostVar > curbSroundHeightPreVar)	curbSroundHeightVarMin = curbSroundHeightPreVar;
				else curbSroundHeightVarMin = curbSroundHeightPostVar;

				if (newCurbFlag == 0 && curbSroundHeightVarMin < (0.04*0.04))
				{
					newCurbFlag = 1;
					curbSroundHeightVarMinOld = curbSroundHeightVarMin;
					curbRecordIndOld = curbRecordInd;
				}
				else if (newCurbFlag == 1)
				{
					if (curbSroundHeightVarMin < curbSroundHeightVarMinOld)
					{
						curbSroundHeightVarMinOld = curbSroundHeightVarMin;
						curbRecordIndOld = curbRecordInd;
					}
				}
			}
			if (newCurbFlag == 1 && (j - curbRecordIndOld) > newCurbWin)
			{
				newCurbFlag = 0;
				curbPoints.insert(curbPoints.end(), lineVeloPoints[i][curbRecordIndOld]);
			}

		}

		if (newCurbFlag == 1)
		{
			curbPoints.insert(curbPoints.end(), lineVeloPoints[i][curbRecordIndOld]);
		}

	}

}
/** \brief generate multi frame curbs
* \param[in] bounding dx
* \param[in] bounding dy
* \param[in] bounding dh
*/
void
velodyne::generateMultiFrameCurbs(double dx, double dy, double dh)
{
	// erase older frames
	for (std::vector<VeloPointT>::iterator it = curbPointsMulti.begin(); it != curbPointsMulti.end();)
	{
		if (it->age >= saveMultiFrameCount || it->x<boundXMin || it->x>boundXMax || it->y<boundYMin || it->y>boundYMax)
		{
			it = curbPointsMulti.erase(it);
		}
		else
		{
			++it;
		}
	}

	for (int i = 0; i < curbPointsMulti.size(); i++)
	{
		double tx, ty;
		tx = curbPointsMulti[i].x - dx;
		ty = curbPointsMulti[i].y - dy;
		curbPointsMulti[i].x = tx*cos(dh) + ty*sin(dh);
		curbPointsMulti[i].y = -tx*sin(dh) + ty*cos(dh);
		curbPointsMulti[i].age++;
	}

	for (int i = 0; i < curbPoints.size(); i++)
	{
		curbPointsMulti.insert(curbPointsMulti.end(), curbPoints[i]);
	}
}

/** \brief generate multi frame curbs
* \param[in] lat original
* \param[in] lon original
* \param[in] heading
*/
void
velodyne::generateGpsPoints(double latCenter, double lonCenter, double heading)
{
	GroundGpsPoints.resize(veloGroundData.size);

	veloProjection.backwardProjectStep1(latCenter, lonCenter);

#pragma omp parallel for
	for (int i = 0; i < veloGroundData.size; i++)
	{
		veloProjection.backwardProjectStep2(veloGroundData.x[i], veloGroundData.y[i], heading, GroundGpsPoints[i].lat, GroundGpsPoints[i].lon);
		GroundGpsPoints[i].r = veloGroundData.r[i];
	}

}

/** \brief contour beam model extract
* \param[in] vehicle local trajectory points
*/
void
velodyne::beamModelExtraction(std::vector<TrajectoryType> trajectoryPoints)
{
	curbPointsBeamModel.clear();

	const int beamAngle = 10;

	std::vector<int> flag;
	flag.resize(curbPointsMulti.size(), 0);

	for (std::vector<TrajectoryType>::iterator it = trajectoryPoints.begin(); it != trajectoryPoints.end();)
	{
		if (it->x >= boundXMax || it->x <= boundXMin || it->y >= boundYMax || it->y <= boundYMin)
		{
			it = trajectoryPoints.erase(it);
		}
		else
		{
			++it;
		}
	}

	for (int i = 0; i < trajectoryPoints.size(); i++)
	{
		std::vector<double> angleDistBin;
		std::vector<int>    binIndexVector;
		angleDistBin.resize(360 / beamAngle, 10000);
		binIndexVector.resize(360 / beamAngle, -1);

		double deltaX;
		double deltaY;
		double angle;
		double dist;
		int binIndex;
		for (int j = 0; j < curbPointsMulti.size(); j++)
		{
			deltaX = curbPointsMulti[j].x - trajectoryPoints[i].x;
			deltaY = curbPointsMulti[j].y - trajectoryPoints[i].y;
			angle = (atan2(deltaY, deltaX) + pii) / pii * 180;
			dist = sqrt(deltaX*deltaX + deltaY*deltaY);

			if (angle >= 360)	angle = 0;

			binIndex = floor(angle / beamAngle);
			if (angleDistBin[binIndex] > dist)
			{
				angleDistBin[binIndex] = dist;
				binIndexVector[binIndex] = j;
			}
		}

		for (int j = 0; j < binIndexVector.size(); j++)
		{
			if (binIndexVector[j] == -1)	continue;
			flag[binIndexVector[j]] = 1;
		}

	}

	for (int i = 0; i < flag.size(); i++)
	{
		if (flag[i] == 1)
		{
			curbPointsBeamModel.insert(curbPointsBeamModel.end(), curbPointsMulti[i]);
		}
	}

}

/** \brief drivable area calculate
* \param[in] local curb map
*/
void
velodyne::createDrivableArea(std::vector<VeloPointT> localCurbMapDrivable)
{
	cvZero(drivableAreaImg);

	for (int i = 0; i < localCurbMapDrivable.size(); i++)
	{
		int indH = (int)(round((localCurbMapDrivable[i].y - obsYMin) / resolution) + 1);
		int indW = (int)(round((localCurbMapDrivable[i].x - obsXMin) / resolution) + 1);

		((uchar*)(drivableAreaImg->imageData + drivableAreaImg->widthStep*(indH - 1)))[indW - 1] = 255;
		((uchar*)(drivableAreaImg->imageData + drivableAreaImg->widthStep*indH))[indW - 1] = 255;
		((uchar*)(drivableAreaImg->imageData + drivableAreaImg->widthStep*(indH + 1)))[indW - 1] = 255;

		((uchar*)(drivableAreaImg->imageData + drivableAreaImg->widthStep*(indH - 1)))[indW] = 255;
		((uchar*)(drivableAreaImg->imageData + drivableAreaImg->widthStep*indH))[indW] = 255;
		((uchar*)(drivableAreaImg->imageData + drivableAreaImg->widthStep*(indH + 1)))[indW] = 255;

		((uchar*)(drivableAreaImg->imageData + drivableAreaImg->widthStep*(indH - 1)))[indW + 1] = 255;
		((uchar*)(drivableAreaImg->imageData + drivableAreaImg->widthStep*indH))[indW + 1] = 255;
		((uchar*)(drivableAreaImg->imageData + drivableAreaImg->widthStep*(indH + 1)))[indW + 1] = 255;
	}
	


	IplConvKernel * se;
	se = cvCreateStructuringElementEx(8, 8, 4, 4, CV_SHAPE_RECT);
	cvDilate(drivableAreaImg, drivableAreaImgErode, se, 1);
	cvReleaseStructuringElement(&se);


	cvXorS(drivableAreaImgErode, cvScalarAll(255), drivableAreaImgErode);

	
	drivableAreaImgErodeMat = new cv::Mat(cv::cvarrToMat(drivableAreaImgErode));
	cv::Mat labelsMat;
	cv::connectedComponents(*drivableAreaImgErodeMat, labelsMat);
	delete drivableAreaImgErodeMat;

	int originalindH = int(round((0 - obsYMin) / resolution) + 1);
	int originalindW = int(round((0 - obsXMin) / resolution) + 1);

	int areaIndex = labelsMat.at<int>(originalindH, originalindW);
	for (int i = 0; i < drivableGridHeight; i++)
	{
		for (int j = 0; j < drivableGridWidth; j++)
		{
			if (labelsMat.at<int>(i, j) == areaIndex)
			{
				((uchar*)(drivableAreaImgErode->imageData + drivableAreaImgErode->widthStep*i))[j] = 255;
			}
			else
			{
				((uchar*)(drivableAreaImgErode->imageData + drivableAreaImgErode->widthStep*i))[j] = 0;
			}
		}
	}

	se = cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_RECT);
	cvDilate(drivableAreaImgErode, drivableAreaImgErode, se, 1);
	cvReleaseStructuringElement(&se);

	//cvShowImage("ppt", drivableAreaImgErode);
	//cvWaitKey(0);
}

/** \brief compute drivable velo points
*/
void
velodyne::computeDrivablePoints(void)
{
	int veloDrivablePointsLen = 0;

	for (int i = 0; i < veloObstaclePointsLen; i++)
	{
		int indH = (int)(round((veloObstaclePoints[1][i] - obsYMin) / resolution) + 1);
		int indW = (int)(round((veloObstaclePoints[0][i] - obsXMin) / resolution) + 1);

		if (((uchar*)(drivableAreaImgErode->imageData + drivableAreaImgErode->widthStep*indH))[indW] == 255)
		{
			veloObstaclePoints[0][veloDrivablePointsLen] = veloObstaclePoints[0][i];
			veloObstaclePoints[1][veloDrivablePointsLen] = veloObstaclePoints[1][i];
			veloObstaclePoints[2][veloDrivablePointsLen] = veloObstaclePoints[2][i];
			veloObstaclePoints[3][veloDrivablePointsLen] = veloObstaclePoints[3][i];
			veloObstaclePoints[4][veloDrivablePointsLen] = veloObstaclePoints[4][i];
			veloDrivablePointsLen++;
		}
	}

	veloObstaclePointsLen = veloDrivablePointsLen;


	veloDrivablePointsLen = 0;

	for (int i = 0; i < totalObstaclePointsLen; i++)
	{
		int indH = (int)(round((totalObstaclePoints[1][i] - obsYMin) / resolution) + 1);
		int indW = (int)(round((totalObstaclePoints[0][i] - obsXMin) / resolution) + 1);

		if (((uchar*)(drivableAreaImgErode->imageData + drivableAreaImgErode->widthStep*indH))[indW] == 255)
		{
			totalObstaclePoints[0][veloDrivablePointsLen] = totalObstaclePoints[0][i];
			totalObstaclePoints[1][veloDrivablePointsLen] = totalObstaclePoints[1][i];
			totalObstaclePoints[2][veloDrivablePointsLen] = totalObstaclePoints[2][i];
			totalObstaclePoints[3][veloDrivablePointsLen] = totalObstaclePoints[3][i];
			totalObstaclePoints[4][veloDrivablePointsLen] = totalObstaclePoints[4][i];
			veloDrivablePointsLen++;
		}
	}

	totalObstaclePointsLen = veloDrivablePointsLen;
}

/** \brief detect the obstacle within drivable area
*/
void
velodyne::obstacleDetect(void)
{
	detectedObstaclePoints.clear();

	memcpy(veloObstaclePointsCopy, veloObstaclePoints, 5 * VeloDataSizeMax * sizeof(VeloDataSizeMax));
	veloObstaclePointsLenCopy = veloObstaclePointsLen;

	veloPlaneFitting(veloObstaclePointsCopy[0], veloObstaclePointsLenCopy, 0.95, &p00, &p10, &p01);

	int newVeloObstaclePointsLenCopy = 0;
	for (int i = 0; i < veloObstaclePointsLenCopy; i++)
	{
		double zFit = p00 + p10*veloObstaclePointsCopy[0][i] + p01*veloObstaclePointsCopy[1][i];
		double deltaZ = (veloObstaclePointsCopy[2][i] - zFit);

		if (deltaZ < 0.6)
		{
			veloObstaclePointsCopy[0][newVeloObstaclePointsLenCopy] = veloObstaclePoints[0][i];
			veloObstaclePointsCopy[1][newVeloObstaclePointsLenCopy] = veloObstaclePoints[1][i];
			veloObstaclePointsCopy[2][newVeloObstaclePointsLenCopy] = veloObstaclePoints[2][i];
			veloObstaclePointsCopy[3][newVeloObstaclePointsLenCopy] = veloObstaclePoints[3][i];
			veloObstaclePointsCopy[4][newVeloObstaclePointsLenCopy] = veloObstaclePoints[4][i];
			newVeloObstaclePointsLenCopy++;
		}
	}
	veloObstaclePointsLenCopy = newVeloObstaclePointsLenCopy;

	veloPlaneFitting(veloObstaclePointsCopy[0], veloObstaclePointsLenCopy, 0.98, &p00, &p10, &p01);

	for (int i = 0; i < totalObstaclePointsLen; i++)
	{
		double zFit = p00 + p10*totalObstaclePoints[0][i] + p01*totalObstaclePoints[1][i];
		double deltaZ = (totalObstaclePoints[2][i] - zFit);

		double dist = sqrt(totalObstaclePoints[0][i] * totalObstaclePoints[0][i] + totalObstaclePoints[1][i] * totalObstaclePoints[1][i]);
		double threshold = 0.3 * dist / 30.0;
		if (threshold > 0.4)		threshold = 0.4;

		if (deltaZ > (0.2 + threshold))
		{
			VeloPointT tempPoint;

			tempPoint.x = totalObstaclePoints[0][i];
			tempPoint.y = totalObstaclePoints[1][i];
			tempPoint.z = totalObstaclePoints[2][i];
			tempPoint.r = totalObstaclePoints[3][i];
			tempPoint.age = 0;

			detectedObstaclePoints.insert(detectedObstaclePoints.end(), tempPoint);
		}
	}
}

/** \brief save one frame inertial data
*/
void
velodyne::callFileSave(void)
{
	if (NULL != veloDataSaveThread)
	{
		delete veloDataSaveThread;
		veloDataSaveThread = NULL;
	}
	veloDataSaveThread = new boost::thread(veloFileSaveThread, this);
}

/** \brief udp package praser
* \param[in] udp package buffer
*/
void
velodyne::packagePrase(char buf[1206])
{
	static float   rotAngleOld = 359.0;
	static float   rotAngle = 0;

	int i = 0, j = 0;
	int index1 = 0, index2 = 0;

	float verticleAngle = 0;
	float distance = 0;
	float reflect = 0;

	static unsigned char highBit = 0, lowBit = 0;

	for (i = 0; i < 12; ++i)
	{
		index1 = 100 * i;
		highBit = buf[index1 + 2];//Rotational Angle
		lowBit = buf[index1 + 3];
		rotAngle = (unsigned short int)((lowBit << 8) + highBit) / 100.0f;
		if (rotAngle < 0) rotAngle = rotAngle + 360;
		else if (rotAngle >= 360) rotAngle = rotAngle - 360;
		//----------check the start of frame ----------//
		if ((rotAngleOld - rotAngle) > 0.1)
		{
			veloMutex.lock();
			if (valid_index == 0)
			{
				valid_index = 1;
				invalid_index = 0;
				newDataFlag = 1;
			}
			else
			{
				valid_index = 0;
				invalid_index = 1;
				newDataFlag = 1;
			}
			veloData[invalid_index].size = 0;
			veloMutex.unlock();
		}
		rotAngleOld = rotAngle;
		//////////////////////////
		for (j = 0; j < 32; j++)
		{
			index2 = 3 * j + index1 + 4;
			highBit = buf[index2];
			lowBit = buf[index2 + 1];//distance 
			distance = (unsigned short int)((lowBit << 8) + highBit)*0.2f / 100.0f;
			reflect = (unsigned char)buf[index2 + 2];
			verticleAngle = xmlData[j];

			if (distance > 1.0 && veloData[invalid_index].size < VeloDataSizeMax)
			{
				float x, y, z;
				x = float(distance*cos(verticleAngle*Ang2Rad)*sin(rotAngle*Ang2Rad));
				y = float(distance*cos(verticleAngle*Ang2Rad)*cos(rotAngle*Ang2Rad));
				z = float(distance*sin(verticleAngle*Ang2Rad));

				veloData[invalid_index].x[veloData[invalid_index].size] = x;
				veloData[invalid_index].y[veloData[invalid_index].size] = y;
				veloData[invalid_index].z[veloData[invalid_index].size] = z;
				veloData[invalid_index].r[veloData[invalid_index].size] = reflect;
				veloData[invalid_index].id[veloData[invalid_index].size] = float(j);

				++veloData[invalid_index].size;
			}
		}

	}
}

/** \brief set velo angle
*/
void
velodyne::xmlCorrection(void)
{
	xmlData[0] = -30.67f;
	xmlData[1] = -9.33f;
	xmlData[2] = -29.33f;
	xmlData[3] = -8.0f;
	xmlData[4] = -28.0f;
	xmlData[5] = -6.67f;
	xmlData[6] = -26.67f;
	xmlData[7] = -5.33f;
	xmlData[8] = -25.33f;
	xmlData[9] = -4.0f;
	xmlData[10] = -24.0f;
	xmlData[11] = -2.67f;
	xmlData[12] = -22.67f;
	xmlData[13] = -1.33f;
	xmlData[14] = -21.33f;
	xmlData[15] = 0.0f;
	xmlData[16] = -20.0f;
	xmlData[17] = 1.33f;
	xmlData[18] = -18.67f;
	xmlData[19] = 2.67f;
	xmlData[20] = -17.33f;
	xmlData[21] = 4.0f;
	xmlData[22] = -16.0f;
	xmlData[23] = 5.33f;
	xmlData[24] = -14.67f;
	xmlData[25] = 6.67f;
	xmlData[26] = -13.33f;
	xmlData[27] = 8.0f;
	xmlData[28] = -12.0f;
	xmlData[29] = 9.33f;
	xmlData[30] = -10.67f;
	xmlData[31] = 10.67f;
}
