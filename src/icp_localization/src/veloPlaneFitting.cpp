//
// File: veloPlaneFitting.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 07-Sep-2016 10:51:21
//

// Include Files
#include "veloPlaneFitting.h"
#include "rtwtypes.h"
// Type Definitions
#ifndef struct_emxArray__common
#define struct_emxArray__common

struct emxArray__common
{
  void *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  boolean_T canFreeData;
};

#endif                                 //struct_emxArray__common

#ifndef struct_emxArray_int32_T
#define struct_emxArray_int32_T

struct emxArray_int32_T
{
  int *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  boolean_T canFreeData;
};

#endif                                 //struct_emxArray_int32_T

#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T

struct emxArray_real_T
{
  double *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  boolean_T canFreeData;
};

#endif                                 //struct_emxArray_real_T

// Function Declarations
static double b_xnrm2(int n, const emxArray_real_T *x, int ix0);
static double c_xnrm2(int n, const emxArray_real_T *x, int ix0);
static void emxEnsureCapacity(emxArray__common *emxArray, int oldNumel, int
  elementSize);
static void emxFree_int32_T(emxArray_int32_T **pEmxArray);
static void emxFree_real_T(emxArray_real_T **pEmxArray);
static void emxInit_int32_T(emxArray_int32_T **pEmxArray, int numDimensions);
static void emxInit_real_T(emxArray_real_T **pEmxArray, int numDimensions);
static void emxInit_real_T1(emxArray_real_T **pEmxArray, int numDimensions);
static int ilazlc(int m, int n, const emxArray_real_T *A, int ia0, int lda);
static int ixamax(int n, const double x[3], int ix0);
static void merge(emxArray_int32_T *idx, emxArray_real_T *x, int offset, int np,
                  int nq, emxArray_int32_T *iwork, emxArray_real_T *xwork);
static void merge_block(emxArray_int32_T *idx, emxArray_real_T *x, int offset,
  int n, int preSortLevel, emxArray_int32_T *iwork, emxArray_real_T *xwork);
static void merge_pow2_block(emxArray_int32_T *idx, emxArray_real_T *x, int
  offset);
static void mldivide(const emxArray_real_T *A, const emxArray_real_T *B, double
                     Y[3]);
static int nonSingletonDim(const emxArray_real_T *x);
static void qrsolve(const emxArray_real_T *A, const emxArray_real_T *B, double
                    Y[3]);
static int rankFromQR(const emxArray_real_T *A);
static double rt_roundd(double u);
static void sort(emxArray_real_T *x, emxArray_int32_T *idx);
static void sortIdx(emxArray_real_T *x, emxArray_int32_T *idx);
static void xgemv(int m, int n, const emxArray_real_T *A, int ia0, int lda,
                  const emxArray_real_T *x, int ix0, double y[3]);
static void xgeqp3(emxArray_real_T *A, double tau_data[], int tau_size[1], int
                   jpvt[3]);
static void xgerc(int m, int n, double alpha1, int ix0, const double y[3],
                  emxArray_real_T *A, int ia0, int lda);
static double xnrm2(int n, const emxArray_real_T *x, int ix0);
static void xswap(int n, emxArray_real_T *x, int ix0, int iy0);
static void xzlarf(int m, int n, int iv0, double tau, emxArray_real_T *C, int
                   ic0, int ldc, double work[3]);
static double xzlarfg(int n, double *alpha1, emxArray_real_T *x, int ix0);

// Function Definitions

//
// Arguments    : int n
//                const emxArray_real_T *x
//                int ix0
// Return Type  : double
//
static double b_xnrm2(int n, const emxArray_real_T *x, int ix0)
{
  double y;
  double scale;
  int kend;
  int k;
  double absxk;
  double t;
  y = 0.0;
  if (n < 1) {
  } else if (n == 1) {
    y = fabs(x->data[ix0 - 1]);
  } else {
    scale = 2.2250738585072014E-308;
    kend = (ix0 + n) - 1;
    for (k = ix0; k <= kend; k++) {
      absxk = fabs(x->data[k - 1]);
      if (absxk > scale) {
        t = scale / absxk;
        y = 1.0 + y * t * t;
        scale = absxk;
      } else {
        t = absxk / scale;
        y += t * t;
      }
    }

    y = scale * sqrt(y);
  }

  return y;
}

//
// Arguments    : int n
//                const emxArray_real_T *x
//                int ix0
// Return Type  : double
//
static double c_xnrm2(int n, const emxArray_real_T *x, int ix0)
{
  double y;
  double scale;
  int kend;
  int k;
  double absxk;
  double t;
  y = 0.0;
  if (n < 1) {
  } else if (n == 1) {
    y = fabs(x->data[ix0 - 1]);
  } else {
    scale = 2.2250738585072014E-308;
    kend = (ix0 + n) - 1;
    for (k = ix0; k <= kend; k++) {
      absxk = fabs(x->data[k - 1]);
      if (absxk > scale) {
        t = scale / absxk;
        y = 1.0 + y * t * t;
        scale = absxk;
      } else {
        t = absxk / scale;
        y += t * t;
      }
    }

    y = scale * sqrt(y);
  }

  return y;
}

//
// Arguments    : emxArray__common *emxArray
//                int oldNumel
//                int elementSize
// Return Type  : void
//
static void emxEnsureCapacity(emxArray__common *emxArray, int oldNumel, int
  elementSize)
{
  int newNumel;
  int i;
  void *newData;
  newNumel = 1;
  for (i = 0; i < emxArray->numDimensions; i++) {
    newNumel *= emxArray->size[i];
  }

  if (newNumel > emxArray->allocatedSize) {
    i = emxArray->allocatedSize;
    if (i < 16) {
      i = 16;
    }

    while (i < newNumel) {
      if (i > 1073741823) {
        i = MAX_int32_T;
      } else {
        i <<= 1;
      }
    }

    newData = calloc((unsigned int)i, (unsigned int)elementSize);
    if (emxArray->data != NULL) {
      memcpy(newData, emxArray->data, (unsigned int)(elementSize * oldNumel));
      if (emxArray->canFreeData) {
        free(emxArray->data);
      }
    }

    emxArray->data = newData;
    emxArray->allocatedSize = i;
    emxArray->canFreeData = true;
  }
}

//
// Arguments    : emxArray_int32_T **pEmxArray
// Return Type  : void
//
static void emxFree_int32_T(emxArray_int32_T **pEmxArray)
{
  if (*pEmxArray != (emxArray_int32_T *)NULL) {
    if (((*pEmxArray)->data != (int *)NULL) && (*pEmxArray)->canFreeData) {
      free((void *)(*pEmxArray)->data);
    }

    free((void *)(*pEmxArray)->size);
    free((void *)*pEmxArray);
    *pEmxArray = (emxArray_int32_T *)NULL;
  }
}

//
// Arguments    : emxArray_real_T **pEmxArray
// Return Type  : void
//
static void emxFree_real_T(emxArray_real_T **pEmxArray)
{
  if (*pEmxArray != (emxArray_real_T *)NULL) {
    if (((*pEmxArray)->data != (double *)NULL) && (*pEmxArray)->canFreeData) {
      free((void *)(*pEmxArray)->data);
    }

    free((void *)(*pEmxArray)->size);
    free((void *)*pEmxArray);
    *pEmxArray = (emxArray_real_T *)NULL;
  }
}

//
// Arguments    : emxArray_int32_T **pEmxArray
//                int numDimensions
// Return Type  : void
//
static void emxInit_int32_T(emxArray_int32_T **pEmxArray, int numDimensions)
{
  emxArray_int32_T *emxArray;
  int i;
  *pEmxArray = (emxArray_int32_T *)malloc(sizeof(emxArray_int32_T));
  emxArray = *pEmxArray;
  emxArray->data = (int *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int *)malloc((unsigned int)(sizeof(int) * numDimensions));
  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

//
// Arguments    : emxArray_real_T **pEmxArray
//                int numDimensions
// Return Type  : void
//
static void emxInit_real_T(emxArray_real_T **pEmxArray, int numDimensions)
{
  emxArray_real_T *emxArray;
  int i;
  *pEmxArray = (emxArray_real_T *)malloc(sizeof(emxArray_real_T));
  emxArray = *pEmxArray;
  emxArray->data = (double *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int *)malloc((unsigned int)(sizeof(int) * numDimensions));
  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

//
// Arguments    : emxArray_real_T **pEmxArray
//                int numDimensions
// Return Type  : void
//
static void emxInit_real_T1(emxArray_real_T **pEmxArray, int numDimensions)
{
  emxArray_real_T *emxArray;
  int i;
  *pEmxArray = (emxArray_real_T *)malloc(sizeof(emxArray_real_T));
  emxArray = *pEmxArray;
  emxArray->data = (double *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int *)malloc((unsigned int)(sizeof(int) * numDimensions));
  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

//
// Arguments    : int m
//                int n
//                const emxArray_real_T *A
//                int ia0
//                int lda
// Return Type  : int
//
static int ilazlc(int m, int n, const emxArray_real_T *A, int ia0, int lda)
{
  int j;
  boolean_T exitg2;
  int coltop;
  int ia;
  int exitg1;
  j = n;
  exitg2 = false;
  while ((!exitg2) && (j > 0)) {
    coltop = ia0 + (j - 1) * lda;
    ia = coltop;
    do {
      exitg1 = 0;
      if (ia <= (coltop + m) - 1) {
        if (A->data[ia - 1] != 0.0) {
          exitg1 = 1;
        } else {
          ia++;
        }
      } else {
        j--;
        exitg1 = 2;
      }
    } while (exitg1 == 0);

    if (exitg1 == 1) {
      exitg2 = true;
    }
  }

  return j;
}

//
// Arguments    : int n
//                const double x[3]
//                int ix0
// Return Type  : int
//
static int ixamax(int n, const double x[3], int ix0)
{
  int idxmax;
  int ix;
  double smax;
  int k;
  double s;
  if (n < 1) {
    idxmax = 0;
  } else {
    idxmax = 1;
    if (n > 1) {
      ix = ix0 - 1;
      smax = fabs(x[ix0 - 1]);
      for (k = 2; k <= n; k++) {
        ix++;
        s = fabs(x[ix]);
        if (s > smax) {
          idxmax = k;
          smax = s;
        }
      }
    }
  }

  return idxmax;
}

//
// Arguments    : emxArray_int32_T *idx
//                emxArray_real_T *x
//                int offset
//                int np
//                int nq
//                emxArray_int32_T *iwork
//                emxArray_real_T *xwork
// Return Type  : void
//
static void merge(emxArray_int32_T *idx, emxArray_real_T *x, int offset, int np,
                  int nq, emxArray_int32_T *iwork, emxArray_real_T *xwork)
{
  int n;
  int qend;
  int p;
  int iout;
  int exitg1;
  if (nq == 0) {
  } else {
    n = np + nq;
    for (qend = 0; qend + 1 <= n; qend++) {
      iwork->data[qend] = idx->data[offset + qend];
      xwork->data[qend] = x->data[offset + qend];
    }

    p = 0;
    n = np;
    qend = np + nq;
    iout = offset - 1;
    do {
      exitg1 = 0;
      iout++;
      if (xwork->data[p] <= xwork->data[n]) {
        idx->data[iout] = iwork->data[p];
        x->data[iout] = xwork->data[p];
        if (p + 1 < np) {
          p++;
        } else {
          exitg1 = 1;
        }
      } else {
        idx->data[iout] = iwork->data[n];
        x->data[iout] = xwork->data[n];
        if (n + 1 < qend) {
          n++;
        } else {
          n = (iout - p) + 1;
          while (p + 1 <= np) {
            idx->data[n + p] = iwork->data[p];
            x->data[n + p] = xwork->data[p];
            p++;
          }

          exitg1 = 1;
        }
      }
    } while (exitg1 == 0);
  }
}

//
// Arguments    : emxArray_int32_T *idx
//                emxArray_real_T *x
//                int offset
//                int n
//                int preSortLevel
//                emxArray_int32_T *iwork
//                emxArray_real_T *xwork
// Return Type  : void
//
static void merge_block(emxArray_int32_T *idx, emxArray_real_T *x, int offset,
  int n, int preSortLevel, emxArray_int32_T *iwork, emxArray_real_T *xwork)
{
  int nPairs;
  int bLen;
  int tailOffset;
  int nTail;
  nPairs = n >> preSortLevel;
  bLen = 1 << preSortLevel;
  while (nPairs > 1) {
    if ((nPairs & 1) != 0) {
      nPairs--;
      tailOffset = bLen * nPairs;
      nTail = n - tailOffset;
      if (nTail > bLen) {
        merge(idx, x, offset + tailOffset, bLen, nTail - bLen, iwork, xwork);
      }
    }

    tailOffset = bLen << 1;
    nPairs >>= 1;
    for (nTail = 1; nTail <= nPairs; nTail++) {
      merge(idx, x, offset + (nTail - 1) * tailOffset, bLen, bLen, iwork, xwork);
    }

    bLen = tailOffset;
  }

  if (n > bLen) {
    merge(idx, x, offset, bLen, n - bLen, iwork, xwork);
  }
}

//
// Arguments    : emxArray_int32_T *idx
//                emxArray_real_T *x
//                int offset
// Return Type  : void
//
static void merge_pow2_block(emxArray_int32_T *idx, emxArray_real_T *x, int
  offset)
{
  int iwork[256];
  double xwork[256];
  int b;
  int bLen;
  int bLen2;
  int nPairs;
  int k;
  int blockOffset;
  int q;
  int p;
  int exitg1;
  for (b = 0; b < 6; b++) {
    bLen = 1 << (b + 2);
    bLen2 = bLen << 1;
    nPairs = 256 >> (b + 3);
    for (k = 1; k <= nPairs; k++) {
      blockOffset = (offset + (k - 1) * bLen2) - 1;
      for (q = 1; q <= bLen2; q++) {
        iwork[q - 1] = idx->data[blockOffset + q];
        xwork[q - 1] = x->data[blockOffset + q];
      }

      p = 0;
      q = bLen;
      do {
        exitg1 = 0;
        blockOffset++;
        if (xwork[p] <= xwork[q]) {
          idx->data[blockOffset] = iwork[p];
          x->data[blockOffset] = xwork[p];
          if (p + 1 < bLen) {
            p++;
          } else {
            exitg1 = 1;
          }
        } else {
          idx->data[blockOffset] = iwork[q];
          x->data[blockOffset] = xwork[q];
          if (q + 1 < bLen2) {
            q++;
          } else {
            q = blockOffset - p;
            while (p + 1 <= bLen) {
              idx->data[(q + p) + 1] = iwork[p];
              x->data[(q + p) + 1] = xwork[p];
              p++;
            }

            exitg1 = 1;
          }
        }
      } while (exitg1 == 0);
    }
  }
}

//
// Arguments    : const emxArray_real_T *A
//                const emxArray_real_T *B
//                double Y[3]
// Return Type  : void
//
static void mldivide(const emxArray_real_T *A, const emxArray_real_T *B, double
                     Y[3])
{
  int kAcol;
  int jy;
  int i1;
  signed char ipiv[3];
  double A_data[9];
  int j;
  int c;
  int ix;
  double temp;
  int k;
  double s;
  int ijA;
  if ((A->size[0] == 0) || (B->size[0] == 0)) {
    for (jy = 0; jy < 3; jy++) {
      Y[jy] = 0.0;
    }
  } else if (A->size[0] == 3) {
    kAcol = A->size[0] * A->size[1];
    for (i1 = 0; i1 < kAcol; i1++) {
      A_data[i1] = A->data[i1];
    }

    for (i1 = 0; i1 < 3; i1++) {
      ipiv[i1] = (signed char)(1 + i1);
    }

    for (j = 0; j < 2; j++) {
      c = j << 2;
      kAcol = 0;
      ix = c;
      temp = fabs(A_data[c]);
      for (k = 2; k <= 3 - j; k++) {
        ix++;
        s = fabs(A_data[ix]);
        if (s > temp) {
          kAcol = k - 1;
          temp = s;
        }
      }

      if (A_data[c + kAcol] != 0.0) {
        if (kAcol != 0) {
          ipiv[j] = (signed char)((j + kAcol) + 1);
          ix = j;
          kAcol += j;
          for (k = 0; k < 3; k++) {
            temp = A_data[ix];
            A_data[ix] = A_data[kAcol];
            A_data[kAcol] = temp;
            ix += 3;
            kAcol += 3;
          }
        }

        i1 = (c - j) + 3;
        for (jy = c + 1; jy + 1 <= i1; jy++) {
          A_data[jy] /= A_data[c];
        }
      }

      kAcol = c;
      jy = c + 3;
      for (k = 1; k <= 2 - j; k++) {
        temp = A_data[jy];
        if (A_data[jy] != 0.0) {
          ix = c + 1;
          i1 = (kAcol - j) + 6;
          for (ijA = 4 + kAcol; ijA + 1 <= i1; ijA++) {
            A_data[ijA] += A_data[ix] * -temp;
            ix++;
          }
        }

        jy += 3;
        kAcol += 3;
      }
    }

    for (i1 = 0; i1 < 3; i1++) {
      Y[i1] = B->data[i1];
    }

    for (kAcol = 0; kAcol < 2; kAcol++) {
      if (ipiv[kAcol] != kAcol + 1) {
        temp = Y[kAcol];
        Y[kAcol] = Y[ipiv[kAcol] - 1];
        Y[ipiv[kAcol] - 1] = temp;
      }
    }

    for (k = 0; k < 3; k++) {
      kAcol = 3 * k;
      if (Y[k] != 0.0) {
        for (jy = k + 1; jy + 1 < 4; jy++) {
          Y[jy] -= Y[k] * A_data[jy + kAcol];
        }
      }
    }

    for (k = 2; k >= 0; k += -1) {
      kAcol = 3 * k;
      if (Y[k] != 0.0) {
        Y[k] /= A_data[k + kAcol];
        for (jy = 0; jy + 1 <= k; jy++) {
          Y[jy] -= Y[k] * A_data[jy + kAcol];
        }
      }
    }
  } else {
    qrsolve(A, B, Y);
  }
}

//
// Arguments    : const emxArray_real_T *x
// Return Type  : int
//
static int nonSingletonDim(const emxArray_real_T *x)
{
  int dim;
  dim = 2;
  if (x->size[0] != 1) {
    dim = 1;
  }

  return dim;
}

//
// Arguments    : const emxArray_real_T *A
//                const emxArray_real_T *B
//                double Y[3]
// Return Type  : void
//
static void qrsolve(const emxArray_real_T *A, const emxArray_real_T *B, double
                    Y[3])
{
  emxArray_real_T *b_A;
  int i;
  int m;
  double tau_data[3];
  int tau_size[1];
  int jpvt[3];
  int rankA;
  emxArray_real_T *b_B;
  int mn;
  int j;
  double wj;
  emxInit_real_T1(&b_A, 2);
  i = b_A->size[0] * b_A->size[1];
  b_A->size[0] = A->size[0];
  b_A->size[1] = 3;
  emxEnsureCapacity((emxArray__common *)b_A, i, (int)sizeof(double));
  m = A->size[0] * A->size[1];
  for (i = 0; i < m; i++) {
    b_A->data[i] = A->data[i];
  }

  xgeqp3(b_A, tau_data, tau_size, jpvt);
  rankA = rankFromQR(b_A);
  for (i = 0; i < 3; i++) {
    Y[i] = 0.0;
  }

  emxInit_real_T(&b_B, 1);
  i = b_B->size[0];
  b_B->size[0] = B->size[0];
  emxEnsureCapacity((emxArray__common *)b_B, i, (int)sizeof(double));
  m = B->size[0];
  for (i = 0; i < m; i++) {
    b_B->data[i] = B->data[i];
  }

  m = b_A->size[0];
  mn = b_A->size[0];
  if (mn <= 3) {
  } else {
    mn = 3;
  }

  for (j = 0; j + 1 <= mn; j++) {
    if (tau_data[j] != 0.0) {
      wj = b_B->data[j];
      for (i = j + 1; i + 1 <= m; i++) {
        wj += b_A->data[i + b_A->size[0] * j] * b_B->data[i];
      }

      wj *= tau_data[j];
      if (wj != 0.0) {
        b_B->data[j] -= wj;
        for (i = j + 1; i + 1 <= m; i++) {
          b_B->data[i] -= b_A->data[i + b_A->size[0] * j] * wj;
        }
      }
    }
  }

  for (i = 0; i + 1 <= rankA; i++) {
    Y[jpvt[i] - 1] = b_B->data[i];
  }

  emxFree_real_T(&b_B);
  for (j = rankA - 1; j + 1 > 0; j--) {
    Y[jpvt[j] - 1] /= b_A->data[j + b_A->size[0] * j];
    for (i = 0; i + 1 <= j; i++) {
      Y[jpvt[i] - 1] -= Y[jpvt[j] - 1] * b_A->data[i + b_A->size[0] * j];
    }
  }

  emxFree_real_T(&b_A);
}

//
// Arguments    : const emxArray_real_T *A
// Return Type  : int
//
static int rankFromQR(const emxArray_real_T *A)
{
  int r;
  int minmn;
  int maxmn;
  double tol;
  r = 0;
  if (A->size[0] < 3) {
    minmn = A->size[0];
    maxmn = 3;
  } else {
    minmn = 3;
    maxmn = A->size[0];
  }

  if (minmn > 0) {
    tol = (double)maxmn * fabs(A->data[0]) * 2.2204460492503131E-16;
    while ((r < minmn) && (fabs(A->data[r + A->size[0] * r]) >= tol)) {
      r++;
    }
  }

  return r;
}

//
// Arguments    : double u
// Return Type  : double
//
static double rt_roundd(double u)
{
  double y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

//
// Arguments    : emxArray_real_T *x
//                emxArray_int32_T *idx
// Return Type  : void
//
static void sort(emxArray_real_T *x, emxArray_int32_T *idx)
{
  int dim;
  int i2;
  emxArray_real_T *vwork;
  int j;
  int vstride;
  int k;
  emxArray_int32_T *iidx;
  dim = nonSingletonDim(x);
  if (dim <= 1) {
    i2 = x->size[0];
  } else {
    i2 = 1;
  }

  emxInit_real_T(&vwork, 1);
  j = vwork->size[0];
  vwork->size[0] = i2;
  emxEnsureCapacity((emxArray__common *)vwork, j, (int)sizeof(double));
  vstride = x->size[0];
  j = idx->size[0];
  idx->size[0] = vstride;
  emxEnsureCapacity((emxArray__common *)idx, j, (int)sizeof(int));
  vstride = 1;
  k = 1;
  while (k <= dim - 1) {
    vstride *= x->size[0];
    k = 2;
  }

  j = 0;
  emxInit_int32_T(&iidx, 1);
  while (j + 1 <= vstride) {
    for (k = 0; k + 1 <= i2; k++) {
      vwork->data[k] = x->data[j + k * vstride];
    }

    sortIdx(vwork, iidx);
    for (k = 0; k + 1 <= i2; k++) {
      x->data[j + k * vstride] = vwork->data[k];
      idx->data[j + k * vstride] = iidx->data[k];
    }

    j++;
  }

  emxFree_int32_T(&iidx);
  emxFree_real_T(&vwork);
}

//
// Arguments    : emxArray_real_T *x
//                emxArray_int32_T *idx
// Return Type  : void
//
static void sortIdx(emxArray_real_T *x, emxArray_int32_T *idx)
{
  emxArray_real_T *b_x;
  int ib;
  int i3;
  int preSortLevel;
  int n;
  double x4[4];
  int idx4[4];
  emxArray_int32_T *iwork;
  emxArray_real_T *xwork;
  int k;
  signed char perm[4];
  int i4;
  emxInit_real_T(&b_x, 1);
  ib = x->size[0];
  i3 = b_x->size[0];
  b_x->size[0] = x->size[0];
  emxEnsureCapacity((emxArray__common *)b_x, i3, (int)sizeof(double));
  preSortLevel = x->size[0];
  for (i3 = 0; i3 < preSortLevel; i3++) {
    b_x->data[i3] = x->data[i3];
  }

  i3 = idx->size[0];
  idx->size[0] = ib;
  emxEnsureCapacity((emxArray__common *)idx, i3, (int)sizeof(int));
  for (i3 = 0; i3 < ib; i3++) {
    idx->data[i3] = 0;
  }

  n = x->size[0];
  for (preSortLevel = 0; preSortLevel < 4; preSortLevel++) {
    x4[preSortLevel] = 0.0;
    idx4[preSortLevel] = 0;
  }

  emxInit_int32_T(&iwork, 1);
  i3 = iwork->size[0];
  iwork->size[0] = ib;
  emxEnsureCapacity((emxArray__common *)iwork, i3, (int)sizeof(int));
  preSortLevel = iwork->size[0];
  i3 = iwork->size[0];
  iwork->size[0] = preSortLevel;
  emxEnsureCapacity((emxArray__common *)iwork, i3, (int)sizeof(int));
  for (i3 = 0; i3 < preSortLevel; i3++) {
    iwork->data[i3] = 0;
  }

  emxInit_real_T(&xwork, 1);
  preSortLevel = x->size[0];
  i3 = xwork->size[0];
  xwork->size[0] = preSortLevel;
  emxEnsureCapacity((emxArray__common *)xwork, i3, (int)sizeof(double));
  preSortLevel = xwork->size[0];
  i3 = xwork->size[0];
  xwork->size[0] = preSortLevel;
  emxEnsureCapacity((emxArray__common *)xwork, i3, (int)sizeof(double));
  for (i3 = 0; i3 < preSortLevel; i3++) {
    xwork->data[i3] = 0.0;
  }

  ib = 0;
  for (k = 0; k + 1 <= n; k++) {
    ib++;
    idx4[ib - 1] = k + 1;
    x4[ib - 1] = b_x->data[k];
    if (ib == 4) {
      if (x4[0] <= x4[1]) {
        preSortLevel = 1;
        ib = 2;
      } else {
        preSortLevel = 2;
        ib = 1;
      }

      if (x4[2] <= x4[3]) {
        i3 = 3;
        i4 = 4;
      } else {
        i3 = 4;
        i4 = 3;
      }

      if (x4[preSortLevel - 1] <= x4[i3 - 1]) {
        if (x4[ib - 1] <= x4[i3 - 1]) {
          perm[0] = (signed char)preSortLevel;
          perm[1] = (signed char)ib;
          perm[2] = (signed char)i3;
          perm[3] = (signed char)i4;
        } else if (x4[ib - 1] <= x4[i4 - 1]) {
          perm[0] = (signed char)preSortLevel;
          perm[1] = (signed char)i3;
          perm[2] = (signed char)ib;
          perm[3] = (signed char)i4;
        } else {
          perm[0] = (signed char)preSortLevel;
          perm[1] = (signed char)i3;
          perm[2] = (signed char)i4;
          perm[3] = (signed char)ib;
        }
      } else if (x4[preSortLevel - 1] <= x4[i4 - 1]) {
        if (x4[ib - 1] <= x4[i4 - 1]) {
          perm[0] = (signed char)i3;
          perm[1] = (signed char)preSortLevel;
          perm[2] = (signed char)ib;
          perm[3] = (signed char)i4;
        } else {
          perm[0] = (signed char)i3;
          perm[1] = (signed char)preSortLevel;
          perm[2] = (signed char)i4;
          perm[3] = (signed char)ib;
        }
      } else {
        perm[0] = (signed char)i3;
        perm[1] = (signed char)i4;
        perm[2] = (signed char)preSortLevel;
        perm[3] = (signed char)ib;
      }

      idx->data[k - 3] = idx4[perm[0] - 1];
      idx->data[k - 2] = idx4[perm[1] - 1];
      idx->data[k - 1] = idx4[perm[2] - 1];
      idx->data[k] = idx4[perm[3] - 1];
      b_x->data[k - 3] = x4[perm[0] - 1];
      b_x->data[k - 2] = x4[perm[1] - 1];
      b_x->data[k - 1] = x4[perm[2] - 1];
      b_x->data[k] = x4[perm[3] - 1];
      ib = 0;
    }
  }

  if (ib > 0) {
    for (preSortLevel = 0; preSortLevel < 4; preSortLevel++) {
      perm[preSortLevel] = 0;
    }

    if (ib == 1) {
      perm[0] = 1;
    } else if (ib == 2) {
      if (x4[0] <= x4[1]) {
        perm[0] = 1;
        perm[1] = 2;
      } else {
        perm[0] = 2;
        perm[1] = 1;
      }
    } else if (x4[0] <= x4[1]) {
      if (x4[1] <= x4[2]) {
        perm[0] = 1;
        perm[1] = 2;
        perm[2] = 3;
      } else if (x4[0] <= x4[2]) {
        perm[0] = 1;
        perm[1] = 3;
        perm[2] = 2;
      } else {
        perm[0] = 3;
        perm[1] = 1;
        perm[2] = 2;
      }
    } else if (x4[0] <= x4[2]) {
      perm[0] = 2;
      perm[1] = 1;
      perm[2] = 3;
    } else if (x4[1] <= x4[2]) {
      perm[0] = 2;
      perm[1] = 3;
      perm[2] = 1;
    } else {
      perm[0] = 3;
      perm[1] = 2;
      perm[2] = 1;
    }

    for (k = 1; k <= ib; k++) {
      idx->data[((n - ib) + k) - 1] = idx4[perm[k - 1] - 1];
      b_x->data[((n - ib) + k) - 1] = x4[perm[k - 1] - 1];
    }
  }

  preSortLevel = 2;
  if (x->size[0] > 1) {
    if (x->size[0] >= 256) {
      ib = x->size[0] >> 8;
      if (ib > 0) {
        for (preSortLevel = 1; preSortLevel <= ib; preSortLevel++) {
          merge_pow2_block(idx, b_x, (preSortLevel - 1) << 8);
        }

        preSortLevel = ib << 8;
        ib = x->size[0] - preSortLevel;
        if (ib > 0) {
          merge_block(idx, b_x, preSortLevel, ib, 2, iwork, xwork);
        }

        preSortLevel = 8;
      }
    }

    merge_block(idx, b_x, 0, x->size[0], preSortLevel, iwork, xwork);
  }

  emxFree_real_T(&xwork);
  emxFree_int32_T(&iwork);
  i3 = x->size[0];
  x->size[0] = b_x->size[0];
  emxEnsureCapacity((emxArray__common *)x, i3, (int)sizeof(double));
  preSortLevel = b_x->size[0];
  for (i3 = 0; i3 < preSortLevel; i3++) {
    x->data[i3] = b_x->data[i3];
  }

  emxFree_real_T(&b_x);
}

//
// Arguments    : int m
//                int n
//                const emxArray_real_T *A
//                int ia0
//                int lda
//                const emxArray_real_T *x
//                int ix0
//                double y[3]
// Return Type  : void
//
static void xgemv(int m, int n, const emxArray_real_T *A, int ia0, int lda,
                  const emxArray_real_T *x, int ix0, double y[3])
{
  int iy;
  int i4;
  int iac;
  int ix;
  double c;
  int i5;
  int ia;
  if (n == 0) {
  } else {
    for (iy = 1; iy <= n; iy++) {
      y[iy - 1] = 0.0;
    }

    iy = 0;
    i4 = ia0 + lda * (n - 1);
    iac = ia0;
    while ((lda > 0) && (iac <= i4)) {
      ix = ix0;
      c = 0.0;
      i5 = (iac + m) - 1;
      for (ia = iac; ia <= i5; ia++) {
        c += A->data[ia - 1] * x->data[ix - 1];
        ix++;
      }

      y[iy] += c;
      iy++;
      iac += lda;
    }
  }
}

//
// Arguments    : emxArray_real_T *A
//                double tau_data[]
//                int tau_size[1]
//                int jpvt[3]
// Return Type  : void
//
static void xgeqp3(emxArray_real_T *A, double tau_data[], int tau_size[1], int
                   jpvt[3])
{
  int m;
  int mn;
  int k;
  double vn1[3];
  double vn2[3];
  double work[3];
  int j;
  int i;
  double temp1;
  int mmi;
  int itemp;
  double temp2;
  m = A->size[0];
  if (A->size[0] <= 3) {
    mn = A->size[0];
  } else {
    mn = 3;
  }

  tau_size[0] = mn;
  for (k = 0; k < 3; k++) {
    jpvt[k] = 1 + k;
  }

  if (A->size[0] == 0) {
  } else {
    k = 1;
    for (j = 0; j < 3; j++) {
      work[j] = 0.0;
      temp1 = xnrm2(m, A, k);
      vn2[j] = temp1;
      k += m;
      vn1[j] = temp1;
    }

    for (i = 1; i <= mn; i++) {
      k = (i + (i - 1) * m) - 1;
      mmi = m - i;
      j = (i + ixamax(4 - i, vn1, i)) - 2;
      if (j + 1 != i) {
        xswap(m, A, 1 + m * j, 1 + m * (i - 1));
        itemp = jpvt[j];
        jpvt[j] = jpvt[i - 1];
        jpvt[i - 1] = itemp;
        vn1[j] = vn1[i - 1];
        vn2[j] = vn2[i - 1];
      }

      if (i < m) {
        temp1 = A->data[k];
        tau_data[i - 1] = xzlarfg(mmi + 1, &temp1, A, k + 2);
        A->data[k] = temp1;
      } else {
        tau_data[i - 1] = 0.0;
      }

      if (i < 3) {
        temp1 = A->data[k];
        A->data[k] = 1.0;
        xzlarf(mmi + 1, 3 - i, k + 1, tau_data[i - 1], A, i + i * m, m, work);
        A->data[k] = temp1;
      }

      for (j = i; j + 1 < 4; j++) {
        if (vn1[j] != 0.0) {
          temp1 = fabs(A->data[(i + A->size[0] * j) - 1]) / vn1[j];
          temp1 = 1.0 - temp1 * temp1;
          if (temp1 < 0.0) {
            temp1 = 0.0;
          }

          temp2 = vn1[j] / vn2[j];
          temp2 = temp1 * (temp2 * temp2);
          if (temp2 <= 1.4901161193847656E-8) {
            if (i < m) {
              vn1[j] = c_xnrm2(mmi, A, (i + m * j) + 1);
              vn2[j] = vn1[j];
            } else {
              vn1[j] = 0.0;
              vn2[j] = 0.0;
            }
          } else {
            vn1[j] *= sqrt(temp1);
          }
        }
      }
    }
  }
}

//
// Arguments    : int m
//                int n
//                double alpha1
//                int ix0
//                const double y[3]
//                emxArray_real_T *A
//                int ia0
//                int lda
// Return Type  : void
//
static void xgerc(int m, int n, double alpha1, int ix0, const double y[3],
                  emxArray_real_T *A, int ia0, int lda)
{
  int jA;
  int jy;
  int j;
  double temp;
  int ix;
  int i6;
  int ijA;
  if (alpha1 == 0.0) {
  } else {
    jA = ia0 - 1;
    jy = 0;
    for (j = 1; j <= n; j++) {
      if (y[jy] != 0.0) {
        temp = y[jy] * alpha1;
        ix = ix0;
        i6 = m + jA;
        for (ijA = jA; ijA + 1 <= i6; ijA++) {
          A->data[ijA] += A->data[ix - 1] * temp;
          ix++;
        }
      }

      jy++;
      jA += lda;
    }
  }
}

//
// Arguments    : int n
//                const emxArray_real_T *x
//                int ix0
// Return Type  : double
//
static double xnrm2(int n, const emxArray_real_T *x, int ix0)
{
  double y;
  double scale;
  int kend;
  int k;
  double absxk;
  double t;
  y = 0.0;
  if (n < 1) {
  } else if (n == 1) {
    y = fabs(x->data[ix0 - 1]);
  } else {
    scale = 2.2250738585072014E-308;
    kend = (ix0 + n) - 1;
    for (k = ix0; k <= kend; k++) {
      absxk = fabs(x->data[k - 1]);
      if (absxk > scale) {
        t = scale / absxk;
        y = 1.0 + y * t * t;
        scale = absxk;
      } else {
        t = absxk / scale;
        y += t * t;
      }
    }

    y = scale * sqrt(y);
  }

  return y;
}

//
// Arguments    : int n
//                emxArray_real_T *x
//                int ix0
//                int iy0
// Return Type  : void
//
static void xswap(int n, emxArray_real_T *x, int ix0, int iy0)
{
  int ix;
  int iy;
  int k;
  double temp;
  ix = ix0 - 1;
  iy = iy0 - 1;
  for (k = 1; k <= n; k++) {
    temp = x->data[ix];
    x->data[ix] = x->data[iy];
    x->data[iy] = temp;
    ix++;
    iy++;
  }
}

//
// Arguments    : int m
//                int n
//                int iv0
//                double tau
//                emxArray_real_T *C
//                int ic0
//                int ldc
//                double work[3]
// Return Type  : void
//
static void xzlarf(int m, int n, int iv0, double tau, emxArray_real_T *C, int
                   ic0, int ldc, double work[3])
{
  int lastv;
  int lastc;
  if (tau != 0.0) {
    lastv = m;
    lastc = iv0 + m;
    while ((lastv > 0) && (C->data[lastc - 2] == 0.0)) {
      lastv--;
      lastc--;
    }

    lastc = ilazlc(lastv, n, C, ic0, ldc);
  } else {
    lastv = 0;
    lastc = 0;
  }

  if (lastv > 0) {
    xgemv(lastv, lastc, C, ic0, ldc, C, iv0, work);
    xgerc(lastv, lastc, -tau, iv0, work, C, ic0, ldc);
  }
}

//
// Arguments    : int n
//                double *alpha1
//                emxArray_real_T *x
//                int ix0
// Return Type  : double
//
static double xzlarfg(int n, double *alpha1, emxArray_real_T *x, int ix0)
{
  double tau;
  double xnorm;
  int knt;
  int i3;
  int k;
  tau = 0.0;
  if (n <= 0) {
  } else {
    xnorm = b_xnrm2(n - 1, x, ix0);
    if (xnorm != 0.0) {
      xnorm = hypot(*alpha1, xnorm);
      if (*alpha1 >= 0.0) {
        xnorm = -xnorm;
      }

      if (fabs(xnorm) < 1.0020841800044864E-292) {
        knt = 0;
        do {
          knt++;
          i3 = (ix0 + n) - 2;
          for (k = ix0; k <= i3; k++) {
            x->data[k - 1] *= 9.9792015476736E+291;
          }

          xnorm *= 9.9792015476736E+291;
          *alpha1 *= 9.9792015476736E+291;
        } while (!(fabs(xnorm) >= 1.0020841800044864E-292));

        xnorm = b_xnrm2(n - 1, x, ix0);
        xnorm = hypot(*alpha1, xnorm);
        if (*alpha1 >= 0.0) {
          xnorm = -xnorm;
        }

        tau = (xnorm - *alpha1) / xnorm;
        *alpha1 = 1.0 / (*alpha1 - xnorm);
        i3 = (ix0 + n) - 2;
        for (k = ix0; k <= i3; k++) {
          x->data[k - 1] *= *alpha1;
        }

        for (k = 1; k <= knt; k++) {
          xnorm *= 1.0020841800044864E-292;
        }

        *alpha1 = xnorm;
      } else {
        tau = (xnorm - *alpha1) / xnorm;
        *alpha1 = 1.0 / (*alpha1 - xnorm);
        i3 = (ix0 + n) - 2;
        for (k = ix0; k <= i3; k++) {
          x->data[k - 1] *= *alpha1;
        }

        *alpha1 = xnorm;
      }
    }
  }

  return tau;
}

//
// Arguments    : const double veloOriginROI[350000]
//                int veloOriginROILen
//                double useRate
//                double *p00
//                double *p10
//                double *p01
// Return Type  : void
//
void veloPlaneFitting(const double veloOriginROI[350000], int veloOriginROILen,
                      double useRate, double *p00, double *p10, double *p01)
{
  int loop_ub;
  emxArray_real_T *unusedU0;
  int i0;
  emxArray_int32_T *iidx;
  double d0;
  int sortLen;
  emxArray_real_T *sortInd;
  emxArray_real_T *b_veloOriginROI;
  int b_loop_ub;
  emxArray_real_T *c_veloOriginROI;
  double p[3];
  if (1 > veloOriginROILen) {
    loop_ub = 0;
  } else {
    loop_ub = veloOriginROILen;
  }

  emxInit_real_T(&unusedU0, 1);
  i0 = unusedU0->size[0];
  unusedU0->size[0] = loop_ub;
  emxEnsureCapacity((emxArray__common *)unusedU0, i0, (int)sizeof(double));
  for (i0 = 0; i0 < loop_ub; i0++) {
    unusedU0->data[i0] = veloOriginROI[140000 + i0];
  }

  emxInit_int32_T(&iidx, 1);
  sort(unusedU0, iidx);
  i0 = unusedU0->size[0];
  unusedU0->size[0] = iidx->size[0];
  emxEnsureCapacity((emxArray__common *)unusedU0, i0, (int)sizeof(double));
  loop_ub = iidx->size[0];
  for (i0 = 0; i0 < loop_ub; i0++) {
    unusedU0->data[i0] = iidx->data[i0];
  }

  emxFree_int32_T(&iidx);
  d0 = rt_roundd(rt_roundd(useRate * (double)unusedU0->size[0]));
  if (d0 < 2.147483648E+9) {
    if (d0 >= -2.147483648E+9) {
      i0 = (int)d0;
    } else {
      i0 = MIN_int32_T;
    }
  } else {
    i0 = MAX_int32_T;
  }

  sortLen = i0;
  if (1 > sortLen) {
    loop_ub = 0;
  } else {
    loop_ub = sortLen;
  }

  emxInit_real_T(&sortInd, 1);
  i0 = sortInd->size[0];
  sortInd->size[0] = loop_ub;
  emxEnsureCapacity((emxArray__common *)sortInd, i0, (int)sizeof(double));
  for (i0 = 0; i0 < loop_ub; i0++) {
    sortInd->data[i0] = unusedU0->data[i0];
  }

  emxFree_real_T(&unusedU0);
  emxInit_real_T1(&b_veloOriginROI, 2);
  i0 = b_veloOriginROI->size[0] * b_veloOriginROI->size[1];
  b_veloOriginROI->size[0] = sortInd->size[0];
  b_veloOriginROI->size[1] = 3;
  emxEnsureCapacity((emxArray__common *)b_veloOriginROI, i0, (int)sizeof(double));
  b_loop_ub = sortInd->size[0];
  for (i0 = 0; i0 < b_loop_ub; i0++) {
    b_veloOriginROI->data[i0] = veloOriginROI[(int)sortInd->data[i0] - 1];
  }

  b_loop_ub = sortInd->size[0];
  for (i0 = 0; i0 < b_loop_ub; i0++) {
    b_veloOriginROI->data[i0 + b_veloOriginROI->size[0]] = veloOriginROI[(int)
      sortInd->data[i0] + 69999];
  }

  for (i0 = 0; i0 < loop_ub; i0++) {
    b_veloOriginROI->data[i0 + (b_veloOriginROI->size[0] << 1)] = 1.0;
  }

  emxInit_real_T(&c_veloOriginROI, 1);
  i0 = c_veloOriginROI->size[0];
  c_veloOriginROI->size[0] = sortInd->size[0];
  emxEnsureCapacity((emxArray__common *)c_veloOriginROI, i0, (int)sizeof(double));
  loop_ub = sortInd->size[0];
  for (i0 = 0; i0 < loop_ub; i0++) {
    c_veloOriginROI->data[i0] = veloOriginROI[(int)sortInd->data[i0] + 139999];
  }

  emxFree_real_T(&sortInd);
  mldivide(b_veloOriginROI, c_veloOriginROI, p);
  *p00 = p[2];
  *p10 = p[0];
  *p01 = p[1];
  emxFree_real_T(&c_veloOriginROI);
  emxFree_real_T(&b_veloOriginROI);
}

//
// Arguments    : void
// Return Type  : void
//
void veloPlaneFitting_initialize()
{
}

//
// Arguments    : void
// Return Type  : void
//
void veloPlaneFitting_terminate()
{
  // (no terminate code required)
}

//
// File trailer for veloPlaneFitting.cpp
//
// [EOF]
//
