#include "ros/ros.h"  
#include "std_msgs/String.h"  
#include "mainCommon.h"
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>


namespace velodyne_pointcloud
{
  /** Euclidean Velodyne coordinate, including intensity and ring number. */
  struct PointXYZIR
  {
    PCL_ADD_POINT4D;                    // quad-word XYZ
    float    intensity;                 ///< laser intensity reading
    uint16_t ring;                      ///< laser ring number
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW     // ensure proper alignment
  } EIGEN_ALIGN16;

}; // namespace velodyne_pointcloud

POINT_CLOUD_REGISTER_POINT_STRUCT(velodyne_pointcloud::PointXYZIR,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, intensity, intensity)
                                  (uint16_t, ring, ring))
  
typedef velodyne_pointcloud::PointXYZIR VPoint;
typedef pcl::PointCloud<VPoint> VPointCloud;



#include <sstream>  
int main(int argc, char **argv)  
{  
  /** 
   * The ros::init() function needs to see argc and argv so that it can perform 
   * any ROS arguments and name remapping that were provided at the command line. For programmatic 
   * remappings you can use a different version of init() which takes remappings 
   * directly, but for most command-line programs, passing argc and argv is the easiest 
   * way to do it.  The third argument to init() is the name of the node. 
   * 
   * You must call one of the versions of ros::init() before using any other 
   * part of the ROS system. 
   */  
  ros::init(argc, argv, "icp_node");  
  
  /** 
   * NodeHandle is the main access point to communications with the ROS system. 
   * The first NodeHandle constructed will fully initialize this node, and the last 
   * NodeHandle destructed will close down the node. 
   */  
  ros::NodeHandle n;  
  
  /** 
   * The advertise() function is how you tell ROS that you want to 
   * publish on a given topic name. This invokes a call to the ROS 
   * master node, which keeps a registry of who is publishing and who 
   * is subscribing. After this advertise() call is made, the master 
   * node will notify anyone who is trying to subscribe to this topic name, 
   * and they will in turn negotiate a peer-to-peer connection with this 
   * node.  advertise() returns a Publisher object which allows you to 
   * publish messages on that topic through a call to publish().  Once 
   * all copies of the returned Publisher object are destroyed, the topic 
   * will be automatically unadvertised. 
   * 
   * The second parameter to advertise() is the size of the message queue 
   * used for publishing messages.  If messages are published more quickly 
   * than we can send them, the number here specifies how many messages to 
   * buffer up before throwing some away. 
   */  
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);  
  
  ros::Rate loop_rate(10);  
  
  /****************************wl*************************/
  ros::NodeHandle node_velodyne;
  ros::Publisher output_velodyne = node_velodyne.advertise<sensor_msgs::PointCloud2>("vecan_velodyne", 10);  

  ros::NodeHandle node_curb;
  ros::Publisher output_curb = node_curb.advertise<sensor_msgs::PointCloud2>("vecan_curb", 10);  



  stateType myState;
  stateInitial(myState);
  //--------------------------interaction  menu------------------------//
  userOperationGuide(myState);

  //--------------------------sensor & map initial----------------------//
  inertial *myInertial = new inertial(myState.inertialMode);
  velodyne *myVelodyne = new velodyne(myState.velodyneMode);
  digitalMap *myMap = new digitalMap("/home/vecan/DigitalMap/bin/CurbMap",
				     "/home/vecan/DigitalMap/bin/LaneMarkerMap",
				     "/home/vecan/DigitalMap/bin/ZebraCrossingMap",
				     "/home/vecan/DigitalMap/bin/GridMap");

  systemInitialize(myState, myMap, myInertial, myVelodyne);
  
  //-------initial localizer----//
  localization *myLocalizer = new localization(myState.inertialVar);


  /****************************wl*************************/	
  /** 
   * A count of how many messages we have sent. This is used to create 
   * a unique string for each message. 
   */  
  int count = 0;  
  while (ros::ok())  
  {  
    /** 
     * This is a message object. You stuff it with data, and then publish it. 
     */  
    std_msgs::String msg;  
  
    std::stringstream ss;  
    ss << "hello world " << count;  
    msg.data = ss.str();  
  
    ROS_INFO("%s", msg.data.c_str());  
  

    /*-------------------------------wl-------------------------------------*/

    if (1 == myState.offlineUpdateFlag || '2' == myState.inputMode)
    {

	//myState.offlineUpdateFlag = 0;
			
	//------------------------save pre-frame data--------------------//
	myState.inertialVarPre = myState.inertialVar;
        std::cout << "lat:" << myState.inertialVarPre.mLat << "  " << "lon:" << myState.inertialVarPre.mLon << " time:" << myState.timeConsume << std::endl;
	std::cout << myState.winSufName << std::endl;
	//-------------------------update sensor data--------------------//
	//----update velodyne data---//
        
	myVelodyne->readFromFile(myState.offlineFileIndex);
	myVelodyne->copyVeloData();
        
	//----update inertial data---//
	myInertial->readFromFile(myState.offlineFileIndex);
	myInertial->returnIntertialData(myState.inertialVar);

	//-------------------------data process--------------------------//
	timeCalculate(1);//start time

	localizationAndObstacleDetection(myState, myMap, myInertial, myVelodyne, myLocalizer);
			
	//--------------------pcl point cloud creation-------------------//
	//myState.Mutex.lock();// lock mutex



	if ('1' == myState.inputMode)
	{
		if (myLocalizer->featureMapMatchingFlag == -1 && myLocalizer->gridMapMatchingFlag == 1)
		{
			sprintf(myState.winSufName, "%d  F", myState.offlineFileIndex);
		}
		else if (myLocalizer->featureMapMatchingFlag == 1 && myLocalizer->gridMapMatchingFlag == -1)
		{
			sprintf(myState.winSufName, "%d  G", myState.offlineFileIndex);
		}
		else if (myLocalizer->featureMapMatchingFlag == -1 && myLocalizer->gridMapMatchingFlag == -1)
		{
			sprintf(myState.winSufName, "%d  FG", myState.offlineFileIndex);
		}
		else
		{
			sprintf(myState.winSufName, "%d", myState.offlineFileIndex);
		}
				
		myState.offlineFileIndex++;
	}
	else if ('2' == myState.inputMode)
	{
		sprintf(myState.winSufName, "%d", myState.onlineFrameIndex);
		myState.onlineFrameIndex++;
	}
			
	//end time
	myState.timeConsume = timeCalculate(2);
			
	myState.dispUpdateFlag = 1;

      

        
	//myState.Mutex.unlock();//unlock mutex
			
			
    }


    VPointCloud::Ptr outMsg_velodyne(new VPointCloud());
    outMsg_velodyne->header.stamp = 0;
    outMsg_velodyne->header.frame_id = "velodyne";
    outMsg_velodyne->height = 1;
   
    for(int i=0;i<myVelodyne->veloNewestData.size;i++){
    	VPoint point;
	point.ring = myVelodyne->veloNewestData.id[i];
        point.x = myVelodyne->veloNewestData.x[i];
        point.y = myVelodyne->veloNewestData.y[i];
        point.z = myVelodyne->veloNewestData.z[i];
        point.intensity = myVelodyne->veloNewestData.r[i];
        outMsg_velodyne->points.push_back(point);
        ++outMsg_velodyne->width;
    }

    VPointCloud::Ptr outMsg_curb(new VPointCloud());
    outMsg_curb->header.stamp = 0;
    outMsg_curb->header.frame_id = "curb_map";
    outMsg_curb->height = 1;
    
    for(int i=0;i<myMap->localCurbMap.size();i++){
    	VPoint point;
	point.ring = 0;
        point.x = myMap->localCurbMap[i].x;
        point.y = myMap->localCurbMap[i].y;
        point.z = myMap->localCurbMap[i].z - 1.8;
        point.intensity = 0;
        outMsg_curb->points.push_back(point);
        ++outMsg_curb->width;
    }


    output_velodyne.publish(outMsg_velodyne);
    output_curb.publish(outMsg_curb);

    if (1 == myState.quitFlag)	break;

    /*-------------------------------wl-------------------------------------*/
    /** 
     * The publish() function is how you send messages. The parameter 
     * is the message object. The type of this object must agree with the type 
     * given as a template parameter to the advertise<>() call, as was done 
     * in the constructor above. 
     */  
    chatter_pub.publish(msg);  
  
    ros::spinOnce();  
  
    loop_rate.sleep();  
    ++count;  
  }  
  
  if (NULL != myInertial)  delete myInertial;
  if (NULL != myVelodyne)  delete myVelodyne;
  if (NULL != myMap)	   delete myMap;
  if (NULL != myLocalizer) delete myLocalizer;
  
  return 0;  
}
